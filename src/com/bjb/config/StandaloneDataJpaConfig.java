package com.bjb.config;

import java.util.Properties;
import java.util.concurrent.Executor;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.bjb.commons.MainBean;
import com.bjb.job.CleansingLog;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories("com.bjb.repository")
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
@ComponentScan("com.bjb")
//@PropertySource("classpath:datasource.properties")
@PropertySource("file:/opt/apache-tomcat-7.0.53/webapps/sam-core-config/datasource.properties")
public class StandaloneDataJpaConfig implements AsyncConfigurer{

	@Bean
	public DataSource dataSource(Environment env) {
		HikariConfig dataSourceConfig = new HikariConfig();
		dataSourceConfig.setDriverClassName(env.getRequiredProperty("db.driver"));
		dataSourceConfig.setJdbcUrl(env.getRequiredProperty("db.url"));
		dataSourceConfig.setUsername(env.getRequiredProperty("db.username"));
		dataSourceConfig.setPassword(env.getRequiredProperty("db.password"));
		dataSourceConfig.setMaximumPoolSize(Integer.valueOf(env.getRequiredProperty("db.poolsize")));
		return new HikariDataSource(dataSourceConfig);
	}

	@Bean
	public PlatformTransactionManager transactionManager(DataSource dataSource, Environment env) {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory(dataSource, env));
		return txManager;
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	@Bean
	public EntityManagerFactory entityManagerFactory(DataSource dataSource, Environment env) {

		// will set the provider to 'org.hibernate.ejb.HibernatePersistence'
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		// will set hibernate.show_sql to 'true'
		vendorAdapter.setShowSql(true);
		// if set to true, will set hibernate.hbm2ddl.auto to 'update'
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.bjb.model");
		factory.setDataSource(dataSource);

		Properties jpaProperties = new Properties();

		// Configures the used database dialect. This allows Hibernate to create
		// SQL
		// that is optimized for the used database.
		jpaProperties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));

		// Specifies the action that is invoked to the database when the
		// Hibernate
		// SessionFactory is created or closed.
		// jpaProperties.put("hibernate.hbm2ddl.auto",
		// env.getRequiredProperty("hibernate.hbm2ddl.auto"));

		// Configures the naming strategy that is used when Hibernate creates
		// new database objects and schema elements
		jpaProperties.put("hibernate.ejb.naming_strategy", env.getRequiredProperty("hibernate.ejb.naming_strategy"));

		// If the value of this property is true, Hibernate writes all SQL
		// statements to the console.
		jpaProperties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));

		// If the value of this property is true, Hibernate will format the SQL
		// that is written to the console.
		jpaProperties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));

		factory.setJpaProperties(jpaProperties);

		// This will trigger the creation of the entity manager factory
		factory.afterPropertiesSet();

		return factory.getObject();
	}
	
	@Bean
	public TaskExecutor taskExecutor(Environment env){
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setThreadNamePrefix("generalExecutor");
        executor.setCorePoolSize(Integer.valueOf(env.getProperty("exe.pool")));
        executor.setMaxPoolSize(Integer.valueOf(env.getProperty("exe.maxpool")));
        executor.setQueueCapacity(Integer.valueOf(env.getProperty("exe.queue")));
        return executor;
	}
	
	@Bean
	public ThreadPoolTaskScheduler poolScheduler(){
		ThreadPoolTaskScheduler sche = new ThreadPoolTaskScheduler();
		sche.setPoolSize(15);
		sche.setThreadNamePrefix("poolScheduler");
		return sche;
	}
	
//	@Bean
//    public ThreadPoolTaskExecutor poolScheduler(Environment env) {
//        ThreadPoolTaskExecutor scheduler = new ThreadPoolTaskExecutor();
//        scheduler.setThreadNamePrefix("poolScheduler");
//        scheduler.setCorePoolSize(Integer.valueOf(env.getProperty("exe.pool")));
//        scheduler.setMaxPoolSize(Integer.valueOf(env.getProperty("exe.threadpool")));
//        scheduler.setQueueCapacity(Integer.valueOf(env.getProperty("exe.queue")));
//        return scheduler;
//    }
	
	@Bean
	public JobDetailFactoryBean jobDetailFactoryBean() {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(CleansingLog.class);
		factory.setGroup("Scheduler");
		factory.setName("CleansingLog");
		return factory;
	}

	// Job is scheduled at 12:00 1/1 every month
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBean(Environment env) {
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBean().getObject());
		stFactory.setStartDelay(0);
		stFactory.setName("CronJob");
		stFactory.setGroup("Scheduler");
		stFactory.setCronExpression(env.getProperty("cron.expression"));
		return stFactory;
	}

	@Bean
	public SchedulerFactoryBean schedulerFactoryBean(Environment env) {
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		scheduler.setTriggers(cronTriggerFactoryBean(env).getObject());
		return scheduler;
	}

	@Bean
	public MainBean mainBean() {
		return new MainBean();
	}

	@Override
	public Executor getAsyncExecutor() {
		 ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	        executor.setCorePoolSize(2);
	        executor.setMaxPoolSize(10);
	        executor.setQueueCapacity(500);
	        executor.setThreadNamePrefix("Async_Exe");
	        executor.initialize();
	        return executor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		 return new SimpleAsyncUncaughtExceptionHandler();
	}
}