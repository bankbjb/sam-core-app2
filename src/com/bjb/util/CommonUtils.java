package com.bjb.util;

public class CommonUtils {

	public static String unPadRight(String s, char c) {

		int end = s.length();
		if (end == 0)
			return s;

		while ((0 < end) && (s.charAt(end - 1) == c))
			end--;
		return 0 < end ? s.substring(0, end) : s.substring(0, 1);
	}

	public static String unPadLeft(String s, char c) {
		int fill = 0;
		int end = s.length();
		if (end == 0)
			return s;

		while ((fill < end) && (s.charAt(fill) == c))
			fill++;

		return fill < end ? s.substring(fill, end) : s.substring(fill - 1, end);
	}

	public static String padleft(String s, int len, char c) {
		try {
			s = s.trim();
			if (s.length() > len)
				throw new Exception("invalid len " + s.length() + "/" + len);

			StringBuffer d = new StringBuffer(len);
			int fill = len - s.length();

			while (fill-- > 0)
				d.append(c);
			d.append(s);
			return d.toString();
		} catch (Exception e) {
			return s;
		}
	}

	public static String padright(String s, int len, char c) {
		try {
			s = s.trim();
			if (s.length() > len)
				throw new Exception("invalid len " + s.length() + "/" + len);

			StringBuffer d = new StringBuffer(len);
			int fill = len - s.length();
			d.append(s);

			while (fill-- > 0)
				d.append(c);
			return d.toString();
		} catch (Exception e) {
			return s;
		}

	}

	public static String removeUntilNumber(String s) {
		return s.replaceAll("[A-Za-z]", "");
	}

	public static String addPrefix(String s, String pre) {
		return pre + s;
	}

	public static boolean isBlank(String a) {
		if (a.equals("") || a == null) {
			return true;
		} else {
			return false;
		}
	}

}
