package com.bjb.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.jpos.iso.ISOMsg;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;

public class ObjectUtils {

	/**
	 * read data from isoMsg to Map
	 * 
	 * @param msg
	 * @return
	 */
	public static Map<String, String> unpackFromIso(ISOMsg msg) {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i <= msg.getMaxField(); i++) {
			if (msg.hasField(i)) {
				map.put(String.valueOf(i), msg.getString(i));
			}
		}
		return map;
	}

	/**
	 * return String from map
	 * 
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String printMap(Map map) {
		Iterator it = map.keySet().iterator();
		String res = "";
		while (it.hasNext()) {
			String key = String.valueOf(it.next());
			res += (key + " = " + map.get(key) + "\n");
		}
		return res;
	}

	/**
	 * read data from object
	 * 
	 * @param o
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static Map<String, Object> unpackFromObject(Object o)
			throws IllegalArgumentException, IllegalAccessException {
		Map<String, Object> map = new HashMap<String, Object>();
		Class<?> clazz = o.getClass();

		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			if (field.get(o) != null) {
				map.put(field.getName().toLowerCase(), field.get(o));
			}
		}
		return map;
	}

	/**
	 * Print object set
	 * 
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String printSet(Set map) {
		Iterator it = map.iterator();
		String res = "";
		while (it.hasNext()) {
			String key = it.next().toString();
			res += key;
		}
		return res;
	}

	public static String printToJson(Object obj) {
		Gson gson = new Gson();
		// Class<?> clazz = obj.getClass();
		String json = gson.toJson(obj);
		return json;
	}

	public static String printToXml(Object obj) {
		XmlMapper mapper = new XmlMapper();
		try {
			String xml = mapper.writeValueAsString(obj);
			return xml;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
