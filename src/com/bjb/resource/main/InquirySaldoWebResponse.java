package com.bjb.resource.main;

public class InquirySaldoWebResponse {
	private String saldo;

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
}
