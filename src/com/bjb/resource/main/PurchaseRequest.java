package com.bjb.resource.main;

import com.bjb.resource.additional.AmountInformation;
import com.bjb.resource.additional.BillInformation;
import com.bjb.resource.additional.TerminalAggregator;

/**
 * @author satrio
 * 
 * Goal			: Generate doc as input for web service
 * Create Date	: Sep 3, 2016
 */
public class PurchaseRequest {

	//from aggregator (rest)
	private String primaryAccountNumber;//2
	private String processingCode;//3
	private String amountTransaction;//4
	private String settlementDate;//15
	private String merchantType;//18
	private String aggregatorCode;//32
	private String caCode;//additional
	private TerminalAggregator terminalAggregator;//41-43
	private String currencyCode;//49
	private AmountInformation amountInformation;//57
	private String pay;//59
	private String featureCode;//60
	private BillInformation billInformation;//additional
	private String sourceAccountNumber;//102
	private String destinationAccountNumber;//103
	private String additionalData;//61 bill id+nominal pulsa
	
	
	
	//soap
	private String posEntryModeCode;//2
	private String localTransactionDate;//13 dont need
	private String track2Data;//35
	private String sequenceNumber;//37
	private String transmissionDateTime;//7
	private String timeLocalTransaction;//12
	
	//generated
	private String systemTraceAuditNumber;//11
	
	public String getPrimaryAccountNumber() {
		return primaryAccountNumber;
	}

	public void setPrimaryAccountNumber(String primaryAccountNumber) {
		this.primaryAccountNumber = primaryAccountNumber;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAmountTransaction() {
		return amountTransaction;
	}

	public void setAmountTransaction(String amountTransaction) {
		this.amountTransaction = amountTransaction;
	}

	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}

	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}

	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}

	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}

	public String getTimeLocalTransaction() {
		return timeLocalTransaction;
	}

	public void setTimeLocalTransaction(String timeLocalTransaction) {
		this.timeLocalTransaction = timeLocalTransaction;
	}

	public String getLocalTransactionDate() {
		return localTransactionDate;
	}

	public void setLocalTransactionDate(String localTransactionDate) {
		this.localTransactionDate = localTransactionDate;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getPosEntryModeCode() {
		return posEntryModeCode;
	}

	public void setPosEntryModeCode(String posEntryModeCode) {
		this.posEntryModeCode = posEntryModeCode;
	}

	public String getAggregatorCode() {
		return aggregatorCode;
	}

	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}

	public String getCaCode() {
		return caCode;
	}

	public void setCaCode(String caCode) {
		this.caCode = caCode;
	}

	public String getTrack2Data() {
		return track2Data;
	}

	public void setTrack2Data(String track2Data) {
		this.track2Data = track2Data;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public TerminalAggregator getTerminalAggregator() {
		return terminalAggregator;
	}

	public void setTerminalAggregator(TerminalAggregator terminalAggregator) {
		this.terminalAggregator = terminalAggregator;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public BillInformation getBillInformation() {
		return billInformation;
	}

	public void setBillInformation(BillInformation billInformation) {
		this.billInformation = billInformation;
	}

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	public AmountInformation getAmountInformation() {
		return amountInformation;
	}

	public void setAmountInformation(AmountInformation amountInformation) {
		this.amountInformation = amountInformation;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public PurchaseRequest() {
		super();
		terminalAggregator = new TerminalAggregator();
		amountInformation = new AmountInformation();
		billInformation = new BillInformation();
	}
	
	

}
