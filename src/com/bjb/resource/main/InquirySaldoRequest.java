package com.bjb.resource.main;

import com.bjb.resource.additional.TerminalAggregator;

/**
 * @author satrio
 * 
 * Goal			: Create doc as input for web service
 * Create Date	: Sep 3, 2016
 */
public class InquirySaldoRequest {

	//from aggregator
	private String primaryAccountNumber;//2
	private String processingCode;//3
	private String amountTransaction;//4
	private String aggregatorCode;//32
	private String caCode;//additional
	private TerminalAggregator terminalAggregator;//41-43
	private String currencyCode;//49
	private String pay;//59
	private String additionalData;//63
	private String sourceAccountNumber;//102
	
	//generated-fix
	private String systemTraceAuditNumber;//11
	private String track2Data;//35
	private String transmissionDateTime;//7

	public String getPrimaryAccountNumber() {
		return primaryAccountNumber;
	}

	public void setPrimaryAccountNumber(String primaryAccountNumber) {
		this.primaryAccountNumber = primaryAccountNumber;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAmountTransaction() {
		return amountTransaction;
	}

	public void setAmountTransaction(String amountTransaction) {
		this.amountTransaction = amountTransaction;
	}

	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}

	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}

	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}

	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}

	public String getAggregatorCode() {
		return aggregatorCode;
	}

	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}

	public String getCaCode() {
		return caCode;
	}

	public void setCaCode(String caCode) {
		this.caCode = caCode;
	}

	public String getTrack2Data() {
		return track2Data;
	}

	public void setTrack2Data(String track2Data) {
		this.track2Data = track2Data;
	}

	public TerminalAggregator getTerminalAggregator() {
		return terminalAggregator;
	}

	public void setTerminalAggregator(TerminalAggregator terminalAggregator) {
		this.terminalAggregator = terminalAggregator;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public InquirySaldoRequest() {
		super();
		terminalAggregator = new TerminalAggregator();
	}

	public InquirySaldoRequest(InquiryPaymentResponse ipr){
		this.aggregatorCode = ipr.getAggregatorCode();
		this.caCode = ipr.getCaCode();
		this.currencyCode = ipr.getCurrencyCode();
		this.pay = ipr.getPay();
		this.primaryAccountNumber = ipr.getPrimaryAccountNumber();
		this.processingCode = "30100";
		this.sourceAccountNumber = ipr.getSourceAccountNumber();
		//this.terminalAggregator = ipr.getTerminalAggregator();
	}
	
	public InquirySaldoRequest(PaymentResponse ipr){
		this.aggregatorCode = ipr.getAggregatorCode();
		this.caCode = ipr.getCaCode();
		this.currencyCode = ipr.getCurrencyCode();
		this.pay = ipr.getPay();
		this.primaryAccountNumber = ipr.getPrimaryAccountNumber();
		this.processingCode = "30100";
		this.sourceAccountNumber = ipr.getSourceAccountNumber();
		//this.terminalAggregator = ipr.getTerminalAggregator();
	}

	@Override
	public String toString() {
		return "InquirySaldoRequest [primaryAccountNumber=" + primaryAccountNumber + ", processingCode="
				+ processingCode + ", amountTransaction=" + amountTransaction + ", aggregatorCode=" + aggregatorCode
				+ ", caCode=" + caCode + ", terminalAggregator=" + terminalAggregator + ", currencyCode=" + currencyCode
				+ ", pay=" + pay + ", additionalData=" + additionalData + ", sourceAccountNumber=" + sourceAccountNumber
				+ ", systemTraceAuditNumber=" + systemTraceAuditNumber + ", track2Data=" + track2Data
				+ ", transmissionDateTime=" + transmissionDateTime + "]";
	}
	
	
	
}
