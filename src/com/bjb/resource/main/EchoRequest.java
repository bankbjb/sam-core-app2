package com.bjb.resource.main;

public class EchoRequest {
	private String transmissionDateTime;
	private String systemTraceAuditNumber;
	private String tanggalBuku;
	private String responseCode;
	private String systemFunctionCode;

	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}

	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}

	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}

	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}

	public String getTanggalBuku() {
		return tanggalBuku;
	}

	public void setTanggalBuku(String tanggalBuku) {
		this.tanggalBuku = tanggalBuku;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getSystemFunctionCode() {
		return systemFunctionCode;
	}

	public void setSystemFunctionCode(String systemFunctionCode) {
		this.systemFunctionCode = systemFunctionCode;
	}

}
