package com.bjb.resource.additional;

import java.io.Serializable;

/**
 * @author satrio
 * 
 * Goal			: Create helper field as input on inquiry saldo request
 * Create Date	: Sep 3, 2016
 */
public class TerminalAggregator implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4731473184710799797L;
	private String terminalIdentifierNumber;
	private String terminalName;
	private String cardAcceptorName;
	
	public String getTerminalIdentifierNumber() {
		return terminalIdentifierNumber;
	}
	public void setTerminalIdentifierNumber(String terminalIdentifierNumber) {
		this.terminalIdentifierNumber = terminalIdentifierNumber;
	}
	public String getTerminalName() {
		return terminalName;
	}
	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}
	public String getCardAcceptorName() {
		return cardAcceptorName;
	}
	public void setCardAcceptorName(String cardAcceptorName) {
		this.cardAcceptorName = cardAcceptorName;
	}
	@Override
	public String toString() {
		return "TerminalAggregator [terminalIdentifierNumber=" + terminalIdentifierNumber + ", terminalName="
				+ terminalName + ", cardAcceptorName=" + cardAcceptorName + "]";
	}


}
