package com.bjb.resource.additional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.cache.ForBitSixtyOneMapping;
import com.bjb.cache.ForBitSixtyOneMessageField;
import com.bjb.cache.SAMPackagerCache;
import com.bjb.constants.Constants;
import com.bjb.iso.util.SamPackager;
import com.bjb.model.FieldMapping;
import com.bjb.model.MessageFields;
import com.bjb.model.MessageMapping;
import com.bjb.resource.main.InquiryPaymentRequest;
import com.bjb.resource.main.PaymentRequest;
import com.bjb.resource.main.PurchaseRequest;
import com.bjb.util.CommonUtils;
import com.bjb.util.ObjectUtils;

/**
 * for bit 61 helper
 * 
 * @author arifino
 *
 */
class MsgLocation {
	String msg;
	Integer loc;
	Integer st;
	Integer end;
	String padding;
	String var;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getLoc() {
		return loc;
	}

	public void setLoc(Integer loc) {
		this.loc = loc;
	}

	public MsgLocation(String msg, Integer loc) {
		super();
		this.msg = msg;
		this.loc = loc;
	}

	public MsgLocation(String msg, Integer loc, Integer st, Integer end, String padding, String var) {
		super();
		this.msg = msg;
		this.loc = loc;
		this.st = st;
		this.end = end;
		this.padding = padding;
		this.var = var;
	}

	public Integer getSt() {
		return st;
	}

	public void setSt(Integer st) {
		this.st = st;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public String getPadding() {
		return padding;
	}

	public void setPadding(String padding) {
		this.padding = padding;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

}

public class AdditionalDataPayment {

	private static Logger log = LoggerFactory.getLogger(AdditionalDataPayment.class);

	/**
	 * Pack bit 61 message based on isoMsg [unused]
	 * 
	 * @param msg
	 * @param messageType
	 * @return String
	 * @throws ISOException
	 */
	@SuppressWarnings("rawtypes")
	public static String pack(ISOMsg msg, String messageType) throws ISOException {
		// set packager
		SamPackager samPackager = new SamPackager();
		msg.setPackager(samPackager);
		// pack to iso to make sure padding is right
		byte[] data = new byte[1024];
		data = msg.pack();
		ISOMsg padded = new ISOMsg();
		padded.setPackager(new SamPackager());
		padded.unpack(data);
		// end packing iso
		// start generating bit 61
		String bit61 = "";
		// TODO Still using raw fitur id not fitur kode
		MessageMapping m = ForBitSixtyOneMapping
				.getMessageMapping(Integer.valueOf(CommonUtils.removeUntilNumber(msg.getString(60))), messageType);
		// System.out.println(m);
		if (m != null) {
			if (m.getStatus().equals(new Integer(1))) {
				Iterator iter = m.getFieldMappingSet().iterator();
				while (iter.hasNext()) {
					FieldMapping f = (FieldMapping) iter.next();
					int pos = -1;
					// if xml or json check location from cache else use as it
					// is
					if (m.getMessageType().equalsIgnoreCase("XML") || m.getMessageType().equalsIgnoreCase("JSON")) {
						String atr = f.getMessageFields().getName();
						pos = SAMPackagerCache.getBit(atr);
					} else {
						pos = Integer.parseInt(f.getMessageFields().getName());
					}
					if (pos > -1) {
						int end = f.getEndAt();
						if (end > padded.getString(pos).length()) {
							end = padded.getString(pos).length();
						}
						bit61 += (padded.getString(pos).substring(f.getStartAt(), end));
					}
				}
				return bit61;
			} else
				return bit61;
		} else
			return bit61;
	}

	public static String pack(InquiryPaymentRequest request, String messageType)
			throws IllegalArgumentException, IllegalAccessException {
		//Suwardi
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(
				Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode().trim())), messageType);
		String bit61 = "";
		if (m != null) {
			if (m.getStatus().equals(new Integer(1))) {
				Map fieldObj = ObjectUtils.unpackFromObject(request);
				Iterator iter = m.getFieldMappingSet().iterator();
				while (iter.hasNext()) {
					FieldMapping f = (FieldMapping) iter.next();
					if (f.getMessageFields().getName().toLowerCase().contains("billinfo")) {
						if (f.getRequest().equals(Integer.valueOf(1))) {
							String fieldname = f.getMessageFields().getName();
							int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
							String billinfo = "";
							// System.out.println(fieldname + " " +
							// billInfoNumber);
							if (billInfoNumber > 0) {
								billinfo = request.getBillInformation().getBills()[billInfoNumber - 1].getInfo();
								// billinfo = CommonUtils.padright(billinfo, 20,
								// ' ');
							}
							if (billinfo != null && billinfo.length() > 0) {
								bit61 += billinfo.substring(f.getStartAt(), f.getEndAt());
							}
						}
					} else {
						if (fieldObj.containsKey(f.getMessageFields().getName().toLowerCase())) {
							String val = String.valueOf(fieldObj.get(f.getMessageFields().getName().toLowerCase()));
							bit61 += val.substring(f.getStartAt(), f.getEndAt());
						}
					}
				}
				return bit61;
			} else
				return bit61;
		} else
			return bit61;
	}

	/**
	 * Pack bit61 from isoMsg Object
	 * 
	 * @param request
	 * @return
	 * @throws IllegalAccessException
	 */
	public static String packsixtyone(ISOMsg request) throws IllegalAccessException {
		log.info("packsixtyone IN ..... " + Integer.valueOf(CommonUtils.removeUntilNumber(request.getString(60))));
		MessageMapping m = ForBitSixtyOneMapping
				.getMessageMapping(Integer.valueOf(CommonUtils.removeUntilNumber(request.getString(60))), "ISO");
		Map<String, List<FieldMapping>> mapFieldMappingToVariable = new HashMap<String, List<FieldMapping>>();
		List<String> listOfVariable = new ArrayList<>();
		String mode = "";
		if (m != null) {
			//String mode = "";
			mode = "";
			if (request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT)||
					request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_SIPANDU)||
					request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_PDAM) ||
					request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_EDUPAY) ||
					request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_ESAMSAT )) {
				mode = "INQUIRY_PAYMENT";
			} else if (request.getString(3).contains(Constants.TRX_CODE_PAYMENT)||
					request.getString(3).contains(Constants.TRX_CODE_PAYMENT_SIPANDU)||
					request.getString(3).contains(Constants.TRX_CODE_PAYMENT_PDAM)||
					request.getString(3).contains(Constants.TRX_CODE_PAYMENT_EDUPAY)||
					request.getString(3).contains(Constants.TRX_CODE_PAYMENT_ESAMSAT)) {
				mode = "PAYMENT";
			} else if (request.getString(3).contains(Constants.TRX_CODE_PURCHASE)) {
				mode = "PURCHASE";
			}
			Map fieldOfObj = ObjectUtils.unpackFromObject(request);
			Iterator iter = m.getFieldMappingSet().iterator();
			// Putting fieldMapping from message mapping into map, and listing
			// all variable to listOfVariable
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (mode != "" && fm.getTrxType().equalsIgnoreCase(mode)) {
					// Checking if fm is request/response
					if (fm.getRequest().equals(Integer.valueOf(1))) {
						// if fm with same variable is exist add it to list in
						// the
						// map
						if (mapFieldMappingToVariable.get(fm.getVariable()) != null) {
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							log.info("Putting field " + fm.getMessageFields().getName() + " to variable "
									+ fm.getVariable());
							// else put it to new entry in map
						} else if (mapFieldMappingToVariable.get(fm.getVariable()) == null) {
							mapFieldMappingToVariable.put(fm.getVariable(), new ArrayList<FieldMapping>());
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							listOfVariable.add(fm.getVariable());
							log.info("putting new variable " + fm.getVariable());
							log.info("Putting field " + fm.getMessageFields().getName() + " to variable "
									+ fm.getVariable());
						}
					}
				}
			}

			/**
			 * false for sorting field mapping in each variable, if multiple
			 * field in one variable is enabled. iter =
			 * mapFieldMappingToVariable.keySet().iterator(); while
			 * (iter.hasNext()) { String key = (String) iter.next();
			 * List<FieldMapping> lisfm = mapFieldMappingToVariable.get(key);
			 * Collections.sort(lisfm, new Comparator<FieldMapping>() {
			 * 
			 * @Override public int compare(FieldMapping o1, FieldMapping o2) {
			 *           return o1.getFieldPosition() - o2.getFieldPosition(); }
			 *           }); mapFieldMappingToVariable.put(key, lisfm); }
			 */

			// putting variable name and id to map, also variable id and
			// length to map
			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// checking if A is available or not
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
//			log.info("firstVar awal ................. " + firstVar.intValue());
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {
				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
//					log.info("lastVar AA " + lastVar.intValue());
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
//					log.info("lastVar AB " + lastVar.intValue());
				} else {
					if(request.getString(3).contains(Constants.TRX_CODE_PAYMENT_ESAMSAT) && mode.equalsIgnoreCase("PAYMENT")){
						lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
						lastVar = lastVar.intValue()+1;
//						log.info("lastVar payment esamsat ................. " + lastVar.intValue());						
					} else {
						lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
//						log.info("lastVar normal ................. " + lastVar.intValue());
					}
				}
//				log.info("firstVar ................. " + firstVar.intValue());
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
//				log.info("numOfRequiredMapping = " + numOfRequiredMapping + " , numOfAvailableMapping = " + numOfAvailableMapping);
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}
//			log.info("mode = " + mode);
			String bit61 = "";
			int x=0;
			if (isComplete) {
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String variableName = (String) iterator.next();
					String variableKey = mapOfVariableNameAndId.get(variableName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(variableKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						if (fieldMapping.getMessageFields().getName().toLowerCase().contains(Constants.VAR_EMPTY)) {
//							log.info("fieldMapping Constants.VAR_EMPTY ................ ");
							if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
								bit61 += specPadding("", "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(), fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
								bit61 += specPadding("", "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
								bit61 += specPadding("", "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
								bit61 += specPadding("", "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							}
						} else {
							if (mode.equalsIgnoreCase("PAYMENT")) {
								if(bit61.equals("")){
									String val = request.getString(Integer
											.valueOf(fieldMapping.getMessageFields().getName().toLowerCase()).intValue());
//									log.info("fieldMapping Constants NOT EMPTY akhir " + val);
									bit61 += val;
								}
							} else {
//								log.info("fieldMapping Constants NOT EMPTY ................ " + bit61);
								String val = request.getString(Integer
										.valueOf(fieldMapping.getMessageFields().getName().toLowerCase()).intValue());
								log.info("fieldMapping Constants NOT EMPTY val " + val);
								/*if(mode.equalsIgnoreCase("INQUIRY_PAYMENT") && val.length() > 18){
									val = val.replace(" ", "");
									log.info("fieldMapping >> 18 " + val);
								} else {*/
									log.info("fieldMapping mapOfVariableIdAndLength " + mapOfVariableIdAndLength.get(variableKey) 
									+ " , getStartAt  = " + fieldMapping.getStartAt() + " , getEndAt = " + fieldMapping.getEndAt());
									val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									log.info("fieldMapping specPadding " + val + " , x = " + x);
									if (request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT) && mode.equalsIgnoreCase("INQUIRY_PAYMENT") && (x>=1)) {
										int y = 32;
										y = y*x;
										x=x+1;
										val = val.substring(y, val.length());
										val = val.trim();
										val = val+"  ";
//										log.info("fieldMapping potong " + val + " , x = " + x + " , y = " + y);
										val = val.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());
//										log.info("fieldMapping Constants NOT EMPTY val substring TRX_CODE_INQ_PAYMENT " + val);
									} else {
										val = val.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());
//										log.info("fieldMapping Constants NOT EMPTY val substring " + val);
										if (request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT)&& mode.equalsIgnoreCase("INQUIRY_PAYMENT")){
											x=x+1;
//											log.info("fieldMapping x = " + x);
										}
									}
										
								//}
								if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
									val = val.trim();
									val = specPadding(val, "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									log.info("fieldMapping Constants NOT EMPTY 1 = " + val);
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
									/*log.info("fieldMapping Constants RPS = " + val);
									if (request.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT) && mode.equalsIgnoreCase("INQUIRY_PAYMENT") && x>1) {
										val = val.substring(0, 32);
										val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										log.info("fieldMapping Constants TRX_CODE_INQ_PAYMENT NOT EMPTY 2 = " + val);
									} else {*/
										val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										log.info("fieldMapping Constants NOT EMPTY 2 = " + val);
									//}
								} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
									val = val.trim();
									val = specPadding(val, "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									log.info("fieldMapping Constants NOT EMPTY 3 = " + val);
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
									val = val.trim();
									val = specPadding(val, "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									log.info("fieldMapping Constants NOT EMPTY 4 = " + val);
								} else {
									throw new ArrayIndexOutOfBoundsException(
											"Padding not available for field " + fieldMapping.getMessageFields().getName());
								}
								log.info("fieldMapping Constants NOT EMPTY akhir " + val);
								bit61 += val;
							}

						}
					}
				}
			}
			return bit61;
		}
		throw new IllegalAccessException("Feature Not Found");
	}

	/**
	 * Pack bit61 from inquiryPayment
	 * 
	 * @param request
	 * @param messageType
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static String packsixtyone(InquiryPaymentRequest request, String messageType)
			throws IllegalArgumentException, IllegalAccessException {
		log.info("packsixtyone InquiryPaymentRequest IN ............");
		//Suwardi
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(
				Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode().trim())), messageType);
		Map<String, List<FieldMapping>> mapFieldMappingToVariable = new HashMap<String, List<FieldMapping>>();
		List<String> listOfVariable = new ArrayList<>();
		if (m != null) {
			Map fieldOfObj = ObjectUtils.unpackFromObject(request);
			Iterator iter = m.getFieldMappingSet().iterator();
			// Putting fieldMapping from message mapping into map, and listing
			// all variable to listOfVariable
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (fm.getTrxType().equalsIgnoreCase("INQUIRY_PAYMENT")) {
					// Checking if fm is request/response
					if (fm.getRequest().equals(Integer.valueOf(1))) {
						// if fm with same variable is exist add it to list in
						// the
						// map
						if (mapFieldMappingToVariable.get(fm.getVariable()) != null) {
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
//							log.info("Putting field " + fm.getMessageFields().getName() + "to variable "
//									+ fm.getVariable());
							// else put it to new entry in map
						} else if (mapFieldMappingToVariable.get(fm.getVariable()) == null) {
							mapFieldMappingToVariable.put(fm.getVariable(), new ArrayList<FieldMapping>());
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							listOfVariable.add(fm.getVariable());
//							log.info("putting new variable " + fm.getVariable());
//							log.info("Putting field " + fm.getMessageFields().getName() + "to variable "
//									+ fm.getVariable());
						}
					}
				}
			}

			/**
			 * false for sorting field mapping in each variable, if multiple
			 * field in one variable is enabled. iter =
			 * mapFieldMappingToVariable.keySet().iterator(); while
			 * (iter.hasNext()) { String key = (String) iter.next();
			 * List<FieldMapping> lisfm = mapFieldMappingToVariable.get(key);
			 * Collections.sort(lisfm, new Comparator<FieldMapping>() {
			 * 
			 * @Override public int compare(FieldMapping o1, FieldMapping o2) {
			 *           return o1.getFieldPosition() - o2.getFieldPosition(); }
			 *           }); mapFieldMappingToVariable.put(key, lisfm); }
			 */

			// putting variable name and id to map, also variable id and
			// length to map
			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				// System.out.println("putting var "+var);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// checking if A is available or not
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {
				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
				} else {
					lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
				}
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}

			String bit61 = "";
			if (isComplete) {
				List<FieldMapping> remapFM = new ArrayList<FieldMapping>();
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String varName = (String) iterator.next();
					String varKey = mapOfVariableNameAndId.get(varName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						fieldMapping.setVariableChar(varName);
						remapFM.add(fieldMapping);
					}
				}
				// sorting remapFM
				Collections.sort(remapFM, new Comparator<FieldMapping>() {
					@Override
					public int compare(FieldMapping o1, FieldMapping o2) {
						return o1.getFieldPosition().compareTo(o2.getFieldPosition());
					}
				});
				for (Iterator iterator = remapFM.iterator(); iterator.hasNext();) {
					FieldMapping fma = (FieldMapping) iterator.next();
					String variableName = fma.getVariableChar();
					String variableKey = mapOfVariableNameAndId.get(variableName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(variableKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						if (fieldMapping.getMessageFields().getName().toLowerCase().contains(Constants.VAR_EMPTY)) {
							if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
								bit61 += specPadding("", "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
								bit61 += specPadding("", "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
								bit61 += specPadding("", "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
								bit61 += specPadding("", "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							}
						} else if (fieldMapping.getMessageFields().getName().toLowerCase().contains("billinfo")) {
							if (fieldMapping.getRequest().equals(Integer.valueOf(1))) {
								String fieldname = fieldMapping.getMessageFields().getName();
								int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
								String billinfo = "";
								// System.out.println(fieldname + " " +
								// billInfoNumber);
								if (billInfoNumber > 0) {
									billinfo = "";
									try {
										billinfo = request.getBillInformation().getBills()[billInfoNumber - 1]
												.getInfo();
									} catch (Exception e) {
										throw new IllegalAccessException(
												"Bill Info " + billInfoNumber + " in Mapping not Found in Request");
									}
								}
								if (billinfo != null && billinfo.length() > 0) {
									log.info(billinfo + " getting var " + variableKey + " "
											+ mapOfVariableIdAndLength.containsKey(variableKey));
									billinfo = specPadding(billinfo, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									log.info(billinfo + " padded " + fieldMapping);
									billinfo = billinfo.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());

									if (!billinfo.equals("")) {
										if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
											billinfo = specPadding(billinfo, "RPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "RPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else {
											throw new ArrayIndexOutOfBoundsException(
													"Padding not available for billinfo_" + billInfoNumber);
										}
										bit61 += billinfo;
									}
								}
							}
						} else {
							if (fieldOfObj.containsKey(fieldMapping.getMessageFields().getName().toLowerCase())) {
								String val = String.valueOf(
										fieldOfObj.get(fieldMapping.getMessageFields().getName().toLowerCase()));
								val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								val = val.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());
								if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
									val = val.trim();
									val = specPadding(val, "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
									val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
									val = val.trim();
									val = specPadding(val, "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
									val = val.trim();
									val = specPadding(val, "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else {
									throw new ArrayIndexOutOfBoundsException("Padding not available for field "
											+ fieldMapping.getMessageFields().getName());
								}
								bit61 += val;
							}
						}
					}

				}
				return bit61;
			}
		}
		throw new IllegalAccessException("Feature Not Found");
	}

	/**
	 * Pack bit61 from purchase
	 * 
	 * @param request
	 * @param messageType
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static String packsixtyone(PurchaseRequest request, String messageType)
			throws IllegalArgumentException, IllegalAccessException {
		log.info("packsixtyone PurchaseRequest IN ............................................");
		//Suwardi
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(
				Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode().trim())), messageType);
		Map<String, List<FieldMapping>> mapFieldMappingToVariable = new HashMap<String, List<FieldMapping>>();
		List<String> listOfVariable = new ArrayList<>();
		if (m != null) {
			Map fieldOfObj = ObjectUtils.unpackFromObject(request);
			Iterator iter = m.getFieldMappingSet().iterator();
			// Putting fieldMapping from message mapping into map, and listing
			// all variable to listOfVariable
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (fm.getTrxType().equalsIgnoreCase("PURCHASE")) {
					// Checking if fm is request/response
					if (fm.getRequest().equals(Integer.valueOf(1))) {
						// if fm with same variable is exist add it to list in
						// the
						// map
						if (mapFieldMappingToVariable.get(fm.getVariable()) != null) {
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
//							log.info("Putting field " + fm.getMessageFields().getName() + "to variable "
//									+ fm.getVariable());
							// else put it to new entry in map
						} else if (mapFieldMappingToVariable.get(fm.getVariable()) == null) {
							mapFieldMappingToVariable.put(fm.getVariable(), new ArrayList<FieldMapping>());
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							listOfVariable.add(fm.getVariable());
//							log.info("putting new variable " + fm.getVariable());
//							log.info("Putting field " + fm.getMessageFields().getName() + "to variable "
//									+ fm.getVariable());
						}
					}
				}
			}

			/**
			 * false for sorting field mapping in each variable, if multiple
			 * field in one variable is enabled. iter =
			 * mapFieldMappingToVariable.keySet().iterator(); while
			 * (iter.hasNext()) { String key = (String) iter.next();
			 * List<FieldMapping> lisfm = mapFieldMappingToVariable.get(key);
			 * Collections.sort(lisfm, new Comparator<FieldMapping>() {
			 * 
			 * @Override public int compare(FieldMapping o1, FieldMapping o2) {
			 *           return o1.getFieldPosition() - o2.getFieldPosition(); }
			 *           }); mapFieldMappingToVariable.put(key, lisfm); }
			 */

			// putting variable name and id to map, also variable id and
			// length to map
			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// checking if A is available or not
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {
				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
				} else {
					lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
				}
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}

			String bit61 = "";
			if (isComplete) {
				List<FieldMapping> remapFM = new ArrayList<FieldMapping>();
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String varName = (String) iterator.next();
					String varKey = mapOfVariableNameAndId.get(varName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						fieldMapping.setVariableChar(varName);
						remapFM.add(fieldMapping);
					}
				}
				// sorting remapFM
				Collections.sort(remapFM, new Comparator<FieldMapping>() {
					@Override
					public int compare(FieldMapping o1, FieldMapping o2) {
						return o1.getFieldPosition().compareTo(o2.getFieldPosition());
					}
				});
				
				for (Iterator iterator = remapFM.iterator(); iterator.hasNext();) {
					FieldMapping fma = (FieldMapping) iterator.next();
					String variableName = fma.getVariableChar();
					String variableKey = mapOfVariableNameAndId.get(variableName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(variableKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						if (fieldMapping.getMessageFields().getName().toLowerCase().contains(Constants.VAR_EMPTY)) {
							if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
								bit61 += specPadding("", "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
								bit61 += specPadding("", "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
								bit61 += specPadding("", "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
								bit61 += specPadding("", "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							}
						} else if (fieldMapping.getMessageFields().getName().toLowerCase().contains("billinfo")) {
							if (fieldMapping.getRequest().equals(Integer.valueOf(1))) {
								String fieldname = fieldMapping.getMessageFields().getName();
								int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
								String billinfo = "";
								// System.out.println(fieldname + " " +
								// billInfoNumber);
								if (billInfoNumber > 0) {
									billinfo = "";
									try {
										billinfo = request.getBillInformation().getBills()[billInfoNumber - 1]
												.getInfo();
									} catch (Exception e) {
										throw new IllegalAccessException(
												"Bill Info " + billInfoNumber + " in Mapping not Found in Request");
									}
								}
								if (billinfo != null && billinfo.length() > 0) {
									billinfo = specPadding(billinfo, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									billinfo = billinfo.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());

									if (!billinfo.equals("")) {
										if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
											billinfo = specPadding(billinfo, "RPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "RPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else {
											throw new ArrayIndexOutOfBoundsException(
													"Padding not available for billinfo_" + billInfoNumber);
										}
										bit61 += billinfo;
									}
								}
							}
						} else {
							if (fieldOfObj.containsKey(fieldMapping.getMessageFields().getName().toLowerCase())) {
								String val = String.valueOf(
										fieldOfObj.get(fieldMapping.getMessageFields().getName().toLowerCase()));
								val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								val = val.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());
								if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
									val = val.trim();
									val = specPadding(val, "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
									val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
									val = val.trim();
									val = specPadding(val, "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
									val = val.trim();
									val = specPadding(val, "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else {
									throw new ArrayIndexOutOfBoundsException("Padding not available for field "
											+ fieldMapping.getMessageFields().getName());
								}
								bit61 += val;
							}
						}
					}

				}
				return bit61;
			}
		}
		throw new IllegalAccessException("Feature Not Found");
	}

	/**
	 * Pack bit61 from payment
	 * 
	 * @param request
	 * @param messageType
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static String packsixtyone(PaymentRequest request, String messageType)
			throws IllegalArgumentException, IllegalAccessException {
//		log.info("packsixtyone PaymentRequest IN .....................................");
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(
				Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode().trim())), messageType);
		Map<String, List<FieldMapping>> mapFieldMappingToVariable = new HashMap<String, List<FieldMapping>>();
		List<String> listOfVariable = new ArrayList<>();
		if (m != null) {
			Map fieldOfObj = ObjectUtils.unpackFromObject(request);
			Iterator iter = m.getFieldMappingSet().iterator();
			// Putting fieldMapping from message mapping into map, and listing
			// all variable to listOfVariable
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (fm.getTrxType().equalsIgnoreCase("PAYMENT")) {
					// Checking if fm is request/response
					if (fm.getRequest().equals(Integer.valueOf(1))) {
						// if fm with same variable is exist add it to list in
						// the
						// map
						if (mapFieldMappingToVariable.get(fm.getVariable()) != null) {
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							// else put it to new entry in map
						} else if (mapFieldMappingToVariable.get(fm.getVariable()) == null) {
							mapFieldMappingToVariable.put(fm.getVariable(), new ArrayList<FieldMapping>());
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							listOfVariable.add(fm.getVariable());
						}
					}
				}
			}

			/**
			 * false for sorting field mapping in each variable, if multiple
			 * field in one variable is enabled. iter =
			 * mapFieldMappingToVariable.keySet().iterator(); while
			 * (iter.hasNext()) { String key = (String) iter.next();
			 * List<FieldMapping> lisfm = mapFieldMappingToVariable.get(key);
			 * Collections.sort(lisfm, new Comparator<FieldMapping>() {
			 * 
			 * @Override public int compare(FieldMapping o1, FieldMapping o2) {
			 *           return o1.getFieldPosition() - o2.getFieldPosition(); }
			 *           }); mapFieldMappingToVariable.put(key, lisfm); }
			 */

			// putting variable name and id to map, also variable id and
			// length to map
			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// Checking if mapping is complete or not
			// checking if AA/AB is available then moved it back to end of list
			// is available
			if (listOfSortedVariable.contains("AA")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AA"));
				listOfSortedVariable.add("AA");
			}
			if (listOfSortedVariable.contains("AB")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AB"));
				listOfSortedVariable.add("AB");
			}
			// checking if A is first mapped variable
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {
				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
				} else {
					lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
				}
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}

			String bit61 = "";
			if (isComplete) {
				List<FieldMapping> remapFM = new ArrayList<FieldMapping>();
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String varName = (String) iterator.next();
					String varKey = mapOfVariableNameAndId.get(varName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						fieldMapping.setVariableChar(varName);
						remapFM.add(fieldMapping);
					}
				}
				// sorting remapFM
				Collections.sort(remapFM, new Comparator<FieldMapping>() {
					@Override
					public int compare(FieldMapping o1, FieldMapping o2) {
						return o1.getFieldPosition().compareTo(o2.getFieldPosition());
					}
				});
				
				for (Iterator iterator = remapFM.iterator(); iterator.hasNext();) {
					FieldMapping fma =  (FieldMapping)iterator.next();
					String variableName = fma.getVariableChar();
					String variableKey = mapOfVariableNameAndId.get(variableName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(variableKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
//						log.info("Bit61 ["+bit61.length()+"]"+bit61);
						if (fieldMapping.getMessageFields().getName().toLowerCase().contains(Constants.VAR_EMPTY)) {
							if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
								bit61 += specPadding("", "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
								bit61 += specPadding("", "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
								bit61 += specPadding("", "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
								bit61 += specPadding("", "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
							}
						} else if (fieldMapping.getMessageFields().getName().toLowerCase().contains("billinfo")) {
							if (fieldMapping.getRequest().equals(Integer.valueOf(1))) {
								String fieldname = fieldMapping.getMessageFields().getName();
								int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
								String billinfo = "";
								// System.out.println(fieldname + " " +
								// billInfoNumber);
								if (billInfoNumber > 0) {
									billinfo = "";
									try {
										billinfo = request.getBillInformation().getBills()[billInfoNumber - 1]
												.getInfo();
									} catch (Exception e) {
										throw new IllegalAccessException(
												"Bill Info " + billInfoNumber + " in Mapping not Found in Request");
									}
								}
								if (billinfo != null && billinfo.length() > 0) {
									billinfo = specPadding(billinfo, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
									billinfo = billinfo.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());

									if (!billinfo.equals("")) {
										if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
											billinfo = specPadding(billinfo, "RPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "LPS",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
											billinfo = billinfo.trim();
											billinfo = specPadding(billinfo, "RPZ",
													mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
										} else {
											throw new ArrayIndexOutOfBoundsException(
													"Padding not available for billinfo_" + billInfoNumber);
										}
										log.info(variableName+" : ["+billinfo.length()+"] "+billinfo);
										bit61 += billinfo;
										log.info("Bit61 AF ["+bit61.length()+"]"+bit61);
										
									}
								}
							}
						} else {
							if (fieldOfObj.containsKey(fieldMapping.getMessageFields().getName().toLowerCase())) {
								String val = String.valueOf(
										fieldOfObj.get(fieldMapping.getMessageFields().getName().toLowerCase()));
								val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								val = val.substring(fieldMapping.getStartAt(), fieldMapping.getEndAt());
								if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
									val = val.trim();
									val = specPadding(val, "LPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
									val = specPadding(val, "RPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
									val = val.trim();
									val = specPadding(val, "LPS", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
									val = val.trim();
									val = specPadding(val, "RPZ", mapOfVariableIdAndLength.get(variableKey).intValue(),fieldMapping.getStartAt(), fieldMapping.getEndAt());
								} else {
									throw new ArrayIndexOutOfBoundsException("Padding not available for field "
											+ fieldMapping.getMessageFields().getName());
								}
								bit61 += val;
							}
						}
					}

				}
				return bit61;
			}
		}
		throw new IllegalAccessException("Feature Not Found");
	}

	/**
	 * Padding content based on mode
	 * 
	 * @param content
	 * @param mode[RPS,RPZ,LPS,LPZ]
	 * @param length
	 * @return
	 */
	public static String specPadding(String content, String mode, int maxlength, int start, int end ) {
		if (mode.equalsIgnoreCase("RPS")) {
			return CommonUtils.padright(content, validateLength(maxlength, start, end), ' ');
		} else if (mode.equalsIgnoreCase("RPZ")) {
			return CommonUtils.padright(content, validateLength(maxlength, start, end), '0');
		} else if (mode.equalsIgnoreCase("LPS")) {
			return CommonUtils.padleft(content, validateLength(maxlength, start, end), ' ');
		} else if (mode.equalsIgnoreCase("LPZ")) {
			return CommonUtils.padleft(content, validateLength(maxlength, start, end), '0');
		} else
			return content;
	}
	
	public static int validateLength (int maxlength, int start, int end) {
		int length = end - start;
		if (maxlength < length) return maxlength;
		else return length;
	}

	/**
	 * Unpadding content based on mode
	 * 
	 * @param content
	 * @param mode["RPS,RPZ,LPS,LPZ]
	 * @return
	 */
	public static String specUnpadding(String content, String mode) {
		if (mode.equalsIgnoreCase("RPS")) {
			return CommonUtils.unPadRight(content, ' ');
		} else if (mode.equalsIgnoreCase("RPZ")) {
			return CommonUtils.unPadRight(content, '0');
		} else if (mode.equalsIgnoreCase("LPS")) {
			return CommonUtils.unPadLeft(content, ' ');
		} else if (mode.equalsIgnoreCase("LPZ")) {
			return CommonUtils.unPadLeft(content, '0');
		} else
			return content;
	}

	/**
	 * Unpack bit61 for web service transaction
	 * 
	 * @param response
	 * @param messageType
	 * @return Bills[]
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("rawtypes")
	public static Bills[] unpacksixtyone(ISOMsg response, String messageType) throws IllegalAccessException {
		log.info("UNPACKING 61");
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(Integer.valueOf(response.getString("60").trim()),
				messageType);
		//log.info("MM : " + m);
		if (m != null) {
			String mode = "";
			if (response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_SIPANDU)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_PDAM)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_EDUPAY)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_ESAMSAT)) {
				mode = "INQUIRY_PAYMENT";
			} else if (response.getString(3).contains(Constants.TRX_CODE_PAYMENT)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_SIPANDU)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_PDAM)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_EDUPAY)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_ESAMSAT)) {
				mode = "PAYMENT";
			} else if (response.getString(3).contains(Constants.TRX_CODE_PURCHASE)) {
				mode = "PURCHASE";
			}
			log.info("messagemapping id " + m.getId() + " with " + m.getFieldMappingSet().size() + " mapping");
			Map<String, List<FieldMapping>> mapFieldMappingToVariable = new HashMap<String, List<FieldMapping>>();
			List<String> listOfVariable = new ArrayList<>();
			Iterator iter = m.getFieldMappingSet().iterator();
			// Map fieldOfObj = ObjectUtils.unpackFromIso(response);
			int numOfBills = 0;
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (mode != "" && fm.getTrxType().equalsIgnoreCase(mode)) {
					if (fm.getResponse().intValue() == 1) {
						if (fm.getMessageFields().getName().toLowerCase().contains("billinfo")) {
							numOfBills++;
						}
						if (mapFieldMappingToVariable.get(fm.getVariable()) != null) {
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							// System.out.println("adding
							// "+fm.getMessageFields().getName()+" to var
							// "+fm.getVariable());

							// else put it to new entry in map
						} else if (mapFieldMappingToVariable.get(fm.getVariable()) == null) {
							mapFieldMappingToVariable.put(fm.getVariable(), new ArrayList<FieldMapping>());
							mapFieldMappingToVariable.get(fm.getVariable()).add(fm);
							listOfVariable.add(fm.getVariable());
							//log.info("adding new var " + fm.getVariable());
							//log.info("adding " + fm.getMessageFields().getName() + " to var " + fm.getVariable());
						}
					}
				}
			}
			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				//log.info("getting " + var);
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				//log.info("adding " + variable);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// checking if AA/AB is available then moved it back to end of list
			// is available
			if (listOfSortedVariable.contains("AA")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AA"));
				listOfSortedVariable.add("AA");
			}
			if (listOfSortedVariable.contains("AB")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AB"));
				listOfSortedVariable.add("AB");
			}
			// checking if A is first mapped variable
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {
				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
					
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
					
				} else {
					lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
					
				}
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
				
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
					
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}
			Bills[] bills = null;
			if (isComplete) {
				String bit61 = response.getString(61);
				// int numOfBills = m.getFieldMappingSet().size();
				bills = new Bills[numOfBills];
				
				List<FieldMapping> remapFM = new ArrayList<FieldMapping>();
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String varName = (String) iterator.next();
					String varKey = mapOfVariableNameAndId.get(varName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
						fieldMapping.setVariableChar(varName);
						remapFM.add(fieldMapping);
					}
				}
				// sorting remapFM
				Collections.sort(remapFM, new Comparator<FieldMapping>() {
					@Override
					public int compare(FieldMapping o1, FieldMapping o2) {
						return o1.getFieldPosition().compareTo(o2.getFieldPosition());
					}
				});
				
				for (Iterator iterator = remapFM.iterator(); iterator.hasNext();) {
					FieldMapping fma = (FieldMapping)iterator.next();
					String varName = fma.getVariableChar();
					String varKey = mapOfVariableNameAndId.get(varName);
					List<FieldMapping> listOfFieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					for (Iterator iterator2 = listOfFieldMappingOfVariable.iterator(); iterator2.hasNext();) {	
						FieldMapping fieldMapping = (FieldMapping) iterator2.next();
//						System.out.println("cek : " + fieldMapping.getFieldPosition());
						if (fieldMapping.getMessageFields().getName().toLowerCase().contains("billinfo")) {
							String fieldname = fieldMapping.getMessageFields().getName();
							int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
//							log.info("bit 61 now [" + bit61.length() + "] : " + bit61);
							String billinfo = "";
							try {
								/*
								 * OLD SAMS
								 */
								//billinfo = bit61.substring(0, mapOfVariableIdAndLength.get(varKey));
								//bit61 = bit61.substring(mapOfVariableIdAndLength.get(varKey));
								
								/*
								 * NEW SAMS
								 */
								billinfo = bit61.substring(0, validateLength(mapOfVariableIdAndLength.get(varKey),fieldMapping.getStartAt(), fieldMapping.getEndAt()));
								bit61 = bit61.substring(validateLength(mapOfVariableIdAndLength.get(varKey),fieldMapping.getStartAt(), fieldMapping.getEndAt()));

//								log.info("-------------------");
//								log.info("billinfo" + billinfo);
//								log.info("length" + validateLength(mapOfVariableIdAndLength.get(varKey),fieldMapping.getStartAt(), fieldMapping.getEndAt()));
//								log.info("bit61 : "+bit61);
								if (fieldMapping.getPadding().equalsIgnoreCase("LPZ")) {
									billinfo = specUnpadding(billinfo, "LPZ");
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPS")) {
									billinfo = specUnpadding(billinfo, "RPS");
								} else if (fieldMapping.getPadding().equalsIgnoreCase("LPS")) {
									billinfo = specUnpadding(billinfo, "LPS");
								} else if (fieldMapping.getPadding().equalsIgnoreCase("RPZ")) {
									billinfo = specUnpadding(billinfo, "RPZ");
								} else {
									throw new ArrayIndexOutOfBoundsException("Padding not available for field "
											+ fieldMapping.getMessageFields().getName());
								}
								System.out.println(varName+"["+billinfo.length()+"] : "+billinfo);
							} catch (Exception e) {
								throw new IllegalAccessError("Mapping Data To Bills Error.");
							}
							Bills bill = new Bills();
							bill.setInfo(billinfo);
							bills[billInfoNumber - 1] = bill;
						} else {
							String msg = bit61.substring(0,validateLength(mapOfVariableIdAndLength.get(varKey),fieldMapping.getStartAt(), fieldMapping.getEndAt()));
							bit61 = bit61.substring(validateLength(mapOfVariableIdAndLength.get(varKey),fieldMapping.getStartAt(), fieldMapping.getEndAt()));
							log.warn(varName+"["+msg.length()+"] : "+msg+";");
						}
					}
				}
			}
			return bills;
		}
		return null;
	}

	/**
	 * Unpack bit61 for ISO transaction
	 * 
	 * @param response
	 * @return
	 * @throws IllegalAccessException
	 * @throws ISOException
	 */
	@SuppressWarnings("rawtypes")
	public static ISOMsg unpacksixtyone(ISOMsg response) throws IllegalAccessException, ISOException {
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(Integer.valueOf(response.getString("60")), "ISO");
		if (m != null) {
			String mode = "";
			if (response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_SIPANDU)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_EDUPAY)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_ESAMSAT)||
					response.getString(3).contains(Constants.TRX_CODE_INQ_PAYMENT_PDAM)) {
				mode = "INQUIRY_PAYMENT";
			} else if (response.getString(3).contains(Constants.TRX_CODE_PAYMENT)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_SIPANDU)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_EDUPAY)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_ESAMSAT)||
					response.getString(3).contains(Constants.TRX_CODE_PAYMENT_PDAM)) {
				mode = "PAYMENT";
			} else if (response.getString(3).contains(Constants.TRX_CODE_PURCHASE)) {
				mode = "PURCHASE";
			}
			log.info("messagemapping id " + m.getId() + " with " + m.getFieldMappingSet().size() + " mapping");
			Map<String, List<FieldMapping>> mapFieldMappingToIndex = new HashMap<String, List<FieldMapping>>();
			List<String> listOfIndex = new ArrayList<>();
			List<String> listOfVariable = new ArrayList<>();
			Map<String, FieldMapping> mapFieldMappingToVariable = new HashMap<String, FieldMapping>();
			Map<String, List<MsgLocation>> mapChangedToIndex = new HashMap<String, List<MsgLocation>>();
			Iterator iter = m.getFieldMappingSet().iterator();
			// Map fieldOfObj = ObjectUtils.unpackFromIso(response);
			while (iter.hasNext()) {
				FieldMapping fm = (FieldMapping) iter.next();
				if (mode != "" && fm.getTrxType().equalsIgnoreCase(mode)) {
					if (fm.getResponse().intValue() == 1) {
						if (mapFieldMappingToIndex.get(fm.getMessageFields().getName()) != null) {
							mapFieldMappingToIndex.get(fm.getMessageFields().getName()).add(fm);
							if (!listOfVariable.contains(fm.getVariable())) {
								listOfVariable.add(fm.getVariable());
							}
							mapFieldMappingToVariable.put(fm.getVariable(), fm);
							// System.out.println("adding
							// "+fm.getMessageFields().getName()+" to var
							// "+fm.getVariable());

							// else put it to new entry in map
						} else if (mapFieldMappingToIndex.get(fm.getMessageFields().getName()) == null) {
							mapFieldMappingToIndex.put(fm.getMessageFields().getName(), new ArrayList<FieldMapping>());
							mapFieldMappingToIndex.get(fm.getMessageFields().getName()).add(fm);
							listOfIndex.add(fm.getMessageFields().getName());
							mapChangedToIndex.put(fm.getMessageFields().getName(), new ArrayList<MsgLocation>());
							if (!listOfVariable.contains(fm.getVariable())) {
								listOfVariable.add(fm.getVariable());
							}
							mapFieldMappingToVariable.put(fm.getVariable(), fm);
							//log.info("adding new index " + fm.getMessageFields().getName());
							//log.info("adding " + fm.getVariable() + " to var " + fm.getMessageFields().getName());
						}
					}
				}
			}

			List<String> listOfSortedVariable = new ArrayList<>();
			Map<String, String> mapOfVariableNameAndId = new HashMap<>();
			Map<String, Integer> mapOfVariableIdAndLength = new HashMap<>();
			for (Iterator iterator = listOfVariable.iterator(); iterator.hasNext();) {
				String var = (String) iterator.next();
				//log.info("getting " + var);
				MessageFields mf = ForBitSixtyOneMessageField.getFieldById("61", Integer.valueOf(var));
				String variable = mf.getName();
				mapOfVariableNameAndId.put(variable, var);
				listOfSortedVariable.add(variable);
				//log.info("adding " + variable);
				mapOfVariableIdAndLength.put(var, Integer.valueOf(mf.getDescription()));
			}

			// sorting listOfSortedVariable
			Collections.sort(listOfSortedVariable, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});

			boolean isComplete = false;
			// Checking if mapping is complete or not
			// checking if AA/AB is available then moved it back to end of list
			// is available
			if (listOfSortedVariable.contains("AA")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AA"));
				listOfSortedVariable.add("AA");
			}
			if (listOfSortedVariable.contains("AB")) {
				listOfSortedVariable.remove(listOfSortedVariable.indexOf("AB"));
				listOfSortedVariable.add("AB");
			}
			// checking if A is first mapped variable
			Integer firstVar = Integer.valueOf(listOfSortedVariable.get(0), 36);
			// converting A to base 36 (10 decimal)
			if (firstVar.intValue() == 10) {

				Integer lastVar;
				if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AA")) {
					lastVar = Integer.valueOf("10", 36);
				} else if (listOfSortedVariable.get(listOfSortedVariable.size() - 1).equals("AB")) {
					lastVar = Integer.valueOf("11", 36);
				} else {
					lastVar = Integer.valueOf(listOfSortedVariable.get(listOfSortedVariable.size() - 1), 36);
				}
				// Checking if all mapping is available
				int numOfRequiredMapping = lastVar.intValue() - firstVar.intValue() + 1;
				int numOfAvailableMapping = listOfSortedVariable.size();
				
				if (numOfAvailableMapping == numOfRequiredMapping) {
					isComplete = true;
				} else {
					isComplete = false;
					throw new IllegalAccessException("Mapping Variable incomplete, Missing "
							+ (numOfRequiredMapping - numOfAvailableMapping) + " mappings");
				}
			} else {
				throw new IllegalAccessException("Variable A not found in Mapping Data");
			}
			ISOMsg responseTo = (ISOMsg) response.clone();
			if (isComplete) {
				String bit61 = response.getString(61);
				// int numOfBills = m.getFieldMappingSet().size();

				List<FieldMapping> remapFM = new ArrayList<FieldMapping>();
				for (Iterator iterator = listOfSortedVariable.iterator(); iterator.hasNext();) {
					String varName = (String) iterator.next();
					String varKey = mapOfVariableNameAndId.get(varName);
					FieldMapping fieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					fieldMappingOfVariable.setVariableChar(varName);
					remapFM.add(fieldMappingOfVariable);
				}
				// sorting remapFM
				Collections.sort(remapFM, new Comparator<FieldMapping>() {
					@Override
					public int compare(FieldMapping o1, FieldMapping o2) {
						return o1.getFieldPosition().compareTo(o2.getFieldPosition());
					}
				});
				
				for (Iterator iterator = remapFM.iterator(); iterator.hasNext();) {
					FieldMapping fma = (FieldMapping)iterator.next();
					String varName = fma.getVariableChar();
					String varKey = mapOfVariableNameAndId.get(varName);
					FieldMapping fieldMappingOfVariable = mapFieldMappingToVariable.get(varKey);
					String msg = "";
					if (Integer.valueOf(fieldMappingOfVariable.getMessageFields().getName()).intValue() > 0) {
						/*
						 *
						 * OLD SAMS
						 */
						//msg = bit61.substring(0, mapOfVariableIdAndLength.get(fieldMappingOfVariable.getVariable()));
						//bit61 = bit61.substring(mapOfVariableIdAndLength.get(fieldMappingOfVariable.getVariable()));
						/*
						 *
						 * NEW SAMS
						 */
						msg = bit61.substring(0, validateLength(mapOfVariableIdAndLength.get(fieldMappingOfVariable.getVariable()),fieldMappingOfVariable.getStartAt(), fieldMappingOfVariable.getEndAt()));
						bit61 = bit61.substring(validateLength(mapOfVariableIdAndLength.get(fieldMappingOfVariable.getVariable()),fieldMappingOfVariable.getStartAt(), fieldMappingOfVariable.getEndAt()));
						mapChangedToIndex.get(fieldMappingOfVariable.getMessageFields().getName())
								.add(new MsgLocation(msg, fieldMappingOfVariable.getFieldPosition(),
										fieldMappingOfVariable.getStartAt(), fieldMappingOfVariable.getEndAt(),
										fieldMappingOfVariable.getPadding(), fieldMappingOfVariable.getVariable()));
					}
				}

				iter = mapChangedToIndex.keySet().iterator();
				while (iter.hasNext()) {
					String key = (String) iter.next();
					List<MsgLocation> lisfm = mapChangedToIndex.get(key);
					Collections.sort(lisfm, new Comparator<MsgLocation>() {

						@Override
						public int compare(MsgLocation o1, MsgLocation o2) {
							return o1.getLoc().intValue() - o2.getLoc().intValue();
						}
					});
				}

				iter = mapChangedToIndex.keySet().iterator();
				while (iter.hasNext()) {
					String field = (String) iter.next();
					if (Integer.valueOf(field).intValue() > 0) {
						try {
							String msg = "";
							List<MsgLocation> infos = mapChangedToIndex.get(field);
							for (Iterator iterator = infos.iterator(); iterator.hasNext();) {
								MsgLocation msgLocation = (MsgLocation) iterator.next();
								String msgTemp = msgLocation.getMsg();
								if (msgLocation.getPadding().equalsIgnoreCase("LPZ")) {
									msgTemp = specUnpadding(msgTemp, "LPZ");
								} else if (msgLocation.getPadding().equalsIgnoreCase("RPS")) {
									msgTemp = specUnpadding(msgTemp, "RPS");
								} else if (msgLocation.getPadding().equalsIgnoreCase("LPS")) {
									msgTemp = specUnpadding(msgTemp, "LPS");
								} else if (msgLocation.getPadding().equalsIgnoreCase("RPZ")) {
									msgTemp = specUnpadding(msgTemp, "RPZ");
								} else {
									throw new ArrayIndexOutOfBoundsException(
											"Padding not available for field " + field);
								}
								msgTemp = specPadding(msgTemp, "RPS",
										mapOfVariableIdAndLength.get(msgLocation.getVar()), msgLocation.getSt(), msgLocation.getEnd());
								msgTemp = msgTemp.substring(msgLocation.getSt().intValue(),
										msgLocation.getEnd().intValue());
								msgTemp = msgTemp.trim();
								msg += msgTemp;
							}
							log.info("Setting " + msg + " to bit " + field);
							responseTo.set(Integer.valueOf(field).intValue(), msg);
						} catch (NumberFormatException e) {
							throw new NumberFormatException("Error Index in Mapping ISO Msg response.");
						} catch (ISOException e) {
							throw new ISOException("Mapping invalid data");
						} catch (StringIndexOutOfBoundsException e) {
							throw new StringIndexOutOfBoundsException("Mapping data have more field than response");
						}
					} else {
						String msg = "";
						List<MsgLocation> infos = mapChangedToIndex.get(field);
						for (Iterator iterator = infos.iterator(); iterator.hasNext();) {
							MsgLocation msgLocation = (MsgLocation) iterator.next();
							msg += msgLocation.getMsg();
						}
						log.info("to trash : " + msg);
					}
				}

			}
			return responseTo;
		}
		return null;

	}

	public static List unpack(InquiryPaymentRequest request, String bit61, String messageType)
			throws IllegalArgumentException, IllegalAccessException {
		//Suwardi
		MessageMapping m = ForBitSixtyOneMapping.getMessageMapping(
				Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode().trim())), messageType);
		if (m != null) {
			List<Bills> bills = new ArrayList<>();
			Iterator iter = m.getFieldMappingSet().iterator();
			while (iter.hasNext()) {
				Map fieldObj = ObjectUtils.unpackFromObject(request);
				FieldMapping f = (FieldMapping) iter.next();
				if (f.getMessageFields().getName().toLowerCase().contains("billinfo")) {
					String fieldname = f.getMessageFields().getName();
					int billInfoNumber = Integer.valueOf(fieldname.substring(fieldname.indexOf("_") + 1));
//					log.info("billInfoNumber = " + billInfoNumber);
					String billinfo = "";
					// System.out.println(fieldname + " " + billInfoNumber);
					if (billInfoNumber > 0) {
//						log.info("bit61 : " + bit61);
						Bills bill = new Bills();
						String bit61unpadded = CommonUtils.unPadLeft(bit61, ' ');
//						log.info("unpad : " + bit61unpadded);
//						log.info("unpad substring Start : " + f.getStartAt()+ " , endAt = "+ f.getEndAt());
//						log.info("unpad setInfo : " + bit61unpadded.substring(f.getStartAt(), f.getEndAt()));
						bill.setInfo(bit61unpadded.substring(f.getStartAt(), f.getEndAt()));
						bills.add(bill);
						bit61 = bit61unpadded.substring(f.getEndAt());
					}
				} else {
					if (fieldObj.containsKey(f.getMessageFields().getName().toLowerCase())) {
						String bit61unpadded = CommonUtils.unPadLeft(bit61, ' ');
//						log.info("bit61unpadded = " + bit61unpadded);
//						log.info("bit61unpadded substring Start : " + f.getStartAt()+ " , endAt = "+ f.getEndAt());
						String val = bit61unpadded.substring(f.getStartAt(), f.getEndAt());
//						log.info("no " + val);
						bit61 = bit61unpadded.substring(f.getEndAt());
//						log.info("61 now : " + bit61);
					}
				}
			}
			return bills;
		}
		return null;
	}
}
