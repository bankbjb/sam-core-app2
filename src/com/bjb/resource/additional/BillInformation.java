package com.bjb.resource.additional;

import java.io.Serializable;

/**
 * 
 * @author satrio
 * 
 * Goal			: Create helper field as input on purchase request
 * Create Date	: Sep 3, 2016
 */
public class BillInformation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6378749925326777282L;
	/**
	 * using array because jax rpc 1.1 supports array, not list.
	 */
	private Bills[] bills;

	public Bills[] getBills() {
		return bills;
	}

	public void setBills(Bills[] bills) {
		this.bills = bills;
	}

}
