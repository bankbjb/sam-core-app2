package com.bjb.resource.additional;

import java.io.Serializable;

import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.cache.ForBitFiftySevenMapping;
import com.bjb.model.CommissionTiering;
import com.bjb.resource.main.InquiryPaymentRequest;
import com.bjb.resource.main.InquiryPaymentResponse;
import com.bjb.resource.main.PaymentRequest;
import com.bjb.resource.main.PurchaseRequest;
import com.bjb.util.CommonUtils;

/**
 * for bit 57 helper
 * 
 * @author arifino
 *
 */
public class AmountInformation implements Serializable {
	/**
	 * 
	 */
	
	private static Logger log = LoggerFactory.getLogger(AmountInformation.class);
	private static final long serialVersionUID = -1141514729840556571L;
	private String billAmount;
	private String feeBJB;
	private String feeAggregator;
	private String totalAmount;

	public String getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public String getFeeBJB() {
		return feeBJB;
	}

	public void setFeeBJB(String feeBJB) {
		this.feeBJB = feeBJB;
	}

	public String getFeeAggregator() {
		return feeAggregator;
	}

	public void setFeeAggregator(String feeAggregator) {
		this.feeAggregator = feeAggregator;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * Check if input is null
	 * 
	 * @param in
	 * @return
	 */
	public boolean checkIfNull(String in) {
		try {
			if (in.equals(null)) {
				return true;
			} else if ((in.equals("0")) || (in.equals("")) || (in.equals("?"))) {
				return true;
			} else
				return false;
		} catch (Exception e) {
			return true;
		}

	}

	/**
	 * Pack the object to bit 57 iso
	 * 
	 * @param o
	 * @return
	 */
	public String pack(Object o) {
		String packed = "";
		if (checkIfNull(billAmount)) {
			billAmount = "0";
		}
		packed += ISOUtil.zeropad(Long.parseLong(billAmount), 12);
		if ((checkIfNull(feeBJB)) && (checkIfNull(feeAggregator))) {
			fillInformation(o);

		}
		packed += ISOUtil.zeropad(Long.parseLong(feeBJB), 12);
		packed += ISOUtil.zeropad(Long.parseLong(feeAggregator), 12);
//		if (checkIfNull(totalAmount)) {
			long total = Long.parseLong(billAmount) + Long.parseLong(feeBJB) + Long.parseLong(feeAggregator);
			this.totalAmount = total + "";
//		}
		
		packed += ISOUtil.zeropad(Long.parseLong(totalAmount), 12);
		return packed;
	}

	/**
	 * For testing : pack without checking db/cache
	 * 
	 * @return
	 */
	public String pack() {
		String packed = "";
		if (checkIfNull(billAmount)) {
			billAmount = "0";
		}
		
		packed += ISOUtil.zeropad(Long.parseLong(billAmount), 12);
		if (checkIfNull(feeBJB)) {
			feeBJB = "0";
		}
		packed += ISOUtil.zeropad(Long.parseLong(feeBJB), 12);
		if (checkIfNull(feeAggregator)) {
			feeAggregator = "0";
		}
		
		packed += ISOUtil.zeropad(Long.parseLong(feeAggregator), 12);
//		if (checkIfNull(totalAmount)) {
			long total = Long.parseLong(billAmount) + Long.parseLong(feeBJB) + Long.parseLong(feeAggregator);
			this.totalAmount = total + "";
//		}
		packed += ISOUtil.zeropad(Long.parseLong(totalAmount), 12);
		return packed;
	}

	/**
	 * fill information about amount information from cache/db
	 * 
	 * @param o
	 */
	public void fillInformation(Object o) {
		try {
			if (o instanceof PaymentRequest) {
				PaymentRequest ipr = (PaymentRequest) o;
				// TODO still raw fitur id not kode
				// String key = new
				// Integer(ipr.getAggregatorCode().substring(1)), new
				// Integer(ipr.getCaCode().substring(2)), new
				// Integer(ipr.getFeatureCode().substring(1)),
				// Long.parseLong(billAmount)
				// CommissionTiering m =
				// ForBitFiftySevenMapping.getCommissionTiering(Integer.valueOf(ipr.getAggregatorCode().substring(1)),
				// Integer.valueOf(ipr.getCaCode().substring(2)),
				// Integer.valueOf(ipr.getFeatureCode().substring(1)),
				// Long.parseLong(billAmount));
				CommissionTiering m = ForBitFiftySevenMapping.getCommissionTiering(
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getAggregatorCode().trim())), Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getCaCode())),
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getFeatureCode().trim())), Long.parseLong(billAmount));

				if (m != null) {
					log.info("Commision Tiering Found");
					feeBJB = String.valueOf(m.getCommissionBjb());
					feeAggregator = String.valueOf(m.getCommisionAggregator());
				} else {
					log.info("Commision Tiering Not Found");
					feeBJB = "0";
					feeAggregator = "0";
				}
			} else if (o instanceof PurchaseRequest) {
				PurchaseRequest ipr = (PurchaseRequest) o;
				// TODO still raw fitur id not fitur kode
				CommissionTiering m = ForBitFiftySevenMapping.getCommissionTiering(
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getAggregatorCode())), Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getCaCode())),
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getFeatureCode())), Long.parseLong(billAmount));
				if (m != null) {
					log.info("Commision Tiering Found");
					feeBJB = m.getCommissionBjb() + "";
					feeAggregator = m.getCommisionAggregator() + "";
				} else {
					log.info("Commision Tiering Not Found");
					feeBJB = "0";
					feeAggregator = "0";
				}
			} else if (o instanceof InquiryPaymentRequest) {
					System.out.println("FILLING IPR");
					InquiryPaymentRequest ipr = (InquiryPaymentRequest) o;
					// TODO still raw fitur id not fitur kode
					CommissionTiering m = ForBitFiftySevenMapping.getCommissionTiering(
							Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getAggregatorCode().trim())), Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getCaCode())),
							Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getFeatureCode().trim())), Long.parseLong(billAmount));
					if (m != null) {
						log.info("Commision Tiering Found");
						feeBJB = m.getCommissionBjb() + "";
						feeAggregator = m.getCommisionAggregator() + "";
					} else {
						log.info("Commision Tiering Not Found");
						feeBJB = "0";
						feeAggregator = "0";
					}
			} else if (o instanceof InquiryPaymentResponse) {
				InquiryPaymentResponse ipr = (InquiryPaymentResponse) o;
				// TODO still raw fitur id not fitur kode
				CommissionTiering m = ForBitFiftySevenMapping.getCommissionTiering(
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getAggregatorCode().trim())), Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getCaCode())),
						Integer.valueOf(CommonUtils.removeUntilNumber(ipr.getFeatureCode().trim())), Long.parseLong(billAmount));
				if (m != null) {
					log.info("Commision Tiering Found");
					feeBJB = m.getCommissionBjb() + "";
					feeAggregator = m.getCommisionAggregator() + "";
					log.info("=========================================");
					log.info("feeBJB");
					log.info("feeAggregator");
					
				} else {
					log.info("Commision Tiering Not Found");
					feeBJB = "0";
					feeAggregator = "0";
				}
			} else {
				feeBJB = "0";
				feeAggregator = "0";
			}
		} catch (Exception e) {
			e.printStackTrace();
			feeBJB = "0";
			feeAggregator = "0";
		}
	}

	public AmountInformation() {
		super();
	}

	public AmountInformation(String billAmount, String feeBJB, String feeAggregator, String totalAmount) {
		super();
		this.billAmount = billAmount;
		this.feeBJB = feeBJB;
		this.feeAggregator = feeAggregator;
		this.totalAmount = totalAmount;
	}

	public AmountInformation(String billAmount, String feeBJB, String feeAggregator) {
		super();
		this.billAmount = billAmount;
		this.feeBJB = feeBJB;
		this.feeAggregator = feeAggregator;
	}

	@Override
	public String toString() {
		return "AmountInformation [billAmount=" + billAmount + ", feeBJB=" + feeBJB + ", feeAggregator=" + feeAggregator
				+ ", totalAmount=" + totalAmount + "]";
	}

	/**
	 * fill object amount information from string bit 57
	 * 
	 * @param bit57
	 */
	public void unpack(String bit57) {
		if (!checkIfNull(bit57)) {
			billAmount = Long.parseLong(bit57.substring(0, 12)) + "";
			feeBJB = Long.parseLong(bit57.substring(12, 24)) + "";
			feeAggregator = Long.parseLong(bit57.substring(24, 36)) + "";
			totalAmount = Long.parseLong(bit57.substring(36, 48)) + "";
		}
	}

}
