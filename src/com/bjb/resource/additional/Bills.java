package com.bjb.resource.additional;

import java.io.Serializable;

/**
 * 
 * @author satrio
 * 
 * Goal			: Helper for BillInformation class
 * Create Date	: Sep 3, 2016
 */
public class Bills implements Serializable {
	
	private String info;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}
