package com.bjb.commons;

import org.jpos.iso.ISOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bjb.cache.Cache;
import com.bjb.cache.ForAggregators;
import com.bjb.config.StandaloneDataJpaConfig;
import com.bjb.dto.Pair;
import com.bjb.iso.util.SamLogExecutor;
import com.bjb.repository.SystemParameterRepository;
import com.bjb.repository.TransactionLogRepository;
import com.bjb.resource.additional.AmountInformation;
import com.bjb.resource.additional.BillInformation;
import com.bjb.resource.additional.Bills;
import com.bjb.resource.main.InquiryPaymentRequest;
import com.bjb.resource.main.InquirySaldoRequest;
import com.bjb.resource.main.PaymentRequest;
import com.bjb.resource.main.PurchaseRequest;
import com.bjb.service.EmailSupport;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class MainBean {

	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(MainBean.class);

	@Autowired
	SamLogExecutor samLogExecutor;
	
	@Autowired
	EmailSupport EmailSupport;

	@Autowired
	TransactionLogRepository repo;
	
	@Autowired
	SystemParameterRepository repos;
	
	public boolean asa(String s,int o){
		if(o>10){
			s = "00";
			return true;
		}else if(0<5){
			s = "11";
			return false;
		}else if(0<10){
			s = "22";
			return false;
		} else {
			s = "99";
			return false;}
	}
	
	public void start() throws ParseException, ISOException, JsonProcessingException, IllegalArgumentException, IllegalAccessException {
		Cache.init();
		InquiryPaymentRequest ipr = new InquiryPaymentRequest();
		InquirySaldoRequest isr = new InquirySaldoRequest();
		PaymentRequest pr = new PaymentRequest();
		PurchaseRequest prr = new PurchaseRequest();
		Pair<Boolean, String> resAgg = ForAggregators.getValidWithReason("W0100", "ISO");
		System.out.println(resAgg.a +" : "+resAgg.b);
//		ipr.setFeatureCode("0106");
//		ipr.setAggregatorCode("0100");
//		ipr.setPrimaryAccountNumber("2132188012");
//		
//		String b1 = "TRX120392093";
//		//b1 = CommonUtils.padright(b1, 20, ' ');
//		String b2 = "Satrio H";
//		//b2 = CommonUtils.padleft(b2, 20, '0');
//		String b3 = "195000";
//		//b3 = CommonUtils.padleft(b3, 20, '0');
//		String b4 = "2132188012";
//	
//		Bills[] bills = new Bills[3];
//		bills[0] = new Bills();
//		bills[0].setInfo(b1);
//		bills[1] = new Bills();
//		bills[1].setInfo(b2);
//		bills[2] = new Bills();
//		bills[2].setInfo(b3);
//		BillInformation bil = new BillInformation();
//		bil.setBills(bills);
//		ipr.setBillInformation(bil);
//		String bit61 = AdditionalDataPayment.packsixtyone(ipr, "XML");
//		System.out.println("generated " +bit61);
//		String bit61r = "TRX120392093        195000              Satrio H            ";
//		ISOMsg res = new ISOMsg();
//		SamPackager samp = new SamPackager();
//		res.setPackager(samp);
//		res.set(2, ipr.getPrimaryAccountNumber());
//		res.set(60, ipr.getFeatureCode());
//		res.set(61, bit61r);
//		Bills[] bilo = AdditionalDataPayment.unpacksixtyone(res, "XML");
//		for(Object b : bilo){
//			Bills ba = (Bills) b;
//			System.out.println("info "+ba.getInfo());
//		}
		ipr.setAdditionalData("");
		ipr.setAggregatorCode("");
		ipr.setAmountInformation(new AmountInformation("","","",""));
		ipr.setAmountTransaction("");
		ipr.setBillInformation(new BillInformation());
		Bills[] a = new Bills[3];
		a[0] = new Bills();
				a[0].setInfo("info1");
		a[1] = new Bills();
				a[1].setInfo("info2");
		a[2] = new Bills();
				a[2].setInfo("info3");
		ipr.getBillInformation().setBills(a);
		ipr.setCaCode("");
		ipr.setCurrencyCode("");
		ipr.setDestinationAccountNumber("");
		ipr.setFeatureCode("");
		ipr.setLocalTransactionDate("");
		ipr.setMerchantType("");
		ipr.setPay("");
		ipr.setPosEntryModeCode("");
		ipr.setPrimaryAccountNumber("");
		ipr.setProcessingCode("");
		ipr.setSequenceNumber("");
		ipr.setSettlementDate("");
		ipr.setSourceAccountNumber("");
		ipr.setSystemTraceAuditNumber("");
		ipr.getTerminalAggregator().setCardAcceptorName("");
		ipr.getTerminalAggregator().setTerminalIdentifierNumber("");
		ipr.getTerminalAggregator().setTerminalName("");
		ipr.setTimeLocalTransaction("");
		ipr.setTrack2Data("");
		ipr.setTransmissionDateTime("");
		
		isr.setAdditionalData("");
		isr.setAggregatorCode("");
		isr.setAmountTransaction("");
		isr.setCaCode("");
		isr.setCurrencyCode("");
		isr.setPay("");
		isr.setPrimaryAccountNumber("");
		isr.setProcessingCode("");
		isr.setSourceAccountNumber("");
		isr.setSystemTraceAuditNumber("");
		isr.getTerminalAggregator().setCardAcceptorName("");
		isr.getTerminalAggregator().setTerminalIdentifierNumber("");
		isr.getTerminalAggregator().setTerminalName("");
		isr.setTrack2Data("");
		isr.setTransmissionDateTime("");
		
		
		try{
		Gson gson = new Gson();
		String raw = gson.toJson(isr);
		System.out.println("JSON : "+raw);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("12 : "+java.sql.Types.NCHAR +" char : "+java.sql.Types.CHAR);
		
		
//		SystemParameter sp = repos.findByName(Constants.CLEANSING_LOG_TIME);
//		System.out.println(sp);

		//ISO
//		String b1 = "TRX12039209310";	
//		b1 = CommonUtils.padright(b1, 20, ' ');
//		String b2 = "Satrio H";
//		b2 = CommonUtils.padright(b2, 20, ' ');
//		String b3 = "195000";
//		b3 = CommonUtils.padleft(b3, 20, '0');
//		String b4 = "Byar Pajak";
//		b4 = CommonUtils.padright(b4, 20, ' ');
//		ISOMsg msg = new ISOMsg();
//		msg.set(2, "0213218392");
//		msg.set(3, "58108");
//		msg.set(60, "0106");
//		msg.set(61, "TRX0080212");
//		SAMPackager samPackager = new SAMPackager();
//		msg.setPackager(samPackager);
//		String bit61 = AdditionalDataPayment.packsixtyone(msg);
//		msg.dump(System.out, "REQ");
//		ISOMsg res = (ISOMsg) msg.clone();
//		res.setPackager(samPackager);
//		res.set(61, b1+b2+b3+b4);
//		res.set(43, "21389289");
//		res.dump(System.out, "IF400");
//		res = AdditionalDataPayment.unpacksixtyone(res);
//		res.dump(System.out, "RES");
//		System.out.println(bit61);
	}

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(StandaloneDataJpaConfig.class);
		ctx.refresh();
		MainBean m = (MainBean) ctx.getBean("mainBean");
		try {
			m.start();
			String s = "";
			System.out.println(m.asa(s, 10)+" "+s);
//			StdScheduler sched = (StdScheduler) ctx.getBean("schedulerFactoryBean");
//			CronTriggerImpl cron = (CronTriggerImpl) ctx.getBean("cronTriggerFactoryBean");
//			System.out.println("time : "+cron.getCronExpression());
//			cron.setCronExpression("0 28 12 1/1 * ? *");
//			sched.rescheduleJob(cron.getKey(), cron);
//			cron = null;
//			cron = (CronTriggerImpl) ctx.getBean("cronTriggerFactoryBean");
//			System.out.println("time : "+cron.getCronExpression());
			//String regex = "^\\\\s*($|#|\\\\w+\\\\s*=|(\\\\?|\\\\*|(?:[0-5]?\\\\d)(?:(?:-|\\/|\\\\,)(?:[0-5]?\\\\d))?(?:,(?:[0-5]?\\\\d)(?:(?:-|\\/|\\\\,)(?:[0-5]?\\\\d))?)*)\\\\s+(\\\\?|\\\\*|(?:[0-5]?\\\\d)(?:(?:-|\\/|\\\\,)(?:[0-5]?\\\\d))?(?:,(?:[0-5]?\\\\d)(?:(?:-|\\/|\\\\,)(?:[0-5]?\\\\d))?)*)\\\\s+(\\\\?|\\\\*|(?:[01]?\\\\d|2[0-3])(?:(?:-|\\/|\\\\,)(?:[01]?\\\\d|2[0-3]))?(?:,(?:[01]?\\\\d|2[0-3])(?:(?:-|\\/|\\\\,)(?:[01]?\\\\d|2[0-3]))?)*)\\\\s+(\\\\?|\\\\*|(?:0?[1-9]|[12]\\\\d|3[01])(?:(?:-|\\/|\\\\,)(?:0?[1-9]|[12]\\\\d|3[01]))?(?:,(?:0?[1-9]|[12]\\\\d|3[01])(?:(?:-|\\/|\\\\,)(?:0?[1-9]|[12]\\\\d|3[01]))?)*)\\\\s+(\\\\?|\\\\*|(?:[1-9]|1[012])(?:(?:-|\\/|\\\\,)(?:[1-9]|1[012]))?(?:L|W)?(?:,(?:[1-9]|1[012])(?:(?:-|\\/|\\\\,)(?:[1-9]|1[012]))?(?:L|W)?)*|\\\\?|\\\\*|(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?(?:,(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)*)\\\\s+(\\\\?|\\\\*|(?:[0-6])(?:(?:-|\\/|\\\\,|#)(?:[0-6]))?(?:L)?(?:,(?:[0-6])(?:(?:-|\\/|\\\\,|#)(?:[0-6]))?(?:L)?)*|\\\\?|\\\\*|(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?(?:,(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?)*)(|\\\\s)+(\\\\?|\\\\*|(?:|\\\\d{4})(?:(?:-|\\/|\\\\,)(?:|\\\\d{4}))?(?:,(?:|\\\\d{4})(?:(?:-|\\/|\\\\,)(?:|\\\\d{4}))?)*))$\r\n";
//			String regex = "";
//		    regex ="^\\s*($|#|\\w+\\s*=|(\\?|\\*|(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?(?:,(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?)*)\\s+(\\?|\\*|(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?(?:,(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?)*)\\s+(\\?|\\*|(?:[01]?\\d|2[0-3])(?:(?:-|\\/|\\,)(?:[01]?\\d|2[0-3]))?(?:,(?:[01]?\\d|2[0-3])(?:(?:-|\\/|\\,)(?:[01]?\\d|2[0-3]))?)*)\\s+(\\?|\\*|(?:0?[1-9]|[12]\\d|3[01])(?:(?:-|\\/|\\,)(?:0?[1-9]|[12]\\d|3[01]))?(?:,(?:0?[1-9]|[12]\\d|3[01])(?:(?:-|\\/|\\,)(?:0?[1-9]|[12]\\d|3[01]))?)*)\\s+(\\?|\\*|(?:[1-9]|1[012])(?:(?:-|\\/|\\,)(?:[1-9]|1[012]))?(?:L|W)?(?:,(?:[1-9]|1[012])(?:(?:-|\\/|\\,)(?:[1-9]|1[012]))?(?:L|W)?)*|\\?|\\*|(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?(?:,(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)*)\\s+(\\?|\\*|(?:[0-6])(?:(?:-|\\/|\\,|#)(?:[0-6]))?(?:L)?(?:,(?:[0-6])(?:(?:-|\\/|\\,|#)(?:[0-6]))?(?:L)?)*|\\?|\\*|(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?(?:,(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?)*)(|\\s)+(\\?|\\*|(?:|\\d{4})(?:(?:-|\\/|\\,)(?:|\\d{4}))?(?:,(?:|\\d{4})(?:(?:-|\\/|\\,)(?:|\\d{4}))?)*))$";
//		    String cron = "0 10 10 1/1 * ? *";
//			System.out.println(cron.matches(regex));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ctx.close();
		// URLClassLoader classLoader = (URLClassLoader)
		// MainBean.class.getClassLoader();
		// System.out.println(Arrays.toString(classLoader.getURLs()));

	}
}