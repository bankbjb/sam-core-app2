package com.bjb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_features")
public class Feature implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7485672473985995442L;
	Integer id;
	String commisionAccountNo;
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="commision_account_no")
	public String getCommisionAccountNo() {
		return commisionAccountNo;
	}
	public void setCommisionAccountNo(String commisionAccountNo) {
		this.commisionAccountNo = commisionAccountNo;
	}
	
	
}
