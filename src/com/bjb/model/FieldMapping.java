package com.bjb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="m_field_mapping")
public class FieldMapping implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6320237725655759374L;
	//private message_mapping_id numeric(19,2) NOT NULL,
	//private Integer index;// integer NOT NULL,
	private Integer startAt;// integer,
	private Integer endAt;// integer,
	private MessageMapping messageMapping;
	private Integer fieldPosition;
	private MessageFields messageFields;
	private Integer request;
	private Integer response;
	private String variable;
	private String padding;
	private String trxType;
	private String variableChar;

	//private MFieldPosition fieldPosition;
	
//	@Id
//	@Column(name="index")
//	public Integer getIndex() {
//		return index;
//	}
//	public void setIndex(Integer index) {
//		this.index = index;
//	}
	@Column(name="start_at")
	public Integer getStartAt() {
		return startAt;
	}
	public String getVariableChar() {
		return variableChar;
	}
	public void setVariableChar(String variableChar) {
		this.variableChar = variableChar;
	}
	public void setStartAt(Integer startAt) {
		this.startAt = startAt;
	}
	@Column(name="end_at")
	public Integer getEndAt() {
		return endAt;
	}
	public void setEndAt(Integer endAt) {
		this.endAt = endAt;
	}
	@Id
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="message_mapping_id")
	public MessageMapping getMessageMapping() {
		return messageMapping;
	}
	public void setMessageMapping(MessageMapping messageMapping) {
		this.messageMapping = messageMapping;
	}
	@Id
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="index")
	public MessageFields getMessageFields() {
		return messageFields;
	}
	public void setMessageFields(MessageFields messageFields) {
		this.messageFields = messageFields;
	}

	@Column(name="field_position")
	public Integer getFieldPosition() {
		return fieldPosition;
	}
	public void setFieldPosition(Integer fieldPosition) {
		this.fieldPosition = fieldPosition;

	}
	@Id
	@Column(name="request")
	public Integer getRequest() {
		return request;
	}
	public void setRequest(Integer request) {
		this.request = request;
	}
	@Id
	@Column(name="response")
	public Integer getResponse() {
		return response;
	}
	public void setResponse(Integer response) {
		this.response = response;
	}
	@Id
	@Column(name="variable")
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	@Column(name="padding")
	public String getPadding() {
		return padding;
	}
	public void setPadding(String padding) {
		this.padding = padding;
	}
	@Override
	public String toString() {
		return "FieldMapping [startAt=" + startAt + ", endAt=" + endAt + ", fieldPosition=" + fieldPosition + ", messageFields=" + messageFields + "]";
	}
	@Id
	@Column(name="trxType")
	public String getTrxType() {
		return trxType;
	}
	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	
}
