package com.bjb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="m_commision_tiering")
public class CommissionTiering implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8724200239783104967L;
	/**
	 * 
	 */
	private Integer id;// numeric NOT NULL,
	private Long commissionBjb;// numeric(19,2),
	private Long commisionAggregator;// numeric(19,2),
	private Long upperLimit;// numeric(19,2),
	private Long lowerLimit;// numeric(19,2),
	//private int featureMappingId;// numeric(19,2),
	private String code;// character varying(255),
	private Integer status;// integer,
//	private Timestamp createDate;//create_date timestamp without time zone,
//	private Integer createBy;// numeric(19,2),
//	private String createWho;// character varying(255),
//	private Timestamp changeDate;// timestamp without time zone,
//	private Integer changeBy;// numeric(19,2),
//	private String changeWho;// character varying(255),
//	private Timestamp approvalDate;// timestamp without time zone,
//	private Integer approvalBy;// numeric(19,2),
//	private String approvalWho;// character varying(255),
//	private String approvalReason;// character varying(255),
//	private String approvalStatus;// character varying(255),
	private FeatureMapping featureMapping;
	
	@Id
	@Column(name="id",nullable=false,unique=true)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="commission_bjb")
	public Long getCommissionBjb() {
		return commissionBjb;
	}
	public void setCommissionBjb(Long commissionBjb) {
		this.commissionBjb = commissionBjb;
	}
	@Column(name="commision_aggregator")
	public Long getCommisionAggregator() {
		return commisionAggregator;
	}
	public void setCommisionAggregator(Long commisionAggregator) {
		this.commisionAggregator = commisionAggregator;
	}
	@Column(name="upper_limit")
	public Long getUpperLimit() {
		return upperLimit;
	}
	public void setUpperLimit(Long upperimit) {
		this.upperLimit = upperimit;
	}
	@Column(name="lower_limit")
	public Long getLowerLimit() {
		return lowerLimit;
	}
	public void setLowerLimit(Long lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	@Column(name="code")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="feature_mapping_id")
	public FeatureMapping getFeatureMapping() {
		return featureMapping;
	}
	public void setFeatureMapping(FeatureMapping featureMapping) {
		this.featureMapping = featureMapping;
	}

	@Override
	public String toString() {
		return "CommissionTiering [id=" + id + ", commissionBjb=" + commissionBjb + ", commisionAggregator="
				+ commisionAggregator + ", upperLimit=" + upperLimit + ", lowerLimit=" + lowerLimit + ", code=" + code
				+ ", status=" + status + "]";
	}
	
	
	
}
