package com.bjb.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bjb.util.ObjectUtils;

@Entity
@Table(name="m_feature_mapping")
public class FeatureMapping implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8605277428674509923L;
	private Integer id;// numeric NOT NULL,
	private Integer aggregatorId;// numeric(19,2),
	private Integer collectingAgentId;// numeric(19,2),
	private Integer featureId;// numeric(19,2),
	private String code;// character varying(255),
	private Long commisionAmount;// numeric(19,2),
	private Integer status;// Integereger,
	private Timestamp createDate;// timestamp without time zone,
	private Integer createBy;// numeric(19,2),
	private String createWho;// character varying(255),
	private Timestamp changeDate;// timestamp without time zone,
	private Integer changeBy;// numeric(19,2),
	private String changeWho;// character varying(255),
	private Timestamp approvalDate;// timestamp without time zone,
	private Integer approvalBy;// numeric(19,2),
	private String approvalWho;// character varying(255),
	private String approvalReason;// character varying(255),
	private String approvalStatus;// character varying(255),
	private Set<CommissionTiering> commisionTieringSet = new HashSet<>();
	
	@Id
	@Column(name="id",nullable=false,unique=true)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="aggregator_id")
	public Integer getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(Integer aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	@Column(name="collecting_agent_id")
	public Integer getCollectingAgentId() {
		return collectingAgentId;
	}
	public void setCollectingAgentId(Integer collectingAgentId) {
		this.collectingAgentId = collectingAgentId;
	}
	@Column(name="feature_id")
	public Integer getFeatureId() {
		return featureId;
	}
	public void setFeatureId(Integer featureId) {
		this.featureId = featureId;
	}
	@Column(name="code")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="commision_amount")
	public Long getCommisionAmount() {
		return commisionAmount;
	}
	public void setCommisionAmount(Long commisionAmount) {
		this.commisionAmount = commisionAmount;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name="create_date")
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	@Column(name="create_by")
	public Integer getCreateBy() {
		return createBy;
	}
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}
	@Column(name="create_who")
	public String getCreateWho() {
		return createWho;
	}
	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}
	@Column(name="change_date")
	public Timestamp getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Timestamp changeDate) {
		this.changeDate = changeDate;
	}
	@Column(name="change_by")
	public Integer getChangeBy() {
		return changeBy;
	}
	public void setChangeBy(Integer changeBy) {
		this.changeBy = changeBy;
	}
	@Column(name="change_who")
	public String getChangeWho() {
		return changeWho;
	}
	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}
	@Column(name="approval_date")
	public Timestamp getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Timestamp approvalDate) {
		this.approvalDate = approvalDate;
	}
	@Column(name="approval_by")
	public Integer getApprovalBy() {
		return approvalBy;
	}
	public void setApprovalBy(Integer approvalBy) {
		this.approvalBy = approvalBy;
	}
	@Column(name="approval_who")
	public String getApprovalWho() {
		return approvalWho;
	}
	public void setApprovalWho(String approvalWho) {
		this.approvalWho = approvalWho;
	}
	@Column(name="approval_reason")
	public String getApprovalReason() {
		return approvalReason;
	}
	public void setApprovalReason(String approvalReason) {
		this.approvalReason = approvalReason;
	}
	@Column(name="approval_status")
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	@OneToMany(fetch = FetchType.EAGER, mappedBy="featureMapping")
	public Set<CommissionTiering> getCommisionTieringSet() {
		return commisionTieringSet;
	}
	public void setCommisionTieringSet(Set<CommissionTiering> commisionTieringSet) {
		this.commisionTieringSet = commisionTieringSet;
	}
	@Override
	public String toString() {
		return "FeatureMapping [id=" + id + ", aggregatorId=" + aggregatorId + ", collectingAgentId="
				+ collectingAgentId + ", featureId=" + featureId + ", code=" + code + ", commisionAmount="
				+ commisionAmount + ", status=" + status + ", createDate=" + createDate + ", createBy=" + createBy
				+ ", createWho=" + createWho + ", changeDate=" + changeDate + ", changeBy=" + changeBy + ", changeWho="
				+ changeWho + ", approvalDate=" + approvalDate + ", approvalBy=" + approvalBy + ", approvalWho="
				+ approvalWho + ", approvalReason=" + approvalReason + ", approvalStatus=" + approvalStatus
				+ ", commisionTieringSet=" + ObjectUtils.printSet(commisionTieringSet) + "]";
	}
	
}
