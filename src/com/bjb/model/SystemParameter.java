package com.bjb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="s_parameters")
public class SystemParameter {
	private String id;// character varying(256) NOT NULL,
	private String name;// character varying(255),
	private String description;// character varying(255),
	private String value;// character varying(255),
	private String status;// integer,

	@Id
	@Column(name="id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="value")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "SystemParameter [id=" + id + ", name=" + name + ", description=" + description + ", value=" + value
				+ ", status=" + status + "]";
	}
	
	


}
