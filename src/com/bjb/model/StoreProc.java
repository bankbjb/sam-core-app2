package com.bjb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "store_proc")
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "stan_generator", procedureName = "stan_generator", parameters = {
				@StoredProcedureParameter(mode = ParameterMode.OUT, name = "outParam", type = String.class) }),
		@NamedStoredProcedureQuery(name = "next_stan", procedureName = "next_stan", parameters = {
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "inParam", type = Integer.class),
				@StoredProcedureParameter(mode = ParameterMode.OUT, name = "outParam", type = String.class) }),
		@NamedStoredProcedureQuery(name = "cleansing_log", procedureName = "cleansing_log", parameters = {
				@StoredProcedureParameter(mode = ParameterMode.OUT, name = "outParam", type = String.class) }) })
public class StoreProc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8335841129694175346L;
	Integer id;

	@Id
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
