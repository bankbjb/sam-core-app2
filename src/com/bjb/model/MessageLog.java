package com.bjb.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="t_message_log")
public class MessageLog implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4175635133967997383L;
	public BigDecimal id;
	public BigDecimal AggregatorCode;
	public BigDecimal caCode;
	public BigDecimal featureCode;
	public String responseCode;
	public String mti;
	public String billingId;
	public String stan;
	public String rawRequest;
	public String rawResponse;
	public Date createDate;
	
	@Id
	@TableGenerator(schema="public", table = "id_table", name = "idTable", 
    allocationSize = 1000, initialValue = 0, pkColumnName = "pk", 
    valueColumnName = "value", pkColumnValue = "messagelog")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="idTable")
	@Column(name="id")
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	@Column(name="aggregator_code")
	public BigDecimal getAggregatorCode() {
		return AggregatorCode;
	}
	public void setAggregatorCode(BigDecimal aggregatorCode) {
		this.AggregatorCode = aggregatorCode;
	}
	@Column(name="ca_code")
	public BigDecimal getCaCode() {
		return caCode;
	}
	public void setCaCode(BigDecimal caCode) {
		this.caCode = caCode;
	}
	@Column(name="feature_code")
	public BigDecimal getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(BigDecimal featureCode) {
		this.featureCode = featureCode;
	}
	@Column(name="response_code")
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	@Column(name="mti")
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	@Column(name="billing_id")
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	@Column(name="stan")
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	@Column(name="raw_request")
	public String getRawRequest() {
		return rawRequest;
	}
	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}
	@Column(name="raw_response")
	public String getRawResponse() {
		return rawResponse;
	}
	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
	@Column(name="create_date")
	@Temporal(TemporalType.DATE)
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public String toString() {
		return "MessageLog [id=" + id + ", aggregatorCode=" + AggregatorCode + ", caCode=" + caCode + ", featureCode="
				+ featureCode + ", responseCode=" + responseCode + ", mti=" + mti + ", billingId=" + billingId
				+ ", stan=" + stan + ", rawRequest=" + rawRequest + ", rawResponse=" + rawResponse + ", createDate="
				+ createDate + "]";
	}
	
	
	
}
