package com.bjb.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bjb.key.model.EmailLogId;

@Entity
@Table(name="t_log_email_notification")
@IdClass(EmailLogId.class)
public class EmailLogNotification implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -317248800480972276L;
	private Integer aggregatorId;// numeric NOT NULL,
	private String aggregatorCode;// character varying,
	private Timestamp lastSendEmail;// timestamp without time zone,
	private Integer retry;// integer,
	private Date date;// date NOT NULL,
	//private EmailLogId emailLogId;
	
	@Id
	@Column(name="aggregator_id")
	public Integer getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(Integer aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	@Column(name="aggregator_code")
	public String getAggregatorCode() {
		return aggregatorCode;
	}
	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}
	@Column(name="last_send_email")
	public Timestamp getLastSendEmail() {
		return lastSendEmail;
	}
	public void setLastSendEmail(Timestamp lastSendEmail) {
		this.lastSendEmail = lastSendEmail;
	}
	@Column(name="retry")
	public Integer getRetry() {
		return retry;
	}
	public void setRetry(Integer retry) {
		this.retry = retry;
	}
	@Id
	@Column(name="date")
	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "EmailLogNotification [aggregatorId=" + aggregatorId + ", aggregatorCode=" + aggregatorCode
				+ ", lastSendEmail=" + lastSendEmail + ", retry=" + retry + ", date=" + date + "]";
	}

}
