package com.bjb.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="m_aggregators")
public class Aggregators implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 834919589905373329L;
	private Integer id;// numeric NOT NULL,
	private String code;
	private String name;
	private String minBalance;// character varying(255),
	private String emailPic;
	private String accountNo;// character varying(255),
	private String accountGlNo;
	private String messageType;// character varying(255),
	private Timestamp pksExpiredDate;// timestamp without time zone,
	private Integer statusCa;// integer,
	private Integer statusNotification;// integer,
	private Integer status;// integer

	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="code")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="min_balance")
	public String getMinBalance() {
		return minBalance;
	}
	public void setMinBalance(String minBalance) {
		this.minBalance = minBalance;
	}
	@Column(name="email_pic")
	public String getEmailPic() {
		return emailPic;
	}
	public void setEmailPic(String emailPic) {
		this.emailPic = emailPic;
	}
	@Column(name="account_no")
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	@Column(name="message_type")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Column(name="pks_expired_date")
	public Timestamp getPksExpiredDate() {
		return pksExpiredDate;
	}
	public void setPksExpiredDate(Timestamp pksExpiredDate) {
		this.pksExpiredDate = pksExpiredDate;
	}
	@Column(name="status_ca")
	public Integer getStatusCa() {
		return statusCa;
	}
	public void setStatusCa(Integer statusCa) {
		this.statusCa = statusCa;
	}
//	@Column(name="pks_no")
//	public String getPksNo() {
//		return pksNo;
//	}
//	public void setPksNo(String pksNo) {
//		this.pksNo = pksNo;
//	}
	@Column(name="status_notification")
	public Integer getStatusNotification() {
		return statusNotification;
	}
	public void setStatusNotification(Integer statusNotification) {
		this.statusNotification = statusNotification;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Aggregators [id=" + id + ", code=" + code + ", minBalance=" + minBalance + ", accountNo=" + accountNo
				+ ", messageType=" + messageType + ", pksExpiredDate=" + pksExpiredDate + ", statusCa=" + statusCa
				+ ", statusNotification=" + statusNotification + ", status=" + status + "]";
	}
	@Column(name="account_gl_no")
	public String getAccountGlNo() {
		return accountGlNo;
	}
	public void setAccountGlNo(String accountGlNo) {
		this.accountGlNo = accountGlNo;
	}
	
	
	
}
