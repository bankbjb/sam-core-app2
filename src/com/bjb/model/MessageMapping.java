package com.bjb.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.bjb.util.ObjectUtils;

@Entity
@Table(name="m_message_mapping")
public class MessageMapping implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -446056175686280989L;
	private Integer id;// numeric NOT NULL,
	private Integer featureId;// numeric(19,2),
	private String code;// character varying(255),
	private String messageType;// character varying(255),
	private Integer status;// integer,

	private Set<FieldMapping> fieldMappingSet = new HashSet<>(); 
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="feature")
	public Integer getFeatureId() {
		return featureId;
	}
	public void setFeatureId(Integer feature) {
		this.featureId = feature;
	}
	@Column(name="code")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="message_type")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@OneToMany(fetch=FetchType.EAGER,mappedBy="messageMapping")
	@OrderBy(clause="field_position asc")
	public Set<FieldMapping> getFieldMappingSet() {
		return fieldMappingSet;
	}
	public void setFieldMappingSet(Set<FieldMapping> fieldMappingSet) {
		this.fieldMappingSet = fieldMappingSet;
	}
	@Override
	public String toString() {
		return "MessageMapping [id=" + id.intValue() + ", featureId=" + featureId.intValue() + ", code=" + code + ", messageType="
				+ messageType + ", status=" + status.intValue()+ ", fieldMappingSet=" + ObjectUtils.printSet(fieldMappingSet) + "]";
	}
	
	public String toStr(){
		return "MessageMapping [id=" + id.intValue() + ", featureId=" + featureId.intValue() + ", code=" + code + ", messageType="
				+ messageType + ", status=" + status.intValue()+ "]";
	}
}
