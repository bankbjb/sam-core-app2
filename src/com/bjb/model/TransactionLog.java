package com.bjb.model;



import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="t_transaction_log")
public class TransactionLog {
	
	private BigDecimal id;// numeric NOT NULL,
	private Integer amount;// numeric(19,2),
	private Long commisionBjb;// numeric(19,2),
	private String terminalId;// character varying(255),
	private String responseCode;// character varying(255),
	private String aggregatorCode;// character varying(255),
	private String name;// character varying(255),
	private String cacode;// character varying(255),
	private String featureCode;// character varying(255),
	private Integer featureMapping;// numeric(19,2),
	private String billingId;// character varying(255),
	private Timestamp createDate;// timestamp without time zone,
	private Long commisionAgent;// numeric(19,2),
	private String rawIsoReq;
	private String rawIsoRes;
	private String stan;
	private String transmissionDate;
	
	@Id
	@TableGenerator(schema="public", table = "id_table", name = "idTable", 
    allocationSize = 1000, initialValue = 0, pkColumnName = "pk", 
    valueColumnName = "value", pkColumnValue = "transactionlog")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="idTable")
	@Column(name="id")
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	@Column(name="amount")
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	@Column(name="commision_bjb")
	public Long getCommisionBjb() {
		return commisionBjb;
	}
	public void setCommisionBjb(Long commisionBjb) {
		this.commisionBjb = commisionBjb;
	}
	@Column(name="terminal_id")
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	@Column(name="response_code")
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	@Column(name="aggregator_code")
	public String getAggregatorCode() {
		return aggregatorCode;
	}
	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="cacode")
	public String getCacode() {
		return cacode;
	}
	public void setCacode(String cacode) {
		this.cacode = cacode;
	}
	@Column(name="feature_code")
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	@Column(name="feature_mapping")
	public Integer getFeatureMapping() {
		return featureMapping;
	}
	public void setFeatureMapping(Integer featureMapping) {
		this.featureMapping = featureMapping;
	}
	@Column(name="billing_id")
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	@Column(name="create_date")
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp create_date) {
		this.createDate = create_date;
	}
	@Column(name="commision_agent")
	public Long getCommisionAgent() {
		return commisionAgent;
	}
	public void setCommisionAgent(Long commisionAgent) {
		this.commisionAgent = commisionAgent;
	}
	@Column(name="raw_iso_request")
	public String getRawIsoReq() {
		return rawIsoReq;
	}
	public void setRawIsoReq(String rawIsoReq) {
		this.rawIsoReq = rawIsoReq;
	}
	@Column(name="raw_iso_response")
	public String getRawIsoRes() {
		return rawIsoRes;
	}
	public void setRawIsoRes(String rawIsoRes) {
		this.rawIsoRes = rawIsoRes;
	}
	@Column(name="stan")
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	@Column(name="transmission_date")
	public String getTransmissionDate() {
		return transmissionDate;
	}
	public void setTransmissionDate(String transmissionDate) {
		this.transmissionDate = transmissionDate;
	}
	@Override
	public String toString() {
		return "TransactionLog [id=" + id + ", amount=" + amount
				+ ", commisionBjb=" + commisionBjb + ", terminalId="
				+ terminalId + ", responseCode=" + responseCode
				+ ", aggregatorCode=" + aggregatorCode + ", name=" + name
				+ ", cacode=" + cacode + ", featureCode=" + featureCode
				+ ", featureMapping=" + featureMapping + ", billingId="
				+ billingId + ", createDate=" + createDate
				+ ", commisionAgent=" + commisionAgent + ", rawIsoReq="
				+ rawIsoReq + ", rawIsoRes=" + rawIsoRes + ", stan=" + stan
				+ ", transmissionDate=" + transmissionDate + "]";
	}

}
