package com.bjb.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="m_message_fields")
public class MessageFields implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2145882081688013403L;
	private Integer id;// numeric NOT NULL,
	private String messageType;// character varying(255),
	private String name;// character varying(255),
	private String description;// character varying(255),
	private Set<FieldMapping> fieldMapping = new HashSet<>();
	private Integer length;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="message_type")
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@OneToMany(fetch=FetchType.EAGER,mappedBy="messageFields")
	public Set<FieldMapping> getFieldMapping() {
		return fieldMapping;
	}
	public void setFieldMapping(Set<FieldMapping> fieldMapping) {
		this.fieldMapping = fieldMapping;
	}
	@Column(name="length")
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	@Override
	public String toString() {
		return "MessageFields [id=" + id + ", messageType=" + messageType + ", name=" + name + ", description="
				+ description + "]";
	}
	
	
	
}
