package com.bjb.webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.cache.ForAdditionalInfo;
import com.bjb.cache.ForAggregators;
import com.bjb.cache.ForCaCode;
import com.bjb.cache.ForSystemParameter;
import com.bjb.cache.MapResponses;
import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.dto.Pair;
import com.bjb.iso.main.OpenSocketConnector;
import com.bjb.iso.util.ISOHelper;
import com.bjb.iso.util.SamLogExecutor;
import com.bjb.iso.util.SamPackager;
import com.bjb.resource.main.EchoRequest;
import com.bjb.resource.main.EchoResponse;
import com.bjb.resource.main.InquiryPaymentRequest;
import com.bjb.resource.main.InquiryPaymentResponse;
import com.bjb.resource.main.InquirySaldoRequest;
import com.bjb.resource.main.InquirySaldoResponse;
import com.bjb.resource.main.PaymentRequest;
import com.bjb.resource.main.PaymentResponse;
import com.bjb.resource.main.PurchaseRequest;
import com.bjb.resource.main.PurchaseResponse;
import com.bjb.resource.main.SignOnRequest;
import com.bjb.resource.main.SignOnResponse;
import com.bjb.service.EmailSupport;
import com.bjb.util.CommonUtils;
import com.bjb.webservice.util.WSUtil;

/**
 * @author satrio/arifino
 * 
 *         Goal : provide web service for aggregator Create Date : Sep 3, 2016
 */
@Component
@Path("SAMWebService")
public class SAMWebService {

	// TODO fix email notification send

	SamPackager samPackager = new SamPackager();
	ISOHelper helper = new ISOHelper();
	Logger log = LoggerFactory.getLogger(SAMWebService.class);
	OpenSocketConnector connector = OpenSocketConnector.getConnector();

	static SamLogExecutor samLogExecutor;

	static EmailSupport emailSupport;

	@Autowired
	public void setEmailSupport(EmailSupport es) {
		emailSupport = es;
	}

	@Autowired
	public void setSAMLogExecutor(SamLogExecutor es) {
		samLogExecutor = es;
	}

	public SAMWebService() {
		super();
		samPackager = new SamPackager();
		helper = new ISOHelper();
		connector = OpenSocketConnector.getConnector();
	}

	/**
	 * Goal : Simply ignore this method, just for useless test Create Date : Oct
	 * 10, 2016
	 * 
	 * @param request
	 * @return
	 */
	public EchoResponse echo(EchoRequest request) {
		ISOMsg echoMsg = new ISOMsg();
		echoMsg.setPackager(samPackager);
		MapResponses.res.size();

		log.info(Messages.INIT_ECHO);
		try {
			echoMsg = helper.generateEchoMessage(request, echoMsg);
			echoMsg.dump(System.err, "WS");

			// send iso message to iso server
			byte[] message = connector.generateISOMessageByte(echoMsg);
			connector.send(message);
		} catch (ISOException e) {
			log.warn(e.getMessage());
		}
		log.info(Messages.ECHO_INITED);

		EchoResponse response = new EchoResponse();
		return response;
	}

	/**
	 * Goal : Simply ignore this method, just for useless test Create Date : Oct
	 * 10, 2016
	 * 
	 * @param request
	 * @return
	 */
	public SignOnResponse signOn(SignOnRequest request) {
		ISOMsg signOnMsg = new ISOMsg();
		signOnMsg.setPackager(samPackager);

		System.out.println("starting sign on");
		try {
			signOnMsg = helper.generateSignOnMessage(request, signOnMsg);
			signOnMsg.dump(System.out, "WS");

			// send iso message to iso server
			byte[] message = connector.generateISOMessageByte(signOnMsg);
			connector.send(message);

		} catch (ISOException e) {
			log.warn(e.getMessage());
		}
		System.out.println("Message sign on sent");

		SignOnResponse response = new SignOnResponse();
		return response;
	}

	/**
	 * Goal : inquiry saldo to switcher bjb Create Date : Sep 3, 2016
	 * 
	 * @param input
	 * @return
	 * @throws ISOException
	 */
	@POST
	@Path("/inquirySaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public InquirySaldoResponse inquirySaldo(InquirySaldoRequest request) {
		// generate ISO Message for inquiry saldo
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "XML");
		if (validity.a) {
		
			ISOMsg inquirySaldoReq = new ISOMsg();
			inquirySaldoReq.setPackager(samPackager);
			ISOHelper helper = new ISOHelper();
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_INQUIRY_SALDO));
			try {
				inquirySaldoReq = helper.generateRequestISOMessage(request, inquirySaldoReq);
				log.info(new String(inquirySaldoReq.pack(), Constants.DEFAULT_CHARSET));
				inquirySaldoReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ", "INQUIRYSALDO"));
			} catch (Exception e) {
				log.warn(e.getMessage());
			}

			log.info(Messages.SEND_MESSAGE(Constants.LABEL_INQUIRY_SALDO));
			try {
				connector.send(connector.generateISOMessageByte(inquirySaldoReq));
				log.info(Messages.MESSAGE_SENT(Constants.LABEL_INQUIRY_SALDO));
			} catch (Exception e) {
				log.warn(e.getMessage());
			}
			ISOMsg reply;
			reply = WSUtil.findResponse(inquirySaldoReq);
			if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
				InquirySaldoResponse response = new InquirySaldoResponse();
				response.setResponseCode(ForAggregators.getErrorCode(validity.b));
				samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
				InquirySaldoResponse response = new InquirySaldoResponse();
				response.setResponseCode(reply.getString(39));
				samLogExecutor.log(reply, inquirySaldoReq, "ISO");
				samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			} else {
				// generate response for client
				reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES", "INQUIRYSALDO"));
				InquirySaldoResponse response = new InquirySaldoResponse();
				response = helper.generateResponseWS(response, reply);
				response.setCaCode(request.getCaCode());
				// samLogExecutor.log(reply, "ISO",
				// Constants.INQUIRY_SALDO_LABEL,
				// 1);
				samLogExecutor.log(reply, inquirySaldoReq, "ISO");
				samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			InquirySaldoResponse response = new InquirySaldoResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_SALDO, 2);
			return response;
		}
	}

	@POST
	@Path("/inquirySaldoRest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public InquirySaldoResponse inquirySaldoRest(InquirySaldoRequest request) {

		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "JSON");
		if (validity.a) {
		
			// generate ISO Message for inquiry saldo
			ISOMsg inquirySaldoReq = new ISOMsg();
			inquirySaldoReq.setPackager(samPackager);
			ISOHelper helper = new ISOHelper();

			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_INQUIRY_SALDO));
			try {
				inquirySaldoReq = helper.generateRequestISOMessage(request, inquirySaldoReq);
				log.info(new String(inquirySaldoReq.pack(), Constants.DEFAULT_CHARSET));
				inquirySaldoReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "REQ", "INQUIRYSALDO"));
			} catch (ISOException e) {
				log.warn(e.getMessage());
			}

			log.info(Messages.SEND_MESSAGE(Constants.LABEL_INQUIRY_SALDO));
			try {
				connector.send(connector.generateISOMessageByte(inquirySaldoReq));
				log.info(Messages.MESSAGE_SENT(Constants.LABEL_INQUIRY_SALDO));

			} catch (Exception e) {
				log.warn(e.getMessage());
			}
			ISOMsg reply;
			reply = WSUtil.findResponse(inquirySaldoReq);
			if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
				InquirySaldoResponse response = new InquirySaldoResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
				
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
				InquirySaldoResponse response = new InquirySaldoResponse();
				response.setResponseCode(reply.getString(39));
				samLogExecutor.log(reply, inquirySaldoReq, "ISO");
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			} else {
				reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES", "INQUIRYSALDO"));
				// generate response for client
				InquirySaldoResponse response = new InquirySaldoResponse();
				response = helper.generateResponseWS(response, reply);
				response.setCaCode(request.getCaCode());
				// samLogExecutor.log(reply, "ISO",
				// Constants.INQUIRY_SALDO_LABEL,
				// 1);
				samLogExecutor.log(reply, inquirySaldoReq, "ISO");
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_SALDO, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			InquirySaldoResponse response = new InquirySaldoResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_SALDO, 2);
			return response;
		}
	}

	/**
	 * Goal : send purchase request to switcher Create Date : Sep 3, 2016
	 * 
	 * @param input
	 * @return
	 * @throws ISOException
	 */
	@POST
	@Path("/purchase")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PurchaseResponse purchase(PurchaseRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "XML");
		if(validity.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a) {
			
			// generate ISO Message for purchase
			String error = "";
			ISOMsg purchaseReq = new ISOMsg();

			purchaseReq.setPackager(samPackager);
			ISOHelper helper = new ISOHelper();
			boolean valid = true;
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PURCHASE));
			try {
				purchaseReq = helper.generateRequestISOMessage(request, purchaseReq, "XML");
				log.info(new String(purchaseReq.pack(), Constants.DEFAULT_CHARSET));
				purchaseReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ", "PURCHASE"));
			} catch (Exception e) {
				log.warn(e.getMessage());
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			}
			if (valid) {
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_PURCHASE));
				try {
					connector.send(connector.generateISOMessageByte(purchaseReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_PURCHASE));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}

				ISOMsg reply = new ISOMsg();
				reply.setPackager(samPackager);
				reply = WSUtil.findResponse(purchaseReq);

				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, purchaseReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES", "PURCHASE"));

					// generate response for client
					PurchaseResponse response = new PurchaseResponse();
					try {
						response = helper.generateResponseWS(response, reply, "XML");
					} catch (Exception e) {
						error = e.getMessage();
					}
					response.setCaCode(request.getCaCode());
					if (response.getResponseCode().equalsIgnoreCase(Constants.RC_SAM_SUCCESS)) {
						emailSupport.sendEmail(response.getAggregatorCode(), "XML");
					}
					// samLogExecutor.log(reply, "ISO",
					// Constants.PURCHASE_LABEL,
					// 1);
					samLogExecutor.log(reply, purchaseReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				} else {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
					return response;
				}
			} else {
				PurchaseResponse response = new PurchaseResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			PurchaseResponse response = new PurchaseResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "XML", Constants.LABEL_PURCHASE, 2);
			return response;
		}
	}

	/**
	 * if (ForAggregators.getValid(request.getAggregatorCode(), "JSON")) { //
	 * generate ISO Message for purchase
	 * 
	 * ISOMsg purchaseReq = new ISOMsg(); purchaseReq.setPackager(samPackager);
	 * 
	 * ISOHelper helper = new ISOHelper();
	 * 
	 * log.info(Messages.GENERATE_MESSAGE(Constants.PURCHASE_LABEL)); try {
	 * purchaseReq = helper.generateRequestISOMessage(request, purchaseReq,
	 * "JSON"); purchaseReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST",
	 * "REQ", "PURCHASE")); } catch (ISOException e) { log.warn(e.getMessage());
	 * }
	 * 
	 * log.info(Messages.SEND_MESSAGE(Constants.PURCHASE_LABEL)); // send iso
	 * message to iso server try {
	 * connector.send(connector.generateISOMessageByte(purchaseReq));
	 * log.info(Messages.MESSAGE_SENT(Constants.PURCHASE_LABEL)); } catch
	 * (ISOException e) { log.warn(e.getMessage()); } ISOMsg reply = new
	 * ISOMsg(); reply.setPackager(samPackager); reply =
	 * WSUtil.findResponse(purchaseReq);
	 * 
	 * reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES",
	 * "PURCHASE"));
	 * 
	 * // generate response for client PurchaseResponse response = new
	 * PurchaseResponse(); response = helper.generateResponseWS(response, reply,
	 * "JSON"); response.setCaCode(request.getCaCode());
	 * response.setBillInformation(request.getBillInformation());
	 * 
	 * emailSupport.sendEmail(response.getAggregatorCode(), "JSON"); //
	 * EmailUtil emu = new EmailUtil(response.getAggregatorCode(), // "JSON");
	 * // EmailThreadManager.getEtm().sendEmail(emu); // //
	 * EmailThreadManager.geEmailThreadManager().setEmu(emu); //
	 * etm.setEmu(emu); // if(!etm.isAlive()){ // System.err.println("Starting
	 * EMAIL THREAD MANAGER"); // etm.start(); // } samLogExecutor.log(reply,
	 * "ISO", Constants.PURCHASE_LABEL, 1); samLogExecutor.log(response,
	 * request, "JSON", Constants.PURCHASE_LABEL, 2); return response; } else {
	 * PurchaseResponse response = new PurchaseResponse(); // response =
	 * helper.generateResponseWS(response, reply);
	 * response.setCaCode(request.getCaCode());
	 * response.setBillInformation(request.getBillInformation());
	 * response.setResponseCode("03"); samLogExecutor.log(response, request,
	 * "JSON", Constants.PURCHASE_LABEL, 2); return response; } if
	 * (ForAggregators.getValid(request.getAggregatorCode(), "JSON")) { //
	 * generate ISO Message for purchase String error = ""; ISOMsg purchaseReq =
	 * new ISOMsg();
	 * 
	 * purchaseReq.setPackager(samPackager); ISOHelper helper = new ISOHelper();
	 * boolean valid = true;
	 * log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PURCHASE)); try {
	 * purchaseReq = helper.generateRequestISOMessage(request, purchaseReq,
	 * "JSON"); log.info(new String(purchaseReq.pack(),
	 * Constants.DEFAULT_CHARSET)); purchaseReq.dump(System.err,
	 * Constants.ISO_DUMP_HEADER("SOAP", "REQ", "PURCHASE")); } catch (Exception
	 * e) { log.warn(e.getMessage()); error = e.getMessage(); valid = false; }
	 * if (valid) { log.info(Messages.SEND_MESSAGE(Constants.LABEL_PURCHASE));
	 * try { connector.send(connector.generateISOMessageByte(purchaseReq));
	 * log.info(Messages.MESSAGE_SENT(Constants.LABEL_PURCHASE)); } catch
	 * (ISOException e) { log.warn(e.getMessage()); }
	 * 
	 * ISOMsg reply = new ISOMsg(); reply.setPackager(samPackager); reply =
	 * WSUtil.findResponse(purchaseReq);
	 * 
	 * reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES",
	 * "PURCHASE"));
	 * 
	 * // generate response for client PurchaseResponse response = new
	 * PurchaseResponse(); try { response = helper.generateResponseWS(response,
	 * reply, "JSON"); } catch (Exception e) { error = e.getMessage(); }
	 * response.setCaCode(request.getCaCode());
	 * emailSupport.sendEmail(response.getAggregatorCode(), "JSON"); //
	 * samLogExecutor.log(reply, "ISO", Constants.PURCHASE_LABEL, // 1);
	 * samLogExecutor.log(reply, purchaseReq, "ISO");
	 * samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE,
	 * 2); return response; } else { if (error.toLowerCase().contains("mapping
	 * not found")) { PurchaseResponse response = new PurchaseResponse();
	 * response.setResponseCode("80"); samLogExecutor.log(response, request,
	 * "JSON", Constants.LABEL_PURCHASE, 2); return response; } else if
	 * (error.toLowerCase().contains("mapping variable")) { PurchaseResponse
	 * response = new PurchaseResponse(); response.setResponseCode("81");
	 * samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE,
	 * 2); return response; } else if (error.toLowerCase().contains("variable
	 * a")) { PurchaseResponse response = new PurchaseResponse();
	 * response.setResponseCode("82"); samLogExecutor.log(response, request,
	 * "JSON", Constants.LABEL_PURCHASE, 2); return response; } else if
	 * (error.toLowerCase().contains("feature not found")) { PurchaseResponse
	 * response = new PurchaseResponse(); response.setResponseCode("88");
	 * samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE,
	 * 2); return response; } else if (error.toLowerCase().contains("mapping
	 * data")) { PurchaseResponse response = new PurchaseResponse();
	 * response.setResponseCode("87"); samLogExecutor.log(response, request,
	 * "JSON", Constants.LABEL_PURCHASE, 2); return response; } else {
	 * PurchaseResponse response = new PurchaseResponse();
	 * response.setResponseCode("99"); samLogExecutor.log(response, request,
	 * "JSON", Constants.LABEL_PURCHASE, 2); return response; } }
	 * 
	 * } else { PurchaseResponse response = new PurchaseResponse(); // response
	 * = helper.generateResponseWS(response, reply);
	 * response.setCaCode(request.getCaCode()); response.setResponseCode("03");
	 * // exe.log(request, "JSON", "payment",0); samLogExecutor.log(response,
	 * request, "JSON", Constants.LABEL_PURCHASE, 2); return response; }
	 */
	@POST
	@Path("/purchaseRest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PurchaseResponse purchaseRest(PurchaseRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "JSON");
		if(validity.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a) {
		
			// generate ISO Message for purchase
			String error = "";
			ISOMsg purchaseReq = new ISOMsg();

			purchaseReq.setPackager(samPackager);
			ISOHelper helper = new ISOHelper();
			boolean valid = true;
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PURCHASE));
			try {
				purchaseReq = helper.generateRequestISOMessage(request, purchaseReq, "JSON");
				log.info(new String(purchaseReq.pack(), Constants.DEFAULT_CHARSET));
				purchaseReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ", "PURCHASE"));
			} catch (Exception e) {
				log.warn(e.getMessage());
				error = e.getMessage();
				valid = false;
			}
			if (valid) {
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_PURCHASE));
				try {
					connector.send(connector.generateISOMessageByte(purchaseReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_PURCHASE));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}

				ISOMsg reply = new ISOMsg();
				reply.setPackager(samPackager);
				reply = WSUtil.findResponse(purchaseReq);

				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, purchaseReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES", "PURCHASE"));

					// generate response for client
					PurchaseResponse response = new PurchaseResponse();
					try {
						response = helper.generateResponseWS(response, reply, "JSON");
					} catch (Exception e) {
						error = e.getMessage();
					}
					response.setCaCode(request.getCaCode());
					if (response.getResponseCode().equalsIgnoreCase(Constants.RC_SAM_SUCCESS)) {
						emailSupport.sendEmail(response.getAggregatorCode(), "JSON");
					}
					// samLogExecutor.log(reply, "ISO",
					// Constants.PURCHASE_LABEL,
					// 1);
					samLogExecutor.log(reply, purchaseReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				} else {
					PurchaseResponse response = new PurchaseResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
					return response;
				}
			} else {
				PurchaseResponse response = new PurchaseResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			PurchaseResponse response = new PurchaseResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "JSON", Constants.LABEL_PURCHASE, 2);
			return response;
		}
	}

	/**
	 * 
	 * Goal : send inquiry payment request to switcher Create Date : Sep 3, 2016
	 * 
	 * @param input
	 * @return
	 * @throws ISOException
	 */
	@POST
	@Path("/inquiryPayment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public InquiryPaymentResponse inquiryPayment(InquiryPaymentRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "XML");
		/*Ayuda add validasi for ca - 26-03-2018*/
		Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(request.getCaCode(), "XML");
		if(validity.a && validasiCA.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a && validasiCA.a) {
		
			// generate ISO Message for purchase
			String error = "";
			ISOMsg inquiryPaymentReq = new ISOMsg();
			inquiryPaymentReq.setPackager(samPackager);

			ISOHelper helper = new ISOHelper();
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
			boolean valid = true;
			try {
				inquiryPaymentReq = helper.generateRequestISOMessage(request, inquiryPaymentReq, "XML");
				log.info(new String(inquiryPaymentReq.pack(), Constants.DEFAULT_CHARSET));
				inquiryPaymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ", "INQUIRYPAYMENT"));
			} catch (Exception e) {
				log.warn(e.getMessage());
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			}

			if (valid) {
				// send iso message to simulated iso server
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
				try {
					connector.send(connector.generateISOMessageByte(inquiryPaymentReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_INQUIRY_PAYMENT));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}

				// get response from iso server
				ISOMsg reply = null;
				reply = WSUtil.findResponse(inquiryPaymentReq);

				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, inquiryPaymentReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES", "INQUIRYPAYMENT"));
					// generate response for client
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					try {
						response = helper.generateResponseWS(response, reply, "XML");
						response.setCaCode(request.getCaCode());
						String bit57 = response.getAmountInformation().pack(response);
						response.getAmountInformation().unpack(bit57);
						response.setAmountTransaction(response.getAmountInformation().getTotalAmount());
					} catch (Exception e) {
						error = e.getMessage();
						e.printStackTrace();
						// response = new InquiryPaymentResponse();
						// response.setResponseCode("87");
					}
					// samLogExecutor.log(reply, "ISO",
					// Constants.INQUIRY_PAYMENT_LABEL, 1);
					samLogExecutor.log(reply, inquiryPaymentReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
			} else {
				InquiryPaymentResponse response = new InquiryPaymentResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			InquiryPaymentResponse response = new InquiryPaymentResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "XML", Constants.LABEL_INQUIRY_PAYMENT, 2);
			return response;
		}

	}

	// @POST
	// @Path("/inquiryPaymentRest")
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.APPLICATION_JSON)
	// public InquiryPaymentResponse inquiryPaymentRest(InquiryPaymentRequest
	// request) {
	// if (ForAggregators.getValid(request.getAggregatorCode(), "JSON")) {
	// // generate ISO Message for purchase
	// String error = "";
	// ISOMsg inquiryPaymentReq = new ISOMsg();
	// inquiryPaymentReq.setPackager(samPackager);
	//
	// ISOHelper helper = new ISOHelper();
	// log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
	// boolean valid = true;
	// try {
	// inquiryPaymentReq = helper.generateRequestISOMessage(request,
	// inquiryPaymentReq, "JSON");
	// log.info(new String(inquiryPaymentReq.pack(),
	// Constants.DEFAULT_CHARSET));
	// inquiryPaymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST",
	// "REQ", "INQUIRYPAYMENT"));
	// } catch (Exception e) {
	// e.printStackTrace();
	// log.warn(e.getMessage());
	// valid = false;
	// error = e.getMessage();
	// }
	//
	// if (valid) {
	// // send iso message to simulated iso server
	// log.info(Messages.SEND_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
	// try {
	// connector.send(connector.generateISOMessageByte(inquiryPaymentReq));
	// log.info(Messages.MESSAGE_SENT(Constants.LABEL_INQUIRY_PAYMENT));
	// } catch (ISOException e) {
	// log.warn(e.getMessage());
	// }
	//
	// // get response from iso server
	// ISOMsg reply = null;
	// reply = WSUtil.findResponse(inquiryPaymentReq);
	// reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES",
	// "INQUIRYPAYMENT"));
	// // generate response for client
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// try {
	// response = helper.generateResponseWS(response, reply, "JSON");
	// response.setCaCode(request.getCaCode());
	// } catch (Exception e) {
	// error = e.getMessage();
	// }
	// // samLogExecutor.log(reply, "ISO",
	// // Constants.INQUIRY_PAYMENT_LABEL, 1);
	// samLogExecutor.log(reply, inquiryPaymentReq, "ISO");
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else {
	// if (error.toLowerCase().contains("mapping not found")) {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else if (error.toLowerCase().contains("mapping variable")) {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else if (error.toLowerCase().contains("variable a")) {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else if (error.toLowerCase().contains("feature not found")) {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else if (error.toLowerCase().contains("mapping data")) {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// } else {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// }
	// }
	//
	// } else {
	// InquiryPaymentResponse response = new InquiryPaymentResponse();
	// response.setResponseCode("03");
	// samLogExecutor.log(response, request, "JSON",
	// Constants.LABEL_INQUIRY_PAYMENT, 2);
	// return response;
	// }
	// }
	@POST
	@Path("/inquiryPaymentRest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public InquiryPaymentResponse inquiryPaymentRest(InquiryPaymentRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "JSON");
		/*Ayuda add validasi for ca - 26-03-2018*/
		Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(request.getCaCode(), "XML");
		if(validity.a && validasiCA.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a && validasiCA.a) {
		
			// generate ISO Message for purchase
			String error = "";
			ISOMsg inquiryPaymentReq = new ISOMsg();
			inquiryPaymentReq.setPackager(samPackager);

			ISOHelper helper = new ISOHelper();
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
			boolean valid = true;
			try {
				inquiryPaymentReq = helper.generateRequestISOMessage(request, inquiryPaymentReq, "JSON");
				log.info(new String(inquiryPaymentReq.pack(), Constants.DEFAULT_CHARSET));
				inquiryPaymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "REQ", "INQUIRYPAYMENT"));
			} catch (Exception e) {
				log.warn(e.getMessage());
				error = e.getMessage();
				valid = false;
			}

			if (valid) {
				// send iso message to simulated iso server
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_INQUIRY_PAYMENT));
				try {
					connector.send(connector.generateISOMessageByte(inquiryPaymentReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_INQUIRY_PAYMENT));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}

				// get response from iso server
				ISOMsg reply = null;
				reply = WSUtil.findResponse(inquiryPaymentReq);

				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, inquiryPaymentReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES", "INQUIRYPAYMENT"));
					// generate response for client
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					try {
						response = helper.generateResponseWS(response, reply, "JSON");
						response.setCaCode(request.getCaCode());
						String bit57 = response.getAmountInformation().pack(response);
						response.getAmountInformation().unpack(bit57);
						response.setAmountTransaction(response.getAmountInformation().getTotalAmount());
					} catch (Exception e) {
						error = e.getMessage();
						e.printStackTrace();
					}
					// samLogExecutor.log(reply, "ISO",
					// Constants.INQUIRY_PAYMENT_LABEL, 1);
					samLogExecutor.log(reply, inquiryPaymentReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				} else {
					InquiryPaymentResponse response = new InquiryPaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
					return response;
				}
			} else {
				InquiryPaymentResponse response = new InquiryPaymentResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
				return response;
			}
		} else {
			log.warn(validity.b);
			InquiryPaymentResponse response = new InquiryPaymentResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "JSON", Constants.LABEL_INQUIRY_PAYMENT, 2);
			return response;
		}
	}

	/**
	 * 
	 * Goal : send payment request to switcher Create Date : Sep 3, 2016
	 * 
	 * @param input
	 * @return
	 * @throws ISOException
	 */
	@POST
	@Path("/payment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PaymentResponse payment(PaymentRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "XML");
		/*Ayuda add validasi for ca - 26-03-2018*/
		Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(request.getCaCode(), "XML");
		if(validity.a && validasiCA.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a && validasiCA.a) {
		
			// generate ISO Message for purchase
			ISOMsg paymentReq = new ISOMsg();
			paymentReq.setPackager(samPackager);
			String error = "";
			boolean valid = true;
			ISOHelper helper = new ISOHelper();
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PAYMENT));
			try {
				//request.setSourceAccountNumber(ForAggregators.getAggregatorAccount(request.getAggregatorCode()));
				paymentReq = helper.generateRequestISOMessage(request, paymentReq, "XML");
				log.info(new String(paymentReq.pack(), Constants.DEFAULT_CHARSET));
				paymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ", "PAYMENT"));
			} catch (ISOException e) {
				log.warn(e.getMessage());
				valid = false;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			}

			if (valid) {
				// send iso message to simulated iso server
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_PAYMENT));
				try {
					connector.send(connector.generateISOMessageByte(paymentReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_PAYMENT));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}
				// get response from iso server
				ISOMsg reply = WSUtil.findResponse(paymentReq);
				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, paymentReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES", "PAYMENT"));
					// generate response for client
					PaymentResponse response = new PaymentResponse();
					try {
						response = helper.generateResponseWS(response, reply, "XML");
					} catch (Exception e) {
						error = e.getMessage();
					}
					response.setCaCode(request.getCaCode());
					try {
						//emailSupport.sendEmail(response.getAggregatorCode(), "XML");
					} catch (Exception e) {
						e.printStackTrace();
					}
					// samLogExecutor.log(reply, "ISO", Constants.PAYMENT_LABEL,
					// 1);
					samLogExecutor.log(reply, paymentReq, "ISO");
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				} else {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
					return response;
				}
			} else {
				PaymentResponse response = new PaymentResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
				return response;
			}

		} else {
			log.warn(validity.b);
			PaymentResponse response = new PaymentResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "XML", Constants.LABEL_PAYMENT, 2);
			return response;
		}
	}

	// @POST
	// @Path("/paymentRest")
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.APPLICATION_JSON)
	// public PaymentResponse paymentRest(PaymentRequest request) {
	// // if (ForAggregators.getValid(request.getAggregatorCode(), "JSON")) {
	// // // generate ISO Message for purchase
	// // ISOMsg paymentReq = new ISOMsg();
	// // paymentReq.setPackager(samPackager);
	// //
	// // ISOHelper helper = new ISOHelper();
	// // log.info(Messages.GENERATE_MESSAGE(Constants.PAYMENT_LABEL));
	// // String error = "";
	// // try {
	// // paymentReq = helper.generateRequestISOMessage(request, paymentReq,
	// // "JSON");
	// // paymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "REQ",
	// // "PAYMENT"));
	// // } catch (ISOException e) {
	// // log.warn(e.getMessage());
	// // }
	// //
	// // // send iso message to simulated iso server
	// // log.info(Messages.SEND_MESSAGE(Constants.PAYMENT_LABEL));
	// // try {
	// // connector.send(connector.generateISOMessageByte(paymentReq));
	// // log.info(Messages.MESSAGE_SENT(Constants.PAYMENT_LABEL));
	// // } catch (ISOException e) {
	// // log.warn(e.getMessage());
	// // }
	// //
	// // // get response from iso server
	// // ISOMsg reply = WSUtil.findResponse(paymentReq);
	// // reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES",
	// // "PAYMENT"));
	// // // generate response for client
	// // PaymentResponse response = new PaymentResponse();
	// // response = helper.generateResponseWS(response, reply, "JSON");
	// // response.setCaCode(request.getCaCode());
	// // response.setBillInformation(request.getBillInformation());
	// //
	// // emailSupport.sendEmail(response.getAggregatorCode(), "JSON");
	// // // EmailUtil emu = new EmailUtil(response.getAggregatorCode(),
	// // // "JSON");
	// // // EmailThreadManager.getEtm().sendEmail(emu);
	// // // // EmailThreadManager.geEmailThreadManager().setEmu(emu);
	// // // etm.setEmu(emu);
	// // // if(!etm.isAlive()){
	// // // System.err.println("Starting EMAIL THREAD MANAGER");
	// // // etm.start();
	// // // }
	// // // exe.log(request, "JSON", "payment",0);
	// // samLogExecutor.log(reply, "ISO", Constants.PAYMENT_LABEL, 1);
	// // samLogExecutor.log(response, request, "JSON",
	// // Constants.PAYMENT_LABEL, 2);
	// // return response;
	// // } else {
	// // PaymentResponse response = new PaymentResponse();
	// // // response = helper.generateResponseWS(response, reply);
	// // response.setCaCode(request.getCaCode());
	// // response.setBillInformation(request.getBillInformation());
	// // response.setResponseCode("03");
	// // // exe.log(request, "XML", "payment",0);
	// // samLogExecutor.log(response, request, "XML", Constants.PAYMENT_LABEL,
	// // 2);
	// // return response;
	// // }
	// if (ForAggregators.getValid(request.getAggregatorCode(), "JSON")) {
	// // generate ISO Message for purchase
	// ISOMsg paymentReq = new ISOMsg();
	// paymentReq.setPackager(samPackager);
	// String error = "";
	// boolean valid = true;
	// ISOHelper helper = new ISOHelper();
	// log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PAYMENT));
	// try {
	// paymentReq = helper.generateRequestISOMessage(request, paymentReq,
	// "JSON");
	// log.info(new String(paymentReq.pack(), Constants.DEFAULT_CHARSET));
	// paymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "REQ",
	// "PAYMENT"));
	// } catch (ISOException e) {
	// log.warn(e.getMessage());
	// valid = false;
	// } catch (IllegalArgumentException e) {
	// e.printStackTrace();
	// error = e.getMessage();
	// valid = false;
	// } catch (IllegalAccessException e) {
	// e.printStackTrace();
	// error = e.getMessage();
	// valid = false;
	// }
	//
	// if (valid) {
	// // send iso message to simulated iso server
	// log.info(Messages.SEND_MESSAGE(Constants.LABEL_PAYMENT));
	// try {
	// connector.send(connector.generateISOMessageByte(paymentReq));
	// log.info(Messages.MESSAGE_SENT(Constants.LABEL_PAYMENT));
	// } catch (ISOException e) {
	// log.warn(e.getMessage());
	// }
	// // get response from iso server
	// ISOMsg reply = WSUtil.findResponse(paymentReq);
	// reply.dump(System.err, Constants.ISO_DUMP_HEADER("SOAP", "RES",
	// "PAYMENT"));
	// // generate response for client
	// PaymentResponse response = new PaymentResponse();
	// try {
	// response = helper.generateResponseWS(response, reply, "JSON");
	// } catch (Exception e) {
	// error = e.getMessage();
	// }
	// response.setCaCode(request.getCaCode());
	// emailSupport.sendEmail(response.getAggregatorCode(), "JSON");
	//
	// // samLogExecutor.log(reply, "ISO", Constants.PAYMENT_LABEL, 1);
	// samLogExecutor.log(reply, paymentReq, "ISO");
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else {
	// if (error.toLowerCase().contains("mapping not found")) {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else if (error.toLowerCase().contains("mapping variable")) {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else if (error.toLowerCase().contains("variable a")) {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else if (error.toLowerCase().contains("feature not found")) {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else if (error.toLowerCase().contains("mapping data")) {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// } else {
	// PaymentResponse response = new PaymentResponse();
	// response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// }
	// }
	//
	// } else {
	// PaymentResponse response = new PaymentResponse();
	// // response = helper.generateResponseWS(response, reply);
	// response.setCaCode(request.getCaCode());
	// response.setResponseCode("03");
	// // exe.log(request, "JSON", "payment",0);
	// samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT,
	// 2);
	// return response;
	// }
	//
	// }
	@POST
	@Path("/paymentRest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PaymentResponse paymentRest(PaymentRequest request) {
		Pair<Boolean, String> validity = ForAggregators.getValidWithReason(request.getAggregatorCode(), "JSON");
		/*Ayuda add validasi for ca - 26-03-2018*/
		Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(request.getCaCode(), "XML");
		if(validity.a && validasiCA.a){
			validity = ForAdditionalInfo.getValidCA(Integer.valueOf(CommonUtils.removeUntilNumber(request.getFeatureCode())), Integer.valueOf(CommonUtils.removeUntilNumber(request.getAggregatorCode())));
		}
		if (validity.a && validasiCA.a) {
		
			// generate ISO Message for purchase
			ISOMsg paymentReq = new ISOMsg();
			paymentReq.setPackager(samPackager);
			String error = "";
			boolean valid = true;
			ISOHelper helper = new ISOHelper();
			log.info(Messages.GENERATE_MESSAGE(Constants.LABEL_PAYMENT));
			try {
				paymentReq = helper.generateRequestISOMessage(request, paymentReq, "JSON");
				log.info(new String(paymentReq.pack(), Constants.DEFAULT_CHARSET));
				paymentReq.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "REQ", "PAYMENT"));
			} catch (ISOException e) {
				log.warn(e.getMessage());
				valid = false;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				error = e.getMessage();
				valid = false;
			}

			if (valid) {
				// send iso message to simulated iso server
				log.info(Messages.SEND_MESSAGE(Constants.LABEL_PAYMENT));
				try {
					connector.send(connector.generateISOMessageByte(paymentReq));
					log.info(Messages.MESSAGE_SENT(Constants.LABEL_PAYMENT));
				} catch (ISOException e) {
					log.warn(e.getMessage());
				}
				// get response from iso server
				ISOMsg reply = WSUtil.findResponse(paymentReq);
				if (reply.getString(39).equals(Constants.RC_SAM_NF)) {
					valid = false;
					error = "not found";
				} else if (!reply.getString(39).equals(Constants.RC_SAM_SUCCESS)) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(reply.getString(39));
					samLogExecutor.log(reply, paymentReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				}
				if (valid) {
					reply.dump(System.err, Constants.ISO_DUMP_HEADER("REST", "RES", "PAYMENT"));
					// generate response for client
					PaymentResponse response = new PaymentResponse();
					try {
						response = helper.generateResponseWS(response, reply, "JSON");
					} catch (Exception e) {
						error = e.getMessage();
					}
					response.setCaCode(request.getCaCode());
					try {
						emailSupport.sendEmail(response.getAggregatorCode(), "JSON");
					} catch (Exception e) {
						e.printStackTrace();
					}
					// samLogExecutor.log(reply, "ISO", Constants.PAYMENT_LABEL,
					// 1);
					samLogExecutor.log(reply, paymentReq, "ISO");
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				}
			}
			if (!valid) {
				if (error.toLowerCase().contains("mapping not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping variable")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("variable a")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("feature not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("mapping data")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else if (error.toLowerCase().contains("not found")) {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				} else {
					PaymentResponse response = new PaymentResponse();
					response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
					return response;
				}
			} else {
				PaymentResponse response = new PaymentResponse();
				response.setResponseCode(ForSystemParameter.getSysValue(Constants.RC_SAM_009));
				samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
				return response;
			}

		} else {
			log.warn(validity.b);
			PaymentResponse response = new PaymentResponse();
			response.setResponseCode(ForAggregators.getErrorCode(validity.b));
			samLogExecutor.log(response, request, "JSON", Constants.LABEL_PAYMENT, 2);
			return response;
		}

	}

}
