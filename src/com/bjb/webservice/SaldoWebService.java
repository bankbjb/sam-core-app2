package com.bjb.webservice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.bjb.cache.ForAggregators;
import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;
import com.bjb.iso.main.OpenSocketConnector;
import com.bjb.iso.util.ISOHelper;
import com.bjb.iso.util.SamPackager;
import com.bjb.model.Aggregators;
import com.bjb.resource.main.InquirySaldoRequest;
import com.bjb.resource.main.InquirySaldoResponse;
import com.bjb.resource.main.InquirySaldoWebRequest;
import com.bjb.resource.main.InquirySaldoWebResponse;
import com.bjb.util.CommonUtils;
import com.bjb.webservice.util.WSUtil;

public class SaldoWebService {
	SamPackager samPackager = new SamPackager();
	ISOHelper helper = new ISOHelper();
	Logger logger = Logger.getAnonymousLogger();
	OpenSocketConnector connector = OpenSocketConnector.getConnector();
	//SAMLogger samLogger = SAMLogger.getSamLogger();

	public SaldoWebService() {
		super();
		samPackager = new SamPackager();
		helper = new ISOHelper();
		logger = Logger.getAnonymousLogger();
		connector = OpenSocketConnector.getConnector();
	}
	
	/**
	 * Convert request from front end and send to switcher
	 * @param rrequest
	 * @return
	 */
	@POST
	@Path("/inquirySaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public InquirySaldoWebResponse inquirySaldo(InquirySaldoWebRequest rrequest){

		// generate ISO Message for inquiry saldo
		InquirySaldoRequest request = new InquirySaldoRequest();
		System.out.println(CommonUtils.removeUntilNumber(rrequest.getAggregatorCode()));
		request.setAggregatorCode(rrequest.getAggregatorCode());
		request.setAdditionalData(Constants.CORE_PAY);
		request.setAmountTransaction("0");
		request.setCurrencyCode(Constants.CORE_CURRENCY);
		request.setPay("JAB");
		request.setPrimaryAccountNumber(ForSystemParameter.getSysValue(Constants.CORE_BIT2));
		request.setProcessingCode("301000");
		request.setSourceAccountNumber(ForAggregators.getAggregatorAccount(rrequest.getAggregatorCode()));
		request.getTerminalAggregator().setCardAcceptorName(Constants.CORE_TA_CANAME);
		request.getTerminalAggregator().setTerminalIdentifierNumber(Constants.CORE_TA_TIN);
		request.getTerminalAggregator().setTerminalName(Constants.CORE_TA_TN);
		request.setTransmissionDateTime(new SimpleDateFormat("MMddHHmmss").format(new Date()));
		
		//AggregatorsDao dao = new AggregatorsDaoImpl();
		//Aggregators mag = dao.getById(new Integer(rrequest.getAggregatorCode().substring(1)));
		Aggregators mag = ForAggregators.getAggregators(request.getAggregatorCode());//dao.getById(new Integer(rrequest.getAggregatorCode()));
		
		if(mag!=null){
		request.setSourceAccountNumber(mag.getAccountNo());
		}
		//request.setSystemTraceAuditNumber(systemTraceAuditNumber);
		//request.setTerminalAggregator(terminalAggregator);
		//request.setTrack2Data(track2Data);
		//request.setTransmissionDateTime(transmissionDateTime);
		//samLogger.logRequest(request, "XML", "inquiry_saldo");
		ISOMsg inquirySaldoReq = new ISOMsg();
		inquirySaldoReq.setPackager(samPackager);
		
		ISOHelper helper = new ISOHelper();
		try {
			inquirySaldoReq = helper.generateRequestISOMessage(request,
					inquirySaldoReq);
			System.out.println("REQUEST : "+new String(inquirySaldoReq.pack()) );
			inquirySaldoReq.dump(System.out, "");
			//samLogger.logTransaction(inquirySaldoReq, request);
		} catch (ISOException e) {
			e.printStackTrace();
		}

		System.out.println("starting inquiry saldo");
		
		try{
			connector.send(connector.generateISOMessageByte(inquirySaldoReq));
		}
		catch (Exception e){
			e.printStackTrace();
		}
		System.err.println("qwer");

		// get response from Hashmap ResponseHandler
		
		// if get the response from Hashmap, return to channel (mapping response)
		ISOMsg reply;
		reply = WSUtil.findResponse(inquirySaldoReq);
		// generate response for client
		System.err.println("REPLY");
		reply.dump(System.err,"");
		InquirySaldoResponse response = new InquirySaldoResponse();
		response = helper.generateResponseWS(response, reply);
		response.setCaCode(request.getCaCode());
		//samLogger.logTransaction(reply,response);
		//System.out.println("return response");
		//System.out.println(response);
		InquirySaldoWebResponse rresponse = new InquirySaldoWebResponse();
		rresponse.setSaldo(response.getInquirySaldo().substring(2));
		return rresponse;
	}
	
}
