package com.bjb.webservice.util;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.cache.MapResponses;
import com.bjb.constants.Constants;
import com.bjb.iso.main.BaseConfig;

public class WSUtil {
	
	private static Logger log = LoggerFactory.getLogger(WSUtil.class);
	
	/**
	 * Find response from ListISOResponse
	 * @param req
	 * @return
	 */
	public static ISOMsg findResponse(ISOMsg req){
		long end = (BaseConfig.getBaseConfig().getTimeoutISORequest()*1000)+System.currentTimeMillis();
		ISOMsg res = null;
		log.info("MAP size : "+MapResponses.res.size());
		//System.out.println("RES : "+req.getString(11));
		while(System.currentTimeMillis()<=end){
			res = MapResponses.res.get(req.getString(11));
			if(res!=null){
				log.info("Response found");
				MapResponses.res.remove(req.getString(11));
				log.info("Response removed, now size : "+MapResponses.res.size());
				break;
			}
		}
		if(res==null){
			log.warn("Response Not Found");
			res = new ISOMsg();//(ISOMsg) req.clone();
			try {
				res.setMTI("0210");
				res.set(39,String.valueOf(Constants.RC_SAM_NF));
			} catch (ISOException e) {
				e.printStackTrace();
			}
		}
		//OPTIONAL TO PRINT RESPONSE
//		try {
//			log.info(new String(res.pack()));
//		} catch (ISOException e) {
//			e.printStackTrace();
//		}
		return res;
	}

}
