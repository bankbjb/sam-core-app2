package com.bjb.key.model;

import java.io.Serializable;
import java.sql.Date;

public class EmailLogId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8709807768611502784L;
	private Integer aggregatorId;
	private Date date;
	public Integer getAggregatorId() {
		return aggregatorId;
	}
	public void setAggregatorId(Integer aggregatorId) {
		this.aggregatorId = aggregatorId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	
}
