package com.bjb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjb.constants.Messages;
import com.bjb.dto.Pair;
import com.bjb.repository.CollectingAgentRepository;
import com.bjb.repository.FeatureMappingRepository;
import com.bjb.repository.FeatureRepository;

@Service("collectingAgentService")
public class CollectingAgentServiceImpl implements CollectingAgentService {


	private CollectingAgentRepository collectingAgentRepository;
	
	private FeatureMappingRepository featureMappingRepository;
	
	private FeatureRepository featureRepository;

	public CollectingAgentRepository getCollectingAgentRepository() {
		return this.collectingAgentRepository;
	}
	

	public FeatureRepository getFeatureRepository() {
		return featureRepository;
	}

	@Autowired
	public void setFeatureRepository(FeatureRepository featureRepository) {
		this.featureRepository = featureRepository;
	}

	@Autowired
	public void setCollectingAgentRepository(CollectingAgentRepository collectingAgentRepository) {
		this.collectingAgentRepository = collectingAgentRepository;
	}
	
	public FeatureMappingRepository getFeatureMappingRepository() {
		return featureMappingRepository;
	}

	@Autowired
	public void setFeatureMappingRepository(FeatureMappingRepository featureMappingRepository) {
		this.featureMappingRepository = featureMappingRepository;
	}
	
	public Integer getCaId(Integer featureId,Integer aggId){
		return featureMappingRepository.findByFeatureIdAndAggregatorId(featureId, aggId).getCollectingAgentId();
	}

	@Override
	public Pair<Boolean, String> getValidCA(Integer featureId,Integer aggId) {
		Integer caId = getCaId(featureId, aggId);
		if (collectingAgentRepository.findById(caId).getStatus().equals(Integer.valueOf(1))) {
			return new Pair<>(true, Messages.AGG_FOUND);
		} else {
			return new Pair<>(false, Messages.AGG_CA_DISABLED);
		}
	}
	
	@Override
	public String getFeatureCommisionAccountNo(Integer featureid){
		return featureRepository.getOne(featureid).getCommisionAccountNo();
	}
	


}
