package com.bjb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmailSupport {
	
	static Logger log = LoggerFactory.getLogger(EmailSupport.class);

	EmailValidatorService emailValidatorService;
	
	@Autowired
	public void setEmailValidatorService(EmailValidatorService emailValidatorServiceRepo){
		emailValidatorService = emailValidatorServiceRepo;
	}
	/**
	 * Send email based on aggregator setting.
	 * @param aggCode
	 * @param msg
	 */
	public void sendEmail(String aggCode,String msg){
		log.info("START TO SEND EMAIL");
		if(emailValidatorService.doSendEmail(aggCode)){
			emailValidatorService.sendEmail(aggCode, msg);
		}
	}
}
