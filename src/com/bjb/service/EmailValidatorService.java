package com.bjb.service;

public interface EmailValidatorService {

	boolean doSendEmail(String aggCode);
	void sendEmail(String aggregatorCode, String msg);

}
