package com.bjb.service;

import com.bjb.dto.Pair;

public interface CollectingAgentService {

	Pair<Boolean, String> getValidCA(Integer featureId, Integer aggId);

	String getFeatureCommisionAccountNo(Integer featureid);
}
