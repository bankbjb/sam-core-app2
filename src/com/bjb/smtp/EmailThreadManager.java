//package com.bjb.smtp;
//
//import java.util.concurrent.FutureTask;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.task.TaskExecutor;
//import org.springframework.stereotype.Component;
//
///**
// * Scheduler for sending notification email.
// * @author arifino
// *
// */
//@Component
//public class EmailThreadManager{
//
//	private static EmailThreadManager etm;
//	
//	public static EmailThreadManager getEtm() {
//		if(etm==null){
//			etm = new EmailThreadManager();
//		}
//		return etm;
//	}
//
//	public static void setEtm(EmailThreadManager etm) {
//		EmailThreadManager.etm = etm;
//	}
//
//	@Autowired
//	private static TaskExecutor taskExecutor;
//
//	public void sendEmail(EmailUtil emu){
//		FutureTask<Integer> task = new FutureTask<Integer>(emu);
//		taskExecutor.execute(task);
//	}
//	
//}
