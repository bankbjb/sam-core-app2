package com.bjb.smtp;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.model.Aggregators;

/**
 * Sending email action.
 * @author arifino
 *
 */
@Component
public class SendMailSSL {
	
	private static Logger log = LoggerFactory.getLogger(SendMailSSL.class);
	
	public static String host = "10.12.14.3";
	public static String auth = "true";
	public static String port = "25"; 
	public static String mailer = "admin.sam@BANKBJB.co.id";
	public static String password = "1234";
	public static String subject = "SAM - BALANCE NOTIFICATION";
	public static String text = "Dear #{#username},\n"
			+ "This is to notify that your balance is below limit.\n"
			+ "Current balance: #{#curBalance}\n"
			+ "Minimum balance: #{#minBalance}\n"
			+ "Please top up your balance to prevent transaction failure."
			+ "Thank you,\n"
			+ "SAM Support";

	public static boolean inited = false;

	public static void init(){
		if(!inited){
			//System.out.println("INIT");
			try{
				if(ForSystemParameter.getSysValue(Constants.MAIL_SMTP_HOST)!=null){
					host = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_HOST);
					//System.out.println(host);
				}
				if(ForSystemParameter.getSysValue(Constants.MAIL_SMTP_AUTH)!=null){
					auth = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_AUTH);
					//System.out.println("auth");
				}
				if(ForSystemParameter.getSysValue(Constants.MAIL_SMTP_PORT)!=null){
					port = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_PORT);
					//System.out.println(port);
				}
				if(ForSystemParameter.getSysValue(Constants.MAIL_MAILER_ACCOUNT)!=null){
					mailer = ForSystemParameter.getSysValue(Constants.MAIL_MAILER_ACCOUNT);
					//System.out.println(mailer);
				}
				if(ForSystemParameter.getSysValue(Constants.MAIL_MAILER_PASSWORD)!=null){
					password = ForSystemParameter.getSysValue(Constants.MAIL_MAILER_PASSWORD);
					//System.out.println(password);
				}
				if(ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_SUBJECT)!=null){
					subject = ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_SUBJECT);
					//System.out.println(subject);
				}
				if(ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_TEXT)!=null){
					text = ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_TEXT);
					//System.out.println(text);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("EMAIL PROPS INITED");
		}
	}
	
	
	/**
	 * Send email
	 * @param agg
	 * @param saldo
	 */
	public static void sendEmail(Aggregators agg, String saldo){
		//System.out.println("SEND EMAIL");
		init();
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", auth);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", port);
		//Session session = Session.getDefaultInstance(props);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailer, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailer));
			if(agg.getEmailPic()!=null){
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(agg.getEmailPic()));
				message.setSubject(subject);
				for (int i = 0; i < 3; i++) {
					text = text.replaceFirst("#.*}","change_"+i);	
				}
				text = text.replaceAll("change_0", agg.getName());
				text = text.replaceAll("change_1", saldo);
				text = text.replaceAll("change_2", agg.getMinBalance());
				message.setText(text);

				Transport.send(message);

				log.info(Messages.MAIL_SENT);
			} else throw new MessagingException();

		} catch (MessagingException e) {
			e.printStackTrace();
			log.warn(Messages.MAIL_SENT_ERROR);
		}
	}
}