package com.bjb.smtp;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.cache.ForAggregators;
import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;
import com.bjb.iso.main.OpenSocketConnector;
import com.bjb.iso.util.ISOHelper;
import com.bjb.iso.util.SamPackager;
import com.bjb.model.Aggregators;
import com.bjb.model.EmailLogNotification;
import com.bjb.repository.EmailLogNotificationRepository;
import com.bjb.resource.main.InquirySaldoRequest;
import com.bjb.resource.main.InquirySaldoResponse;
import com.bjb.webservice.util.WSUtil;

/**
 * For instantiating sending email action.
 * 
 * @author arifino
 *
 */

@Component
public class EmailUtil implements Callable<Integer> {
	private String aggCode;
	private String msg;
	boolean run = true;

	private static EmailLogNotificationRepository emailLogNotificationRepository;

	private static SamPackager samPackager;
	
	@Autowired
	public void setEmailLogNotificationRepository(EmailLogNotificationRepository repo){
		emailLogNotificationRepository = repo;
	}
	
	@Autowired
	public void setSamPackager(SamPackager saam){
		samPackager = saam;
	}
	
	public boolean doSendEmail(String aggCode) {
		java.sql.Date date = new Date(new java.util.Date().getTime());
		EmailLogNotification em = emailLogNotificationRepository.findByAggregatorIdAndDate(Integer.valueOf(aggCode),
				date);
		int max = 3;
		if (ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_MAX) != null) {
			max = Integer.valueOf(ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_MAX));
		}
		if (em != null) {
			if (em.getRetry() < max) {
				int r = em.getRetry() + 1;
				em.setRetry(r);
				em.setLastSendEmail(new Timestamp(new java.util.Date().getTime()));
				emailLogNotificationRepository.save(em);
				return true;
			} else
				return false;
		} else {
			em = new EmailLogNotification();
			em.setAggregatorId(Integer.valueOf(aggCode));
			em.setAggregatorCode(aggCode);
			em.setDate(date);
			em.setRetry(1);
			em.setLastSendEmail(new Timestamp(new java.util.Date().getTime()));
			emailLogNotificationRepository.save(em);
			return true;
		}
	}

	public boolean checknotificationemail(String aggregatorCode, String msg) {
		if (ForAggregators.getValid(aggregatorCode, msg)) {
			// System.out.println("check valid");
			Aggregators agg = ForAggregators.getAggregators(aggregatorCode);
			if (agg != null && agg.getStatusNotification().equals(Integer.valueOf(1))) {
				// System.out.println(agg);
				String accNo = agg.getAccountNo();
				if (accNo != null) {
					InquirySaldoRequest request = new InquirySaldoRequest();
					request.setAggregatorCode(aggregatorCode);
					// request.setAdditionalData(additionalData);
					// request.setAmountTransaction(amountTransaction);
					// request.setCaCode(caCode);
					// request.setCurrencyCode(currencyCode);
					// request.setPay(pay);
					// request.setPrimaryAccountNumber(primaryAccountNumber);
					request.setProcessingCode("301000");
					request.setSourceAccountNumber(accNo);
					// request.setSystemTraceAuditNumber(systemTraceAuditNumber);
					// request.setTerminalAggregator(terminalAggregator);
					// request.setTrack2Data(track2Data);
					// request.setTransmissionDateTime(transmissionDateTime);
					// samLogger.logRequest(request, "XML", "inquiry_saldo");
					ISOMsg inquirySaldoReq = new ISOMsg();
					inquirySaldoReq.setPackager(samPackager);

					ISOHelper helper = new ISOHelper();
					try {
						inquirySaldoReq = helper.generateRequestISOMessage(request, inquirySaldoReq);
						inquirySaldoReq.dump(System.out, "");
						// samLogger.logTransaction(inquirySaldoReq, request);
					} catch (ISOException e) {
						e.printStackTrace();
					}

					System.out.println("starting inquiry saldo for balance checking");

					OpenSocketConnector connector = OpenSocketConnector.getConnector();
					try {
						connector.send(connector.generateISOMessageByte(inquirySaldoReq));
					} catch (Exception e) {
						e.printStackTrace();
					}
					ISOMsg reply;
					reply = WSUtil.findResponse(inquirySaldoReq);
					// generate response for client
					reply.dump(System.err, "");
					InquirySaldoResponse response = new InquirySaldoResponse();
					response = helper.generateResponseWS(response, reply);
					// samLogger.logTransaction(reply,response);
					// send email
					Long saldo = Long.parseLong(response.getInquirySaldo().substring(2));
					// System.out.println("saldo : "+saldo);
					Long minSaldo = Long.parseLong(agg.getMinBalance());
					// System.out.println("min balance : "+minSaldo);
					if (saldo < minSaldo) {
						// check if logged today
						// if(ForAggregators.getValid(aggregatorCode,msg)){
						// send email
						// System.out.println("send email");
						SendMailSSL.sendEmail(agg, String.valueOf(saldo));
						// }
						return false;
					} else
						return false;
				} else
					return false;
			} else
				return false;
		} else
			return false;
	}

	@Override
	public Integer call() throws Exception {
		// if (doSendEmail(aggCode)){
		if (checknotificationemail(aggCode, msg)) {
			return 1;
		} else
			return 0;
		// } else return 0;
	}
}
