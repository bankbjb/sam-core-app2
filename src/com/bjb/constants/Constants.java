package com.bjb.constants;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * This class store all constants used in the system.
 * @author arifino
 *
 */
public class Constants {

	//Base config used constants.
	//IF_400 GROUP
	/**
	 * [IF_400] Label for IF 400 IP address in database
	 */
	public static final String IF_400_IP = "IF_400_IP";
	/**
	 * [IF_400] Label for IF 400 port in database
	 */
	public static final String IF_400_PORT = "IF_400_PORT";
	/**
	 * [IF_400] Label for IF 400 timeout time in database
	 */
	public static final String IF_400_TIMEOUT = "IF_400_TIMEOUT";
	/**
	 * [IF_400] Label for IF 400 echo time in database
	 */
	public static final String IF_400_ECHO = "IF_400_ECHO";
	/**
	 * [IF_400] Label for IF 400 connect retry time in database
	 */
	public static final String IF_400_CONNECT = "IF_400_CONNECT";
	//WEB_SERVICE GROUP
	/**
	 * [WEB_SERVICE] Label for Web Service IP address in database
	 */
	public static final String WEB_SERVICE_IP = "WEB_SERVICE_IP";
	/**
	 * [WEB_SERVICE] Label for Web Service port in database
	 */
	public static final String WEB_SERVICE_PORT = "WEB_SERVICE_PORT";

	
	/**
	 * [CHARSET] Standard Charsets used in the system : ISO 8859 1
	 */
	public static final Charset DEFAULT_CHARSET = StandardCharsets.ISO_8859_1;
	
	/**
	 * [SAM_LABEL] Label for purchase transaction
	 */
	public static final String LABEL_PURCHASE = "purchase";
	/**
	 * [SAM_LABEL] Label for inquiry saldo transaction
	 */
	public static final String LABEL_INQUIRY_SALDO = "inquiry_saldo";
	/**
	 * [SAM_LABEL] Label for inquiry payment transaction
	 */
	public static final String LABEL_INQUIRY_PAYMENT = "inquiry_payment";
	/**
	 * [SAM_LABEL] Label for payment transaction
	 */
	public static final String LABEL_PAYMENT = "payment";
	
	//label for logging scheduler
	/**
	 * [CORE_SAM_LOG] Label for max logger in database [UNUSED]
	 */
	public static final String CORE_SAM_LOG_MAX_LOGGER = "CORE_SAM_LOG_MAX_LOGGER";
	/**
	 * [CORE_SAM_LOG] Label for log delay in database [UNUSED]
	 */
	public static final String CORE_SAM_LOG_DELAY = "CORE_SAM_LOG_DELAY";
	
	//label for dblistener
	/**
	 * [DB] Stores driver name of the database
	 */
	public static final String DB_DRIVER_NAME = "org.postgresql.Driver";
	/**
	 * [DB] Stores database url 
	 */
	public static final String DB_URL = "jdbc:postgresql:";
	/**
	 * [DATABASE] Label for database ip in database for DBListener.class
	 */
	public static final String DATABASE_IP = "DATABASE_IP";
	/**
	 * [DATABASE] Label for database port in database for DBListener.class
	 */
	public static final String DATABASE_PORT = "DATABASE_PORT";
	/**
	 * [DATABASE] Label for database name in database for DBListener.class
	 */
	public static final String DATABASE_NAME = "DATABASE_NAME";
	/**
	 * [DATABASE] Label for database username in database for DBListener.class
	 */
	public static final String DATABASE_USERNAME = "DATABASE_USERNAME";
	/**
	 * [DATABASE] Label for database password in database for DBListener.class
	 */
	public static final String DATABASE_PASSWORD = "DATABASE_PASSWORD";
	/**
	 * [NOTIFICATION] Label for notification trigger for aggregators(ins/upd)
	 */
	public static final String NOTIFICATION_AGGREGATOR = "aggregators_changed";
	/**
	 * [NOTIFICATION] Label for notification trigger for aggregators(del)
	 */
	public static final String NOTIFICATION_AGGREGATOR_DEL = "aggregators_changed_del";
	/**
	 * [NOTIFICATION] Label for notification trigger for featureMapping/commisionTiering(ins/upd)
	 */
	public static final String NOTIFICATION_FIFTYSEVEN = "fiftyseven_changed";
	/**
	 * [NOTIFICATION] Label for notification trigger for featureMapping/commisionTiering(del)
	 */
	public static final String NOTIFICATION_FIFTYSEVEN_DEL = "fiftyseven_changed_del";
	/**
	 * [NOTIFICATION] Label for notification trigger for messageMapping/fieldMapping(ins/upd)
	 */
	public static final String NOTIFICATION_SIXTYONE = "sixtyone_changed";
	/**
	 * [NOTIFICATION] Label for notification trigger for messageMapping/fieldMapping(del)
	 */
	public static final String NOTIFICATION_SIXTYONE_DEL = "sixtyone_changed_del";
	/**
	 * [NOTIFICATION] Label for notification trigger for systemParameter
	 */
	public static final String NOTIFICATION_PARAMETERS = "parameters_changed";
	/**
	 * [CORE] Label for stan cache number in database
	 */
	public static final String CORE_STAN_CACHE = "CORE_STAN_CACHE";
	/**
	 * [GROUP_LABEL] Label for Core Constants
	 */
	public static final String GROUP_LABEL_CORE = "CORE";
	/**
	 * [GROUP_LABEL] Label for Database Constants
	 */
	public static final String GROUP_LABEL_DATABASE = "DATABASE";
	/**
	 * [GROUP_LABEL] Label for procode inquiry payment in database  
	 */
	public static final String PRO_CODE_INQ_PAYMENTS = "TRX_CODE_INQ_PAYMENT";
	/**
	 * [GROUP_LABEL] Label for procode payment in database  
	 */
	public static final String PRO_CODE_PAYMENTS = "TRX_CODE_PAYMENT";
	/**
	 * [GROUP_LABEL] Label for IF 400 Constants
	 */
	public static final String GROUP_LABEL_IF_400 = "IF_400";
	/**
	 * [GROUP_LABEL] Label for Web Service Constants
	 */
	public static final String GROUP_LABEL_WEB_SERVICE = "WEB_SERVICE";
	/**
	 * [GROUP_LABEL] Label for Mail Constants
	 */
	public static final String GROUP_LABEL_MAIL = "MAIL";
	/**
	 * [GROUP_LABEL] Label for Retention Constants
	 */
	public static final String GROUP_LABEL_RETENTION = "RETENTION";
	/**
	 * [GROUP_LABEL] Label for Balance Constants
	 */
	public static final String GROUP_LABEL_BALANCE = "BALANCE";
	/**
	 * [MAIL] Label for smtp host address in database
	 */
	public static final String MAIL_SMTP_HOST = "MAIL_SMTP_HOST";
	/**
	 * [MAIL] Label for smtp authentication option in database
	 */
	public static final String MAIL_SMTP_AUTH = "MAIL_SMTP_AUTH";
	/**
	 * [MAIL] Label for smtp port in database
	 */
	public static final String MAIL_SMTP_PORT = "MAIL_SMTP_PORT";
	/**
	 * [MAIL] Label for mailer account in database
	 */
	public static final String MAIL_MAILER_ACCOUNT = "MAIL_MAILER_ACCOUNT";
	/**
	 * [MAIL] Label for mailer password in database
	 */
	public static final String MAIL_MAILER_PASSWORD = "MAIL_MAILER_PASSWORD";
	/**
	 * [BALANCE] Label for balance notification subject for email in database
	 */
	public static final String BALANCE_NOTIF_SUBJECT = "BALANCE_NOTIF_SUBJECT";
	/**
	 * [BALANCE] Label for balance notification text for email in database
	 */
	public static final String BALANCE_NOTIF_TEXT = "BALANCE_NOTIF_TEXT";
	/**
	 * [MAIL] Label for max email thread in database [UNUSED]
	 */
	public static final String MAIL_MAX_THREAD = "MAIL_MAX_THREAD";
	/**
	 * [BALANCE] Label for maximum notification/day in database
	 */
	public static final String BALANCE_NOTIF_MAX = "BALANCE_NOTIF_MAX";
	/**
	 * [CORE] Label for SAM port in database
	 */
	public static final String CORE_SAM_PORT = "CORE_SAM_PORT";
	/**
	 * [CLEANSING] Label for time to cleansing table log to history
	 */
	public static final String CLEANSING_LOG_TIME = "CLEANSING_LOG_TIME";
	/**
	 * [TRX] Code for inquiry payment 
	 */
	public static final String TRX_CODE_INQ_PAYMENT = "341018";
	/**
	 * [TRX] Code for payment
	 */
	public static final String TRX_CODE_PAYMENT = "541018";
	
	/**
	 * [TRX] Code for inquiry payment SIPANDU *NEW*
	 */
	public static final String TRX_CODE_INQ_PAYMENT_SIPANDU = "300020";
	/**
	 * [TRX] Code for inquiry payment EDUPAY *NEW*
	 */
	public static final String TRX_CODE_INQ_PAYMENT_EDUPAY = "321000";
	/**
	 * [TRX] Code for inquiry payment ESAMSAT *NEW*
	 */
	public static final String TRX_CODE_INQ_PAYMENT_ESAMSAT = "301099";
	/**
	 * [TRX] Code for payment SIPANDU *NEW*
	 */
	public static final String TRX_CODE_PAYMENT_SIPANDU = "500020";
	/**
	 * [TRX] Code for inquiry payment PDAM *NEW*
	 */
	public static final String TRX_CODE_INQ_PAYMENT_PDAM = "321066";
	/**
	 * [TRX] Code for payment PDAM *NEW*
	 */
	public static final String TRX_CODE_PAYMENT_PDAM = "521066";
	/**
	 * [TRX] Code for payment EDUPAY *NEW*
	 */
	public static final String TRX_CODE_PAYMENT_EDUPAY = "521000";
	/**
	 * [TRX] Code for payment ESAMSAT *NEW*
	 */
	public static final String TRX_CODE_PAYMENT_ESAMSAT = "541099";
	/**
	 * [TRX] Code for purchase
	 */
	public static final String TRX_CODE_PURCHASE = "501089";
	/**
	 * [GROUP_LABEL] Label for RC Constants
	 */
	public static final String GROUP_LABEL_RC = "RC";
	
	/**
	 * [RC] Label for SAM_001 : Response Code if Aggregator is invalid
	 */
	public static final String RC_SAM_001 = "RC_SAM_001";
	/**
	 * [RC] Label for SAM_002 : Response Code if required item is not available in request (ex: Billinfo1,Billinfo2 is mapped to A,B but Billinfo2 is not available in request)
	 */
	public static final String RC_SAM_002 = "RC_SAM_002";
	/**
	 * [RC] Label for SAM_003 : Response Code if mapping variable incomplete (ex: A,B,D -> Missing C)
	 */
	public static final String RC_SAM_003 = "RC_SAM_003";
	/**
	 * [RC] Label for SAM_004 : Response Code if mapping for variable A doesn't exist
	 */
	public static final String RC_SAM_004 = "RC_SAM_004";
	/**
	 * [RC] Label for SAM_005 : Response Code if bit61 from IF400 can't be mapped based on mapping data
	 */
	public static final String RC_SAM_005 = "RC_SAM_005";
	/**
	 * [RC] Label for SAM_006 : Response Code if feature code not found
	 */
	public static final String RC_SAM_006 = "RC_SAM_006";
	/**
	 * [RC] Label for SAM_007 : Response Code if exception happen in ISO Messaging
	 */
	public static final String RC_SAM_007 = "RC_SAM_007";
	/**
	 * [RC] Label for SAM_008 : Response Code if SAM doesn't receive response from IF400
	 */
	public static final String RC_SAM_008 = "RC_SAM_008";
	/**
	 * [RC] Label for SAM_009 : Response Code if exception happen
	 */
	public static final String RC_SAM_009 = "RC_SAM_009";
	/**
	 * [RC] Label for SAM_010 : Response Code if aggregator's collecting agent is disabled
	 */
	public static final String RC_SAM_010 = "RC_SAM_010";
	/**
	 * [RC] Label for SAM_011 : Response Code if aggregator is disabled
	 */
	public static final String RC_SAM_011 = "RC_SAM_011";
	/**
	 * [RC] Label for SAM_012 : Response Code if aggregator's message type mismatch with used request
	 */
	public static final String RC_SAM_012 = "RC_SAM_012";
	/**
	 * [RC] Label for SAM_013 : Response Code if aggregator's pks date expired
	 */
	public static final String RC_SAM_013 = "RC_SAM_013";
	/**
	 * [RC] Stores Response Code success = "00"
	 */
	public static final String RC_SAM_SUCCESS = "00";
	/**
	 * [RC] Stores Response Code response not found = "99"
	 */
	public static final Object RC_SAM_NF = "99";
	/**
	 * [NOTIFICATION] Label for notification trigger for cron
	 */
	public static final String NOTIFICATION_CRON = "cron_changed";
	public static final String GROUP_LABEL_CLEANSING = "CLEANSING";
	public static final String PREFIX_AGGREGATOR = "W";
	public static final String PREFIX_COLLECTINGAGENT = "CA";
	public static final String PREFIX_FEATURE = "P";
	public static final String VAR_EMPTY = "empty";
	public static final String CORE_BIT2 = "CORE_BIT2";
	public static final String CORE_BIT35 = "CORE_BIT35";
	public static final String CORE_CURRENCY = "360";
	public static final String CORE_PAY = "JAB";
	public static final String CORE_TA_CANAME = "CANAME";
	public static final String CORE_TA_TIN = "7779";
	public static final String CORE_TA_TN = "027779";

	
	
	
	/**
	 * Return HEADER for ISO_DUMP
	 * @param string
	 * @param string2
	 * @param string3
	 * @return
	 */
	public static String ISO_DUMP_HEADER(String string, String string2, String string3) {
		return "["+string+"]["+string2+"]["+string3+"]";
	}
}
