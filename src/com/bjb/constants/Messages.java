package com.bjb.constants;

public class Messages {
	public static final String DB_LISTENER_START = "<<< STARTING DB LISTENER >>>";
	public static final String DB_LISTENER_LISTEN = "DBLISTENER RECEIVES NOTIFICATION -> ";
	
	public static final String AGG_PKS_DATE_EXPIRED = "PKS DATE EXPIRED";
	public static final String AGG_CA_DISABLED = "AGG CA DISABLED";
	public static final String AGG_MESSAGE_TYPE_MISMATCH = "AGG MSG TYPE MISMATCH";
	public static final String AGG_DISABLED = "AGG DISABLED";
	public static final String CACHE_AGG_UPDATED = "<<< Cache Aggregators is Updated >>>";
	public static final String CACHE_AGG_UPDATE_ERROR = "<<< Something wrong with aggregators update >>>";
	public static final String CACHE_57_UPDATED = "<<< Cache for Bit 57 is updated >>>";
	public static final String CACHE_57_UPDATE_ERROR = "<<< Something wrong with commision_tiering/feature_mapping update >>>";
	public static final String CACHE_61_UPDATE = "<<< Cache bit Sixty One is updated >>>";
	public static final String CACHE_61_UPDATE_ERROR = "<<< Something wrong with message_mapping/field_mapping/message_fields update >>>";
	public static final String CACHE_SYSPARAM_UPDATED = "<<< SystemParameter is updated >>>";
	public static final String HIBERNATE_UTIL_ERROR = "<<< Initial SessionFactory creation failed >>>";
	public static final String CLEAN_LOG_START = "<<< SCHEDULING CLEANSING LOG >>>";
	public static final String CLEAN_LOG_SEQUENCE = "<<< STARTING CLEANSING SEQUENCE >>>";
	public static final String CLEAN_LOG_END = "<<< CLEANSING SEQEUNCE ENDED >>>";
	public static final String SOCKET_START = "<<< STARTING THREAD OPEN SOCKET CONNECTOR >>>";
	public static final String SOCKET_START_ERROR = "<<< FAILED TO CONNECT TO SERVER, TRY RESTART SERVER/SAM >>>";
	public static final String SOCKET_ECHO_SENT = "<<< Echo Sent >>>";
	public static final String SOCKET_BROKEN = "<<< BROKEN CONNECTION >>>";
	public static final String SOCKET_RETRY = "<<< EXCEPTION ON CONNECT(), RETRY >>>";
	public static final String LOG_ERROR = "<<< LOGGING ERROR >>>";
	public static final String MAIL_SENT = "<<< MAIL SENT >>>";
	public static final String MAIL_SENT_ERROR = "<<< ERROR SENDING EMAIL >>>";
	public static final String UNRECOGNIZED_NOTIFICATION = "<<< UNRECOGNIZED NOTIFICATION >>>";
	public static final String CACHE_INIT = "<<< INITIATING CACHE >>>";
	public static final String CACHE_INITIATED = "<<< CACHE INITIATED SUCCESSFULLY >>>";
	public static final String INIT_ECHO = "<<< INITIATING ECHO >>>";
	public static final String ECHO_INITED = "<<< ECHO INITIATED >>>";
	public static final String NOTIFICATION_MAIL_SENT = "<<< NOTIFICATION MAIL SENT >>>";
	public static final String AGG_RETRIEVING = "<<< RETRIEVING AGGREGATOR DATA >>>";
	public static final String AGG_FOUND = "<<< AGGREGATOR DATA FOUND >>>";
	public static final String AGG_NOT_FOUND = "<<< AGGREGATOR NOT FOUND >>>";
	public static final String AGG_VALID = "<<< AGGREGATOR VALID >>>";
	public static final String AGG_CA_FEATURE_NOT_FOUND = "<<< AGGREGATOR DOESN'T HAVE ANY RELATION WITH ANY CA TIED TO FEATURE >>>";
	public static final String SOCKET_TRY_CONNECT(String host,int port){
		return "<<< CONNECTING TO " + host + " : " + port+" >>>";
	}
	public static String GENERATE_MESSAGE(String label) {
		return "<<< GENERATING "+label+" MESSAGE >>>";
	}
	public static String SEND_MESSAGE(String label) {
		return "<<< TRYING TO SEND "+label+" MESSAGE >>>";
	}
	public static String MESSAGE_SENT(String label) {
		return "<<< "+label+" Message Sent >>>";
	}
	
}
