package com.bjb.iso.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is for instantiating logging action.
 * 
 * @author arifino
 *
 */
public class SAMLog {

	@SuppressWarnings("unused")
	private static Logger logg = LoggerFactory.getLogger(SAMLog.class);
	
	long delay;
	Object loggedObject;
	Object loggedObject2;
	String type;
	String event;
	int code; // 00 for message 01 for transaction 02 for message double

	public SAMLog() {

	}

	public SAMLog(long delay, Object loggedObject, String type, String event, int a) {
		super();
		this.delay = delay;
		this.loggedObject = loggedObject;
		this.type = type;
		this.event = event;
		this.code = a;
	}

	public SAMLog(long delay, Object loggedObject, Object log2, String type, String event, int a) {
		super();
		this.delay = delay;
		this.loggedObject = loggedObject;
		this.loggedObject2 = log2;
		this.type = type;
		this.event = event;
		this.code = a;
	}

	
//	@Override
//	public Integer call() throws Exception {
//		int res = 0;
//		try {
//			// System.out.println("Delay Log : "+delay/1000);
//			Thread.sleep(delay);
//			if (code == 0) {
//				res = log(loggedObject, type, event);
//			} else if (code == 1) {
//				res = log(loggedObject);
//			} else if (code == 2) {
//				res = log(loggedObject, loggedObject2, type, event);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			Thread.sleep(delay / 2);
//			if (code == 0) {
//				res = log(loggedObject, type, event);
//			} else if (code == 1) {
//				res = log(loggedObject);
//			} else if (code == 2) {
//				res = log(loggedObject, loggedObject2, type, type);
//			}
//		}
//		return res;
//	}
}
