package com.bjb.iso.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.cache.ForAdditionalInfo;
import com.bjb.cache.ForAggregators;
import com.bjb.cache.ForStan;
import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;
import com.bjb.iso.main.SAMISOServer;
import com.bjb.resource.additional.AdditionalDataPayment;
import com.bjb.resource.additional.BillInformation;
import com.bjb.resource.main.EchoRequest;
import com.bjb.resource.main.InquiryPaymentRequest;
import com.bjb.resource.main.InquiryPaymentResponse;
import com.bjb.resource.main.InquirySaldoRequest;
import com.bjb.resource.main.InquirySaldoResponse;
import com.bjb.resource.main.PaymentRequest;
import com.bjb.resource.main.PaymentResponse;
import com.bjb.resource.main.PurchaseRequest;
import com.bjb.resource.main.PurchaseResponse;
import com.bjb.resource.main.SignOnRequest;
import com.bjb.util.CommonUtils;

/**
 * 
 * @author satrio
 * 
 *         Goal : Utilization for ISO Messaging Create Date : Sep 3, 2016
 */
public class ISOHelper {

	private static String stan;
	
	private static Logger log = LoggerFactory.getLogger(ISOHelper.class);

	public static String getStan() {
		stan = ForStan.getStanNow();
		return stan;
	}

	public String byteToString(byte[] data) {
		return new String(data, Constants.DEFAULT_CHARSET);
	}

	public ISOMsg generateSignOnMessage(SignOnRequest request, ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(70, "001");

		return msg;
	}

	public ISOMsg generateEchoMessage(EchoRequest request, ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(70, "301");

		return msg;
	}

	public ISOMsg generateEchoMessage(ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(70, "301");

		return msg;
	}

	/**
	 * 
	 * Goal : generate iso message request from input (inquiry saldo) Create
	 * Date : Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return ISOMsg
	 * @throws ISOException
	 */
	public ISOMsg generateRequestISOMessage(InquirySaldoRequest req, ISOMsg msg) throws ISOException {
		try {
			msg.setMTI("0200");
			msg.set(2, ForSystemParameter.getSysValue(Constants.CORE_BIT2));
			msg.set(3, req.getProcessingCode());
			msg.set(4, req.getAmountTransaction());
			msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
			msg.set(11, getStan());
			msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
			msg.set(35, ForSystemParameter.getSysValue(Constants.CORE_BIT35));
			msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
			msg.set(42, req.getTerminalAggregator().getTerminalName());
			msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
			msg.set(49, req.getCurrencyCode());
			msg.set(59, req.getPay()); // PAY
			msg.set(63, req.getAdditionalData()); // 01
			msg.set(102, ForAggregators.getAggregatorAccount(msg.getString(32)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * 
	 * Goal : generate iso message request from input (purchase) Create Date :
	 * Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 */
	/*public ISOMsg generateRequestISOMessage(PurchaseRequest req, ISOMsg msg) throws ISOException {
		try {
			msg.setMTI("0200");
			msg.set(2, req.getPrimaryAccountNumber());
			msg.set(3, req.getProcessingCode());
			msg.set(4, req.getAmountTransaction());
			msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
			msg.set(11, getStan());
			msg.set(12, req.getTimeLocalTransaction());
			msg.set(15, req.getSettlementDate());
			msg.set(18, req.getMerchantType());
			msg.set(22, req.getPosEntryModeCode());
			msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
			msg.set(35, req.getTrack2Data());
			msg.set(37, req.getSequenceNumber());
			msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
			msg.set(42, req.getTerminalAggregator().getTerminalName());
			msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
			msg.set(49, req.getCurrencyCode());
			msg.set(57, req.getAmountInformation().pack(req));
			msg.set(59, req.getPay());
			msg.set(60, req.getFeatureCode());
			msg.set(61, req.getAdditionalData());// bill id+nominal pulsa
			msg.set(102, req.getSourceAccountNumber());
			msg.set(103, req.getDestinationAccountNumber());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}*/

	/**
	 * 
	 * Goal : generate iso message request from input (purchase) Create Date :
	 * Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public ISOMsg generateRequestISOMessage(PurchaseRequest req, ISOMsg msg, String msgtype)
			throws ISOException, IllegalArgumentException, IllegalAccessException {
		msg.setMTI("0200");
		msg.set(2, ForSystemParameter.getSysValue(Constants.CORE_BIT2));
		msg.set(3, req.getProcessingCode());
		msg.set(4, req.getAmountTransaction());
		msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(12, req.getTimeLocalTransaction());
		msg.set(15, req.getSettlementDate());
		msg.set(18, req.getMerchantType());
		msg.set(22, req.getPosEntryModeCode());
		msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
		msg.set(35, ForSystemParameter.getSysValue(Constants.CORE_BIT35));
		msg.set(37, CommonUtils.padleft(msg.getString(11),12,'0'));
		msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
		msg.set(42, req.getTerminalAggregator().getTerminalName());
		msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
		msg.set(49, req.getCurrencyCode());
		msg.set(57, req.getAmountInformation().pack(req));
		msg.set(59, req.getPay());
		msg.set(60, req.getFeatureCode());
		req.setAdditionalData(AdditionalDataPayment.packsixtyone(req, msgtype));
		msg.set(61, req.getAdditionalData());// bill id+nominal pulsa
		msg.set(102, ForAggregators.getAggregatorAccount(msg.getString(32)));
		log.info("bit102" + ForAggregators.getAggregatorAccount(msg.getString(32)));
		//add send glAcc and commFeaAcc
		String glAcc = ForAggregators.getAggregatorGlAccount(msg.getString(32));
		String commFeaAcc = ForAdditionalInfo.getFeatureCommisionAccountNo(Integer.valueOf(msg.getString(60)));
//		String bit103 = CommonUtils.padright(glAcc, 14, ' ')+CommonUtils.padright(commFeaAcc, 14, ' ');
		String bit103 = CommonUtils.padright(commFeaAcc, 14, ' ');
		//String bit103 = ForAggregators.getAggregatorGlAccount(msg.getString(32));
		msg.set(103, bit103);
		log.info("bit103" + bit103);
//		msg.set(103, ForAggregators.getAggregatorGlAccount(msg.getString(32)));
		return msg;
	}

	/**
	 * 
	 * Goal : generate iso message request from input (inquiry payment) Create
	 * Date : Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 *//*
	public ISOMsg generateRequestISOMessage(InquiryPaymentRequest req, ISOMsg msg) throws ISOException {
		try {
			msg.setMTI("0200");
			msg.set(2, req.getPrimaryAccountNumber());
			msg.set(3, req.getProcessingCode());
			msg.set(4, req.getAmountTransaction());
			msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
			msg.set(11, getStan());
			msg.set(12, req.getTimeLocalTransaction());
			msg.set(13, req.getLocalTransactionDate());
			msg.set(15, req.getSettlementDate());
			msg.set(18, req.getMerchantType());
			msg.set(22, req.getPosEntryModeCode());
			msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
			msg.set(35, req.getTrack2Data());
			msg.set(37, req.getSequenceNumber());
			msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
			msg.set(42, req.getTerminalAggregator().getTerminalName());
			msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
			msg.set(49, req.getCurrencyCode());
			// prepare bit 57
			msg.set(57, req.getAmountInformation().pack(req));
			// System.out.println(msg.getString(57));
			msg.set(59, req.getPay());
			msg.set(60, req.getFeatureCode());
			// prepare bit 61
			// req.setAdditionalData(AdditionalDataPayment.pack(msg));
			msg.set(61, req.getAdditionalData());
			msg.set(102, req.getSourceAccountNumber());
			msg.set(103, req.getDestinationAccountNumber());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}*/

	/**
	 * 
	 * Goal : generate iso message request from input (inquiry payment) Create
	 * Date : Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public ISOMsg generateRequestISOMessage(InquiryPaymentRequest req, ISOMsg msg, String mesType)
			throws ISOException, IllegalArgumentException, IllegalAccessException {
		msg.setMTI("0200");
		msg.set(2, ForSystemParameter.getSysValue(Constants.CORE_BIT2));
		msg.set(3, req.getProcessingCode());
		msg.set(4, req.getAmountTransaction());
		msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(12, req.getTimeLocalTransaction());
		msg.set(13, req.getLocalTransactionDate());
		msg.set(15, req.getSettlementDate());
		msg.set(18, req.getMerchantType());
		msg.set(22, req.getPosEntryModeCode());
		msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
		msg.set(35, ForSystemParameter.getSysValue(Constants.CORE_BIT35));
		msg.set(37, CommonUtils.padleft(msg.getString(11),12,'0'));
		msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
		msg.set(42, req.getTerminalAggregator().getTerminalName());
		msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
		
		/*Ayuda Add Ca code - 26-03-2018*/
		msg.set(48,CommonUtils.removeUntilNumber(req.getCaCode()));
		
		msg.set(49, req.getCurrencyCode());
		// prepare bit 57
		msg.set(57, req.getAmountInformation().pack(req));
		// System.out.println(msg.getString(57));
		msg.set(59, req.getPay());
		msg.set(60, req.getFeatureCode());
		// prepare bit 61
		req.setAdditionalData(AdditionalDataPayment.packsixtyone(req, mesType));
		msg.set(61, req.getAdditionalData());
		msg.set(102, ForAggregators.getAggregatorAccount(msg.getString(32)));
//		log.info("bit102 = "+ ForAggregators.getAggregatorAccount(msg.getString(32)));
		//suwardi
		String commFeaAcc = ForAdditionalInfo.getFeatureCommisionAccountNo(Integer.valueOf(msg.getString(60).trim()));
		String bit103 = CommonUtils.padright(commFeaAcc, 14, ' ');
		msg.set(103, bit103);
//		log.info("bit103 = "+ bit103);
		//msg.set(103, ForAggregators.getAggregatorGlAccount(msg.getString(32)));
		return msg;
	}

	/**
	 * 
	 * Goal : generate iso message request from input (payment) Create Date :
	 * Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 */
	/*public ISOMsg generateRequestISOMessage(PaymentRequest req, ISOMsg msg) throws ISOException {
		try {
			msg.setMTI("0200");
			msg.set(2, req.getPrimaryAccountNumber());
			msg.set(3, req.getProcessingCode());
			msg.set(4, req.getAmountTransaction());
			msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
			msg.set(11, getStan());
			msg.set(12, req.getTimeLocalTransaction());
			msg.set(13, req.getLocalTransactionDate());
			msg.set(15, req.getSettlementDate());
			msg.set(18, req.getMerchantType());
			msg.set(22, req.getPosEntryModeCode());
			msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
			msg.set(35, req.getTrack2Data());
			msg.set(37, req.getSequenceNumber());
			msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
			msg.set(42, req.getTerminalAggregator().getTerminalName());
			msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
			msg.set(49, req.getCurrencyCode());
			msg.set(57, req.getAmountInformation().pack(req));
			msg.set(59, req.getPay());
			msg.set(60, req.getFeatureCode());
			// req.setAdditionalData(AdditionalDataPayment.pack(msg));
			msg.set(61, req.getAdditionalData());
			msg.set(102, req.getSourceAccountNumber());
			msg.set(103, req.getDestinationAccountNumber());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}*/

	/**
	 * 
	 * Goal : generate iso message request from input (payment) Create Date :
	 * Sep 3, 2016
	 * 
	 * @param req
	 * @param msg
	 * @return
	 * @throws ISOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public ISOMsg generateRequestISOMessage(PaymentRequest req, ISOMsg msg, String mesType)
			throws ISOException, IllegalArgumentException, IllegalAccessException {
		try {
			msg.setMTI("0200");
			msg.set(2, ForSystemParameter.getSysValue(Constants.CORE_BIT2));
			msg.set(3, req.getProcessingCode());
			msg.set(4, req.getAmountTransaction());
			msg.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
			msg.set(11, getStan());
			msg.set(12, req.getTimeLocalTransaction());
			msg.set(13, req.getLocalTransactionDate());
			msg.set(15, req.getSettlementDate());
			msg.set(18, req.getMerchantType());
			msg.set(22, req.getPosEntryModeCode());
			msg.set(32, CommonUtils.removeUntilNumber(req.getAggregatorCode()));
			msg.set(35, ForSystemParameter.getSysValue(Constants.CORE_BIT35));
			msg.set(37, CommonUtils.padleft(msg.getString(11),12,'0'));
			msg.set(41, req.getTerminalAggregator().getTerminalIdentifierNumber());
			msg.set(42, req.getTerminalAggregator().getTerminalName());
			msg.set(43, req.getTerminalAggregator().getCardAcceptorName());
			
			/*Ayuda Add Ca code - 26-03-2018*/
			msg.set(48,CommonUtils.removeUntilNumber(req.getCaCode()));
			
			msg.set(49, req.getCurrencyCode());
			msg.set(57, req.getAmountInformation().pack(req));
			msg.set(59, req.getPay());
			msg.set(60, req.getFeatureCode());
			req.setAdditionalData(AdditionalDataPayment.packsixtyone(req, mesType));
			msg.set(61, req.getAdditionalData());
			msg.set(102, ForAggregators.getAggregatorAccount(msg.getString(32)));
			log.info("bit102 = "+ ForAggregators.getAggregatorAccount(msg.getString(32)));
			//add send glAcc and commFeaAcc
			String glAcc = ForAggregators.getAggregatorGlAccount(msg.getString(32));
			//suwardi
			String commFeaAcc = ForAdditionalInfo.getFeatureCommisionAccountNo(Integer.valueOf(msg.getString(60).trim()));
//			String bit103 = CommonUtils.padright(glAcc, 14, ' ')+CommonUtils.padright(commFeaAcc, 14, ' ');
			String bit103 = CommonUtils.padright(commFeaAcc, 14, ' ');
			log.info("bit103 = "+ bit103);
			//String bit103 = ForAggregators.getAggregatorGlAccount(msg.getString(32));
			msg.set(103, bit103);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * 
	 * Goal : generate response as output (inquiry saldo) Create Date : Sep 3,
	 * 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 */
	public InquirySaldoResponse generateResponseWS(InquirySaldoResponse response, ISOMsg msg) {

		try {
			response.setPrimaryAccountNumber(msg.getString(2));
			response.setProcessingCode(msg.getString(3));
			response.setAmountTransaction(msg.getString(4));
			response.setTransmissionDateTime(msg.getString(7));
			response.setSystemTraceAuditNumber(msg.getString(11));
			response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
			response.setTrack2Data(msg.getString(35));
			response.setResponseCode(msg.getString(39));
			response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
			response.getTerminalAggregator().setTerminalName(msg.getString(42));
			response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
			response.setCurrencyCode(msg.getString(49));
			response.setInquirySaldo(msg.getString(54));
			response.setAdditionalData(msg.getString(59));
			response.setSourceAccountNumber(msg.getString(102));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (purchase) Create Date : Sep 3, 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 */
	public PurchaseResponse generateResponseWS(PurchaseResponse response, ISOMsg msg) {
		try {
			response.setPrimaryAccountNumber(msg.getString(2));
			response.setProcessingCode(msg.getString(3));
			response.setAmountTransaction(msg.getString(4));
			response.setTransmissionDateTime(msg.getString(7));
			response.setSystemTraceAuditNumber(msg.getString(11));
			response.setTimeLocalTransaction(msg.getString(12));
			response.setSettlementDate(msg.getString(15));
			response.setMerchantType(msg.getString(18));
			response.setPosEntryModeCode(msg.getString(22));
			response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
			response.setTrack2Data(msg.getString(35));
			response.setResponseCode(msg.getString(39));
			response.setSequenceNumber(msg.getString(37));
			response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
			response.getTerminalAggregator().setTerminalName(msg.getString(42));
			response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
			response.setCurrencyCode(msg.getString(49));
			// bit 57
			response.getAmountInformation().unpack(msg.getString(57));
			// end bit 57
			response.setPay(msg.getString(59));
			response.setFeatureCode(msg.getString(60));
			response.setAdditionalData(msg.getString(61));
			response.setSourceAccountNumber(msg.getString(102));
//			log.info("response bit102 = "+ msg.getString(102));
			response.setDestinationAccountNumber(msg.getString(103));
//			log.info("response bit103 = "+ msg.getString(103));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * for testing ignore this
	 * 
	 * @param response
	 * @param msg
	 * @return
	 */
	public InquiryPaymentRequest generateResponseWS(InquiryPaymentRequest response, ISOMsg msg) {
		try {
			response.setPrimaryAccountNumber(msg.getString(2));
			response.setProcessingCode(msg.getString(3));
			response.setAmountTransaction(msg.getString(4));
			response.setTransmissionDateTime(msg.getString(7));
			response.setSystemTraceAuditNumber(msg.getString(11));
			response.setTimeLocalTransaction(msg.getString(12));
			response.setLocalTransactionDate(msg.getString(13));
			response.setSettlementDate(msg.getString(15));
			response.setMerchantType(msg.getString(18));
			response.setPosEntryModeCode(msg.getString(22));
			response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
			response.setTrack2Data(msg.getString(35));
			response.setSequenceNumber(msg.getString(37));
			// response.setResponseCode(msg.getString(39));
			response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
			response.getTerminalAggregator().setTerminalName(msg.getString(42));
			response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
			response.setCurrencyCode(msg.getString(49));
			// bit 57
			response.getAmountInformation().pack(msg.getString(57));
			log.info("response bit 57 = " + msg.getString(57));
			// end bit 57
			response.setPay(msg.getString(59));
			response.setFeatureCode(msg.getString(60));
			response.setAdditionalData(msg.getString(61));
			response.setSourceAccountNumber(msg.getString(102));
//			log.info("response bit102 = "+ msg.getString(102));
			response.setDestinationAccountNumber(msg.getString(103));
//			log.info("response bit103 = "+ msg.getString(103));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (inquiry payment) Create Date : Sep 3,
	 * 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 */
	public InquiryPaymentResponse generateResponseWS(InquiryPaymentResponse response, ISOMsg msg) {
		try {
			response.setPrimaryAccountNumber(msg.getString(2));
			response.setProcessingCode(msg.getString(3));
			response.setAmountTransaction(msg.getString(4));
			response.setTransmissionDateTime(msg.getString(7));
			response.setSystemTraceAuditNumber(msg.getString(11));
			response.setTimeLocalTransaction(msg.getString(12));
			response.setLocalTransactionDate(msg.getString(13));
			response.setSettlementDate(msg.getString(15));
			response.setMerchantType(msg.getString(18));
			response.setPosEntryModeCode(msg.getString(22));
			response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
			response.setTrack2Data(msg.getString(35));
			response.setSequenceNumber(msg.getString(37));
			response.setResponseCode(msg.getString(39));
			response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
			response.getTerminalAggregator().setTerminalName(msg.getString(42));
			response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
			response.setCurrencyCode(msg.getString(49));
			// bit 57
			response.getAmountInformation().unpack(msg.getString(57));
			// end bit 57
			response.setPay(msg.getString(59));
			response.setFeatureCode(msg.getString(60));
			response.setAdditionalData(msg.getString(61));
			response.setSourceAccountNumber(msg.getString(102));
//			log.info("response bit102 = "+ msg.getString(102));
			response.setDestinationAccountNumber(msg.getString(103));
//			log.info("response bit103 = "+ msg.getString(103));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (payment) Create Date : Sep 3, 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 */
	public PaymentResponse generateResponseWS(PaymentResponse response, ISOMsg msg) {
		try {
			response.setPrimaryAccountNumber(msg.getString(2));
			response.setProcessingCode(msg.getString(3));
			response.setAmountTransaction(msg.getString(4));
			response.setTransmissionDateTime(msg.getString(7));
			response.setSystemTraceAuditNumber(msg.getString(11));
			response.setTimeLocalTransaction(msg.getString(12));
			response.setLocalTransactionDate(msg.getString(13));
			response.setSettlementDate(msg.getString(15));
			response.setMerchantType(msg.getString(18));
			response.setPosEntryModeCode(msg.getString(22));
			response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
			response.setTrack2Data(msg.getString(35));
			response.setSequenceNumber(msg.getString(37));
			response.setResponseCode(msg.getString(39));
			response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
			response.getTerminalAggregator().setTerminalName(msg.getString(42));
			response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
			//** Add field 47 & 48 **//
			response.setNumberTransactionP(msg.getString(47));
			response.setNumberTransactionBank(msg.getString(48));
			
			response.setCurrencyCode(msg.getString(49));
			// bit 57
			response.getAmountInformation().unpack(msg.getString(57));
			// end bit 57
			response.setPay(msg.getString(59));
			response.setFeatureCode(msg.getString(60));
			response.setAdditionalData(msg.getString(61));
			response.setSourceAccountNumber(msg.getString(102));
//			log.info("response bit102 = "+ msg.getString(102));
			response.setDestinationAccountNumber(msg.getString(103));
//			log.info("response bit103 = "+ msg.getString(103));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (inquiry payment) Create Date : Sep 3,
	 * 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 * @throws IllegalAccessException
	 */
	public InquiryPaymentResponse generateResponseWS(InquiryPaymentResponse response, ISOMsg msg, String messageType)
			throws IllegalAccessException {
		response.setPrimaryAccountNumber(msg.getString(2));
		response.setProcessingCode(msg.getString(3));
		response.setAmountTransaction(msg.getString(4));
		response.setTransmissionDateTime(msg.getString(7));
		response.setSystemTraceAuditNumber(msg.getString(11));
		response.setTimeLocalTransaction(msg.getString(12));
		response.setLocalTransactionDate(msg.getString(13));
		response.setSettlementDate(msg.getString(15));
		response.setMerchantType(msg.getString(18));
		response.setPosEntryModeCode(msg.getString(22));
		response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
		response.setTrack2Data(msg.getString(35));
		response.setSequenceNumber(msg.getString(37));
		response.setResponseCode(msg.getString(39));
		response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
		response.getTerminalAggregator().setTerminalName(msg.getString(42));
		response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
		response.setCurrencyCode(msg.getString(49));
		// bit 57
		response.getAmountInformation().unpack(msg.getString(57));
		// end bit 57
		response.setPay(msg.getString(59));
		response.setFeatureCode(msg.getString(60));
		response.setBillInformation(new BillInformation());
		response.getBillInformation().setBills(AdditionalDataPayment.unpacksixtyone(msg, messageType));
		response.setAdditionalData(msg.getString(61));
		response.setSourceAccountNumber(msg.getString(102));
//		log.info("response bit102 = "+ msg.getString(102));
		response.setDestinationAccountNumber(msg.getString(103));
//		log.info("response bit103 = "+ msg.getString(103));
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (purchase) Create Date : Sep 3, 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 * @throws IllegalAccessException
	 */
	public PurchaseResponse generateResponseWS(PurchaseResponse response, ISOMsg msg, String mesType)
			throws IllegalAccessException {
		response.setPrimaryAccountNumber(msg.getString(2));
		response.setProcessingCode(msg.getString(3));
		response.setAmountTransaction(msg.getString(4));
		response.setTransmissionDateTime(msg.getString(7));
		response.setSystemTraceAuditNumber(msg.getString(11));
		response.setTimeLocalTransaction(msg.getString(12));
		response.setSettlementDate(msg.getString(15));
		response.setMerchantType(msg.getString(18));
		response.setPosEntryModeCode(msg.getString(22));
		response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
		response.setTrack2Data(msg.getString(35));
		response.setResponseCode(msg.getString(39));
		response.setSequenceNumber(msg.getString(37));
		response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
		response.getTerminalAggregator().setTerminalName(msg.getString(42));
		response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
		response.setCurrencyCode(msg.getString(49));
		// bit 57
		response.getAmountInformation().unpack(msg.getString(57));
		// end bit 57
		response.setPay(msg.getString(59));
		response.setFeatureCode(msg.getString(60));
		response.setBillInformation(new BillInformation());
		response.getBillInformation().setBills(AdditionalDataPayment.unpacksixtyone(msg, mesType));
		response.setAdditionalData(msg.getString(61));
		response.setSourceAccountNumber(msg.getString(102));
//		log.info("response bit102 = "+ msg.getString(102));
		response.setDestinationAccountNumber(msg.getString(103));
//		log.info("response bit103 = "+ msg.getString(103));
		return response;
	}

	/**
	 * 
	 * Goal : generate response as output (payment) Create Date : Sep 3, 2016
	 * 
	 * @param response
	 * @param msg
	 * @return
	 * @throws IllegalAccessException
	 */
	public PaymentResponse generateResponseWS(PaymentResponse response, ISOMsg msg, String messageType)
			throws IllegalAccessException {

		response.setPrimaryAccountNumber(msg.getString(2));
		response.setProcessingCode(msg.getString(3));
		response.setAmountTransaction(msg.getString(4));
		response.setTransmissionDateTime(msg.getString(7));
		response.setSystemTraceAuditNumber(msg.getString(11));
		response.setTimeLocalTransaction(msg.getString(12));
		response.setLocalTransactionDate(msg.getString(13));
		response.setSettlementDate(msg.getString(15));
		response.setMerchantType(msg.getString(18));
		response.setPosEntryModeCode(msg.getString(22));
		response.setAggregatorCode(CommonUtils.addPrefix(msg.getString(32), Constants.PREFIX_AGGREGATOR));
		response.setTrack2Data(msg.getString(35));
		response.setSequenceNumber(msg.getString(37));
		response.setResponseCode(msg.getString(39));
		response.getTerminalAggregator().setTerminalIdentifierNumber(msg.getString(41));
		response.getTerminalAggregator().setTerminalName(msg.getString(42));
		response.getTerminalAggregator().setCardAcceptorName(msg.getString(43));
		response.setCurrencyCode(msg.getString(49));
		
		//** Add field 47 & 48 **//
		response.setNumberTransactionP(msg.getString(47));
		response.setNumberTransactionBank(msg.getString(48));
		
		// bit 57
		response.getAmountInformation().unpack(msg.getString(57));
		// end bit 57
		response.setPay(msg.getString(59));
		response.setFeatureCode(msg.getString(60));
		response.setBillInformation(new BillInformation());
		response.getBillInformation().setBills(AdditionalDataPayment.unpacksixtyone(msg, messageType));
		response.setAdditionalData(msg.getString(61));
		response.setSourceAccountNumber(msg.getString(102));
//		log.info("response bit102 = "+ msg.getString(102));
		response.setDestinationAccountNumber(msg.getString(103));
//		log.info("response bit103 = "+ msg.getString(103));

		return response;
	}

	/**
	 * 
	 * Goal : log iso request, iso response, and iso field to console Create
	 * Date : Sep 3, 2016
	 * 
	 * @param request
	 * @param response
	 * @throws ISOException
	 */
	public void traceISOMessage(ISOMsg request, ISOMsg response) throws ISOException {

		if (response != null) {
			System.out.println("Request : [" + new String(request.pack()) + "]");
			System.out.println("Response : [" + new String(response.pack()) + "]");
		} else {
			System.err.println("null response");
		}

		System.out.println("MTI='" + response.getMTI() + "'");
		for (int i = 1; i <= response.getMaxField(); i++) {
			if (response.hasField(i))
				System.out.println(i + "='" + response.getString(i) + "'");
		}

	}

}
