package com.bjb.iso.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.model.FeatureMapping;
import com.bjb.model.MessageLog;
import com.bjb.model.TransactionLog;
import com.bjb.repository.FeatureMappingRepository;
import com.bjb.repository.MessageLogRepository;
import com.bjb.repository.TransactionLogRepository;
import com.bjb.util.CommonUtils;
import com.bjb.util.ObjectUtils;

/**
 * This class for scheduling logging action.
 * 
 * @author arifino
 *
 */
@Component
public class SamLogExecutor {

	private static Logger log = LoggerFactory.getLogger(SamLogExecutor.class);

	private MessageLogRepository messageLogRepository;
	
	private static FeatureMappingRepository featureMappingRepository;
	
	@Autowired
	public void setFeatureMappingRepository(FeatureMappingRepository ory) {
		featureMappingRepository = ory;
	}

	@Autowired
	public void setMessageLogRepository(MessageLogRepository repo) {
		messageLogRepository = repo;
	}

	@Autowired
	private TransactionLogRepository transactionLogRepository;
	
	@SuppressWarnings("unused")
	@Transactional
	public void log(Object loggedObject, String type, String event, int code) {
		try {
			TransactionLog tl = log(loggedObject);
//			System.out.println("TL Object : "+tl.toString());
			transactionLogRepository.save(tl);
			Integer result = 1;
			// return new AsyncResult<>(result);
		} catch (Exception e) {
			// return new AsyncResult<>(0);
		}
	}
	@SuppressWarnings("unused")
	@Transactional
	public void log(Object response,Object request, String type) {
		try {
			TransactionLog tl = log(response,request);
			transactionLogRepository.save(tl);
			Integer result = 1;
			// return new AsyncResult<>(result);
		} catch (Exception e) {
			// return new AsyncResult<>(0);
		}
	}

	@SuppressWarnings("unused")
	@Transactional
	public void log(Object loggedObject, Object loggedObject2, String type, String event, int code) {
		try {
			MessageLog l = log(loggedObject, loggedObject2, type, event);
//			System.out.println("=========== MessageLog : "+l.toString());
			messageLogRepository.save(l);
			Integer result = 1;
			// return new AsyncResult<>(result);
		} catch (Exception e) {
			// return new AsyncResult<>(0);
		}
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	public MessageLog log(Object o, Object o2, String type, String event) {
		Map map = null;
		Map map2 = null;
		if (type.equalsIgnoreCase("ISO")) {
			map = ObjectUtils.unpackFromIso((ISOMsg) o);
			map2 = ObjectUtils.unpackFromIso((ISOMsg) o2);
		} else if (type.equalsIgnoreCase("XML") || type.equalsIgnoreCase("JSON")) {
			try {
				map = ObjectUtils.unpackFromObject(o);
				map2 = ObjectUtils.unpackFromObject(o2);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
//		System.out.println(ObjectUtils.printMap(map));
//		System.out.println(ObjectUtils.printMap(map2));
		if (map != null) {
			MessageLog m = new MessageLog();
			// m.setId(new BigDecimal(id));
			if (type.equals("ISO")) {
				if (map.get("32") != null) {
					String aggCode = (String) map.get("32");
					m.setAggregatorCode(new BigDecimal(CommonUtils.removeUntilNumber(aggCode)));
				}
				if (map.get("59") != null) {
					m.setBillingId((String) map.get("59"));
				}
				// m.setCaCode(caCode);
				m.setCreateDate(new java.sql.Date(new Date().getTime()));
				if (map.get("60") != null) {
					String featureCode = (String) map.get("60");
					m.setFeatureCode(new BigDecimal(CommonUtils.removeUntilNumber(featureCode)));
				}
				// m.setId(id);
				 m.setMti((String) map.get(0));

				try {
					ISOMsg msg = (ISOMsg) o2;
					String content = new String(msg.pack(), Constants.DEFAULT_CHARSET);// ObjectUtils.printMap(map2);
					msg = (ISOMsg) o;
					String content2 = new String(msg.pack(), Constants.DEFAULT_CHARSET);// ObjectUtils.printMap(map);
					m.setRawResponse(content2);
					m.setRawRequest(content);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// m.setRawRequest("ISO\nREQUEST\n" + event + "\n" + content);
				m.setResponseCode((String) map.get("39"));
				// m.setRawResponse("ISO\nRESPONSE\n" + event + "\n" +
				// content2);
				if (map.get("11") != null) {
					m.setStan((String) map.get("11"));
				}
			} else if (type.equals("XML") || type.equals("JSON")) {
				if (map.get("aggregatorCode".toLowerCase()) != null) {
					String aggCode = (String) map.get("aggregatorCode".toLowerCase());
					m.setAggregatorCode(new BigDecimal(CommonUtils.removeUntilNumber(aggCode)));
					//System.out.println(m.getAggregatorCode());
				}
				if (map.get("additionalData".toLowerCase()) != null) {
					m.setBillingId((String) map.get("additionalData".toLowerCase()));
				}
				if (map.get("cacode".toLowerCase()) != null) {
					String cacode = (String) map.get("cacode".toLowerCase());
					m.setCaCode(new BigDecimal(CommonUtils.removeUntilNumber(cacode)));
				}
				m.setCreateDate(new java.sql.Date(new Date().getTime()));
				if (map.get("featureCode".toLowerCase()) != null) {

					String featureCode = (String) map.get("featureCode".toLowerCase());
					m.setFeatureCode(new BigDecimal(CommonUtils.removeUntilNumber(featureCode)));
				}
				if (map.get("systemTraceAuditNumber".toLowerCase()) != null) {
					m.setStan((String) map.get("systemTraceAuditNumber".toLowerCase()));
				}
				if (type.equalsIgnoreCase("XML")) {
					m.setRawResponse(ObjectUtils.printToXml(o));
					m.setRawRequest(ObjectUtils.printToXml(o2));
				} else if (type.equalsIgnoreCase("JSON")) {
					m.setRawResponse(ObjectUtils.printToJson(o));
					m.setRawRequest(ObjectUtils.printToJson(o2));
				}
				m.setResponseCode((String) map.get("responsecode".toLowerCase()));
			}
			return m;
		} else {
			log.warn(Messages.LOG_ERROR);
			return null;
		}

	}

	private TransactionLog log(Object isoMsg) {
//		System.out.println("LOGGING....2");
		ISOMsg msg = (ISOMsg) isoMsg;
		TransactionLog tlog = null;
		try {
			// saldo
			if (msg.getString(3).equalsIgnoreCase("301000")) {
				if (msg.getMTI().equals("0200")) {
					// InquirySaldoRequest isr = (InquirySaldoRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					tlog.setName("Inquiry Saldo Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// InquirySaldoResponse isr = (InquirySaldoResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					tlog.setName("Inquiry Saldo Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// inq_payment
			} else if (msg.getString(3).equalsIgnoreCase("341018")
					/*add OR procode SIPANDU "300020"*/
					|| msg.getString(3).equalsIgnoreCase("300020")
					/*add OR procode PDAM "321066"*/
					|| msg.getString(3).equalsIgnoreCase("321066")
					/*add OR procode EDUPAY "321000"*/
					|| msg.getString(3).equalsIgnoreCase("321000")
					/*add OR procode ESAMSAT "301099"*/
					|| msg.getString(3).equalsIgnoreCase("301099")
					) {
				if (msg.getMTI().equals("0200")) {
					// InquiryPaymentRequest ipr = (InquiryPaymentRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Inquiry Payment Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// InquiryPaymentResponse ipr = (InquiryPaymentResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
						
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Inquiry Payment Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// purchase
			} else if (msg.getString(3).equalsIgnoreCase("501089")) {
				if (msg.getMTI().equals("0200")) {
					// PurchaseRequest ipr = (PurchaseRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Purchase Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// PurchaseResponse ipr = (PurchaseResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Purchase Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// payment
			} else if (msg.getString(3).equalsIgnoreCase("541018")
					/*add OR procode SIPANDU "500020"*/
					|| msg.getString(3).equalsIgnoreCase("500020")
					/*add OR procode PDAM "500020"*/
					|| msg.getString(3).equalsIgnoreCase("521066")
					/*add OR procode EDUPAY "521000"*/
					|| msg.getString(3).equalsIgnoreCase("521000")
					/*add OR procode ESAMSAT "541099"*/
					|| msg.getString(3).equalsIgnoreCase("541099")) {
				if (msg.getMTI().equals("0200")) {
					// PaymentRequest ipr = (PaymentRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));			
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Payment Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// PaymentResponse ipr = (PaymentResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Payment Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return tlog;
		}
		try {
			return tlog;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tlog;
	}
	private void setFeatureMapping(ISOMsg msg, TransactionLog tlog) {
		try{
			System.out.println("Trying get feature mapping with feature ID : "+msg.getString(60)+" and agg code : "+msg.getString(32));
			int fmid = getFeatureMappingID(
					Integer.parseInt(CommonUtils.removeUntilNumber(msg.getString(60).trim())),
					Integer.parseInt(CommonUtils.removeUntilNumber(msg.getString(32).trim())));
			if(fmid!=0){
				tlog.setFeatureMapping(fmid);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private TransactionLog log(Object isoMsg,Object isoMsg2) {
//		System.out.println("LOGGING....");
		ISOMsg msg = (ISOMsg) isoMsg;
		ISOMsg msg2 = (ISOMsg) isoMsg2;
		TransactionLog tlog = null;
		try {
			// saldo
			if (msg.getString(3).equalsIgnoreCase("301000")) {
				if (msg.getMTI().equals("0200")) {
					// InquirySaldoRequest isr = (InquirySaldoRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					tlog.setName("Inquiry Saldo Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// InquirySaldoResponse isr = (InquirySaldoResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					tlog.setName("Inquiry Saldo Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// inq_payment
			} else if (msg.getString(3).equalsIgnoreCase("341018")
					/*add OR procode SIPANDU "300020"*/
					|| msg.getString(3).equalsIgnoreCase("300020")
					/*add OR procode PDAM "321066"*/
					|| msg.getString(3).equalsIgnoreCase("321066")
					/*add OR procode EDUPAY "321000"*/
					|| msg.getString(3).equalsIgnoreCase("321000")
					/*add OR procode ESAMSAT "301099"*/
					|| msg.getString(3).equalsIgnoreCase("301099")
					) {
				if (msg.getMTI().equals("0200")) {
					// InquiryPaymentRequest ipr = (InquiryPaymentRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Inquiry Payment Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// InquiryPaymentResponse ipr = (InquiryPaymentResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Inquiry Payment Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// purchase
			} else if (msg.getString(3).equalsIgnoreCase("501089")) {
				if (msg.getMTI().equals("0200")) {
					// PurchaseRequest ipr = (PurchaseRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Purchase Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// PurchaseResponse ipr = (PurchaseResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Purchase Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
				// payment
			} else if (msg.getString(3).equalsIgnoreCase("541018")
					/*add OR procode SIPANDU "500020"*/
					|| msg.getString(3).equalsIgnoreCase("500020")
					/*add OR procode PDAM "500020"*/
					|| msg.getString(3).equalsIgnoreCase("521066")
					/*add OR procode EDUPAY "521000"*/
					|| msg.getString(3).equalsIgnoreCase("521000")
					/*add OR procode ESAMSAT "541099"*/
					|| msg.getString(3).equalsIgnoreCase("541099")) {
				if (msg.getMTI().equals("0200")) {
					// PaymentRequest ipr = (PaymentRequest) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Payment Request");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				} else if (msg.getMTI().equals("0210")) {
					// PaymentResponse ipr = (PaymentResponse) o;
					tlog = new TransactionLog();
					// tlog.setId(new BigDecimal(id));
					tlog.setAmount(Integer.valueOf(msg.getString(4)));
					tlog.setAggregatorCode(msg.getString(32));
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					// bit 57
					if (msg.getString(57) != null) {
						tlog.setBillingId(msg.getString(61));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(0, 12)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(12, 24)));
						tlog.setCommisionAgent(Long.parseLong(msg.getString(57).substring(24, 36)));
						tlog.setCommisionBjb(Long.parseLong(msg.getString(57).substring(36, 48)));
					}
					// end bit 57
					tlog.setCreateDate(new Timestamp(new Date().getTime()));
					tlog.setFeatureCode(msg.getString(60));
					setFeatureMapping(msg, tlog);
					tlog.setName("Payment Response");
					tlog.setResponseCode(msg.getString(39));
					tlog.setTerminalId(msg.getString(41));
					tlog.setRawIsoReq(new String(msg2.pack(),Constants.DEFAULT_CHARSET));
					tlog.setRawIsoRes(new String(msg.pack(),Constants.DEFAULT_CHARSET));
					tlog.setStan(msg.getString(11));
					tlog.setTransmissionDate(msg.getString(13));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return tlog;
		}
		try {
			return tlog;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tlog;
	}
	
	private int getFeatureMappingID(int featureId, int aggId){
		FeatureMapping fm = featureMappingRepository.findByFeatureIdAndAggregatorId(featureId, aggId);
		if (fm != null) {
			return fm.getId();
		}else{
			System.out.println("FM Null");
		}
		return 0;
	}

}
