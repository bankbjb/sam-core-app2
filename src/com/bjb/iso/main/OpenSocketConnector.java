package com.bjb.iso.main;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.constants.Messages;
import com.bjb.iso.util.ISOHelper;
import com.bjb.iso.util.SamPackager;
import com.bjb.util.PubUtil;

/**
 * This class create connection between SAM and IF400
 * 
 * @author arifino
 *
 */
public class OpenSocketConnector extends Thread {

	private static Logger log = LoggerFactory.getLogger(OpenSocketConnector.class);

	private static OpenSocketConnector connector = null;
	private static BaseConfig baseConfig = BaseConfig.getBaseConfig();
	private static Socket socket;

	static String host;
	static int port;
	static int echoInterval;
	static int connectInterval;
	static OutputStream out;
	static InputStream in;

	public static OpenSocketConnector getConnector() {
		if (connector == null) {
			connector = new OpenSocketConnector();
		}
		return connector;
	}

	@Override
	public void run() {
		log.info(Messages.SOCKET_START);
		try {
			host = getBaseConfig().getIPServer();
			port = getBaseConfig().getPortServer();
			echoInterval = getBaseConfig().getEchoInterval();
			connectInterval = getBaseConfig().getConnectInterval();
			connect();
		} catch (StackOverflowError s) {
			log.warn(Messages.SOCKET_START_ERROR);
		}
	}

	public void connect() {
		try {
			log.info(Messages.SOCKET_TRY_CONNECT(host, port));

			socket = new Socket(host, port);
			socket.setKeepAlive(true);
			if (getSocket().isConnected()) {
				log.info("[CONNECTED] : " + getSocket().isConnected());

				in = socket.getInputStream();
				out = socket.getOutputStream();

				log.info("InputStream : " + in);
				log.info("OutputStream : " + out);

				ISOHelper helper = new ISOHelper();
				ISOMsg echoMsg = new ISOMsg();
				echoMsg.setPackager(new SamPackager());

				// response handler
				// new Thread(new ResponseHandler(),
				// "response_handler").start();
				Thread rec = new Thread(new Receiver(), "response receiver");
				rec.setDaemon(true);
				rec.start();
				Thread resp = new Thread(new ResponseProcessor(), "response processor");
				resp.setDaemon(true);
				resp.start();

				// maintain connection with server socket
				while (socket.isConnected()) {
					echoMsg = helper.generateEchoMessage(echoMsg);
					// echoMsg.dump(System.out, "");

					byte[] isoToBeSend = generateISOMessageByte(echoMsg);
					out.write(isoToBeSend);
					log.info(Messages.SOCKET_ECHO_SENT);

					Thread.sleep(echoInterval * 1000);
				}

				log.warn(Messages.SOCKET_BROKEN);

				// retry new connection
				this.connect();
			}
			 log.info("just finished connected to "
			 + getSocket().getRemoteSocketAddress());

		} catch (Exception e) {
			log.warn(Messages.SOCKET_RETRY);
			log.warn(e.getMessage());
			try {
				// System.out.println(connectInterval);
				Thread.sleep(connectInterval * 1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			this.connect();
		} finally {
			if(!socket.isConnected()){
				log.warn(Messages.SOCKET_RETRY);
				try {
					Thread.sleep(connectInterval * 1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				this.connect();
			}
		}
	}

	public void send(byte[] message) {
		try {
			out.write(message);
			// System.out.println(new String(message));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte[] generateISOMessageByte(ISOMsg message) throws ISOException {
		// make the iso string
		String iso = new String(message.pack());

		// get the length of iso string in byte[]
		byte[] isoLengthByteArray = PubUtil.intTo2ByteArray(iso.length());

		// create iso message to send
		byte[] isoToBeSend = PubUtil.concateByteArray(isoLengthByteArray, iso.getBytes());

		return isoToBeSend;
	}

	public static Socket getSocket() {
		return socket;
	}

	public static void setSocket(Socket client) {
		OpenSocketConnector.socket = client;
	}

	public static BaseConfig getBaseConfig() {
		return baseConfig;
	}

	public static void setBaseConfig(BaseConfig baseConfig) {
		OpenSocketConnector.baseConfig = baseConfig;
	}

}
