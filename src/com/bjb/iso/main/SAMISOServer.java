package com.bjb.iso.main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.ASCIIChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.bjb.cache.Cache;
import com.bjb.cache.DBListener;
import com.bjb.cache.ForAdditionalInfo;
import com.bjb.cache.ForAggregators;
import com.bjb.cache.ForCaCode;
import com.bjb.cache.ForStan;
import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;
import com.bjb.dto.Pair;
import com.bjb.iso.util.SamLogExecutor;
import com.bjb.iso.util.SamPackager;
import com.bjb.resource.additional.AdditionalDataPayment;
import com.bjb.resource.additional.AmountInformation;
import com.bjb.service.CollectingAgentService;
import com.bjb.service.EmailSupport;
import com.bjb.util.CommonUtils;
import com.bjb.util.PubUtil;
import com.bjb.webservice.util.WSUtil;

/**
 * Implementation of JPOS Server as intermediate in SAM to receiving request,
 * forwarding message to IF400 receiving response and forwarding back to client.
 * 
 * @author arifino
 *
 */
@SuppressWarnings("serial")
@Component
public class SAMISOServer extends HttpServlet implements ISORequestListener {

	private static Logger log = LoggerFactory.getLogger(SAMISOServer.class);

	static SamLogExecutor samLogExecutor;

	static EmailSupport emailSupport;

	static ApplicationContext applicationContext;

	static SamPackager samPackager;
	
	//static CollectingAgentService collectingService;

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(ApplicationContext applicationContext) {
		SAMISOServer.applicationContext = applicationContext;
	}

	public static void setSamLogExecutor(SamLogExecutor samLogExecutor) {
		SAMISOServer.samLogExecutor = samLogExecutor;
	}

	public static void setSamPackager(SamPackager samPackager) {
		SAMISOServer.samPackager = samPackager;
	}

	@Override
	public void init() throws ServletException {
		applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
		Cache.init();
		super.init();
		samLogExecutor = (SamLogExecutor) applicationContext.getBean("samLogExecutor");
		emailSupport = (EmailSupport) applicationContext.getBean("emailSupport");
		samPackager = (SamPackager) applicationContext.getBean("samPackager");
		//collectingService = (CollectingAgentService) applicationContext.getBean("collectingAgentService");
		// opening listener
		String hostname = BaseConfig.getBaseConfig().getIPListener();
		int portNumber = BaseConfig.getBaseConfig().getPortListener();

		// ISOPackager samPackager = new SAMPackager();

		ServerChannel sahChannel = new ASCIIChannel(hostname, portNumber, samPackager);

		ISOServer samServer = new ISOServer(portNumber, sahChannel, null);
		samServer.addISORequestListener(new SAMISOServer());

		DBListener dbl = new DBListener();
		Thread dblistener = new Thread(dbl);
		dblistener.setDaemon(true);
		dblistener.start();
		new Thread(samServer).start();
		// end opening listener
		log.info("Server ready to receive connection on port [" + portNumber + "]");
		// System.out.println();
		// System.out.println("==================================");

		log.info("Try to connect to switching ... ");
		// try to connect to switching
		// System.out.println("==================================");
		// System.out.println();

		OpenSocketConnector connector = OpenSocketConnector.getConnector();
		connector.start();
		log.info("Connecting to switching running on background");
		// log.warn("SAMLOGEXE : " + samLogExecutor.hashCode());
		// System.out.println();
		// System.out.println("==================================");

	}

	// public ISOMsg findResponse(ISOMsg req) throws ISOException{
	// long end =
	// BaseConfig.getBaseConfig().getTimeoutISORequest()+System.currentTimeMillis();
	// ISOMsg res = null;
	// while(System.currentTimeMillis()<=end){
	// res = ISOResList.get(req.getString(7).concat(req.getString(11)));
	// if(res!=null){
	// break;
	// }
	// }
	// if(res==null){
	// res = new ISOMsg();
	// res.set(39,"92");
	// }
	// return res;
	// }

	@Override
	public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
		try {
			log.info("Server receives connection from ["
					+ ((BaseChannel) isoSrc).getSocket().getInetAddress().getHostAddress() + "]");

			if (isoMsg.getMTI().equalsIgnoreCase("0800")) {
				log.info("SAM ISO Server Accepting Network Management Request");
				acceptNetworkMsg(isoSrc, isoMsg);
			} else if (isoMsg.getMTI().equalsIgnoreCase("0200")) {
				processRequest(isoSrc, isoMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static ISOMsg createDummyNetworkRequets(ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, ForStan.getStanNow());
		msg.set(15, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(70, "301");

		return msg;
	}

	public static ISOMsg createDummySignOnNetworkRequets(ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, ForStan.getStanNow());
		msg.set(15, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(70, "001");

		return msg;
	}

	public byte[] generateISOMessageByte(ISOMsg message) throws ISOException {
		// make the iso string
		String iso = new String(message.pack());

		// get the length of iso string in byte[]
		byte[] isoLengthByteArray = PubUtil.intTo2ByteArray(iso.length());

		// create iso message to send
		byte[] isoToBeSend = PubUtil.concateByteArray(isoLengthByteArray, iso.getBytes());

		return isoToBeSend;
	}

	private void processRequest(ISOSource isoSrc, ISOMsg isoMsg) throws ISOException, IOException {
		if (isoMsg.getString(3).equalsIgnoreCase("301000")) {
			log.info("SAM ISO Server Accepting Inquiry Saldo Request");
			// send iso message to simulated iso server
			ISOMsg reply = null;
			Pair<Boolean, String> validity = ForAggregators.getValidWithReason(isoMsg.getString(32), "ISO");
			if (validity.a) {
				try {
					isoMsg.dump(System.err, "");
					OpenSocketConnector.getConnector().send(generateISOMessageByte(isoMsg));
				} catch (ISOException e) {
					e.printStackTrace();
				}
				reply = WSUtil.findResponse(isoMsg);
			} else {
				reply = new ISOMsg();
				reply.set(39, ForAggregators.getErrorCode(validity.b));
			}
			if (reply != null) {
				// samLogExecutor.log(reply, "ISO",
				// Constants.LABEL_INQUIRY_SALDO, 1);
				samLogExecutor.log(reply, isoMsg, "ISO");
				samLogExecutor.log(reply, isoMsg, "ISO", Constants.LABEL_INQUIRY_SALDO, 2);
			}
			// send response to iso client
			isoSrc.send(reply);
		} else if (isoMsg.getString(3).equalsIgnoreCase("501089")) {
			log.info("SAM ISO Server Accepting Purchase Request");
			// send iso message to simulated iso server
			ISOMsg reply = null;
			String err = "";
			Pair<Boolean, String> validity = ForAggregators.getValidWithReason(isoMsg.getString(32), "ISO");
			if(validity.a){
				validity = ForAdditionalInfo.getValidCA(
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(60))), 
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(32))));
			}
			if (validity.a) {
				try {
					String bit61 = AdditionalDataPayment.packsixtyone(isoMsg);
					isoMsg.set(61, bit61);
					isoMsg.dump(System.err, "");
					OpenSocketConnector.getConnector().send(generateISOMessageByte(isoMsg));
				} catch (ISOException e) {
					e.printStackTrace();
					err = e.getMessage();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					err = e.getMessage();
				}
				if (err.equals("")) {
					reply = WSUtil.findResponse(isoMsg);
					try {
						reply = AdditionalDataPayment.unpacksixtyone(reply);
					} catch (Exception e) {
						err = e.getMessage();
					}
				} else {
					if (err.toLowerCase().contains("mapping not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					} else if (err.toLowerCase().contains("mapping variable")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					} else if (err.toLowerCase().contains("variable a")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					} else if (err.toLowerCase().contains("feature not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					} else if (err.toLowerCase().contains("mapping data")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					} else if (err.toLowerCase().contains("not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					} else {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					}
				}
			} else {
				reply = new ISOMsg();
				reply.set(39, ForAggregators.getErrorCode(validity.b));
			}

			if (reply != null) {
				emailSupport.sendEmail(reply.getString(32), "ISO");
				samLogExecutor.log(reply, isoMsg, "ISO", Constants.LABEL_PURCHASE, 2);
				// samLogExecutor.log(reply, "ISO", Constants.LABEL_PURCHASE,
				// 1);
				samLogExecutor.log(reply, isoMsg, "ISO");
			}
			// send response to iso client
			isoSrc.send(reply);
		} else if (isoMsg.getString(3).equalsIgnoreCase("341018")
				/*add OR procode SIPANDU "300020"*/
				|| isoMsg.getString(3).equalsIgnoreCase("300020")
				/*add OR procode PDAM "321066"*/
				|| isoMsg.getString(3).equalsIgnoreCase("321066")
				/*add OR procode EDUPAY "321000"*/
				|| isoMsg.getString(3).equalsIgnoreCase("321000")
				/*add OR procode ESAMSAT "301099"*/
				|| isoMsg.getString(3).equalsIgnoreCase("301099")) {
			log.info("SAM ISO Server Accepting Inquiry Payment Request");
			// send iso message to simulated iso server
			ISOMsg reply = null;
			String err = "";
			Pair<Boolean, String> validity = ForAggregators.getValidWithReason(isoMsg.getString(32), "ISO");
			/*Ayuda add validasi for ca - 26-03-2018*/
			Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(isoMsg.getString(48), "ISO");
			if(validity.a && validasiCA.a){
				validity = ForAdditionalInfo.getValidCA(
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(60))), 
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(32))));
			}
			if (validity.a && validasiCA.a) {
				try {
					String bit61 = AdditionalDataPayment.packsixtyone(isoMsg);
					isoMsg.set(61, bit61);
					isoMsg.dump(System.err, "");
					OpenSocketConnector.getConnector().send(generateISOMessageByte(isoMsg));
				} catch (ISOException e) {
					e.printStackTrace();
					err = e.getMessage();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					err = e.getMessage();
				}
				if (err.equals("")) {
					reply = WSUtil.findResponse(isoMsg);
					try {
						reply = AdditionalDataPayment.unpacksixtyone(reply);
						
						
					} catch (Exception e) {
						err = e.getMessage();
					}
				} else {
					if (err.toLowerCase().contains("mapping not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					} else if (err.toLowerCase().contains("mapping variable")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					} else if (err.toLowerCase().contains("variable a")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					} else if (err.toLowerCase().contains("feature not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					} else if (err.toLowerCase().contains("mapping data")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					} else if (err.toLowerCase().contains("not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					} else {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					}
				}
			} else {
				reply = new ISOMsg();
				reply.setMTI("0210");
				reply.set(39, ForAggregators.getErrorCode(validity.b));
				reply.set(61, isoMsg.getString(61));
			}

			if (reply != null) {
				samLogExecutor.log(reply, isoMsg, "ISO",Constants.LABEL_INQUIRY_PAYMENT, 2);
				samLogExecutor.log(reply, isoMsg, "ISO");
				samLogExecutor.log(reply, "ISO", Constants.LABEL_INQUIRY_PAYMENT, 1);
			}
			// send response to iso client
			isoSrc.send(reply);
		} else if (isoMsg.getString(3).equalsIgnoreCase("541018")
				/*add OR procode SIPANDU "500020"*/
				|| isoMsg.getString(3).equalsIgnoreCase("500020")
				/*add OR procode PDAM "500020"*/
				|| isoMsg.getString(3).equalsIgnoreCase("521066")
				/*add OR procode EDUPAY "521000"*/
				|| isoMsg.getString(3).equalsIgnoreCase("521000")
				/*add OR procode ESAMSAT "541099"*/
				|| isoMsg.getString(3).equalsIgnoreCase("541099")){
			log.info("SAM ISO Server Accepting Payment Request");
			// send iso message to simulated iso server
			ISOMsg reply = null;
			String err = "";
			Pair<Boolean, String> validity = ForAggregators.getValidWithReason(isoMsg.getString(32), "ISO");
			/*Ayuda add validasi for ca - 26-03-2018*/
			Pair<Boolean, String> validasiCA = ForCaCode.getValidCaCode(isoMsg.getString(48), "ISO");
			if(validity.a && validasiCA.a){
				validity = ForAdditionalInfo.getValidCA(
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(60))), 
						Integer.valueOf(CommonUtils.removeUntilNumber(isoMsg.getString(32))));
			}
			if (validity.a && validasiCA.a) {
				try {
					String bit61 = AdditionalDataPayment.packsixtyone(isoMsg);
					isoMsg.set(61, bit61);
					isoMsg.dump(System.err, "");
					OpenSocketConnector.getConnector().send(generateISOMessageByte(isoMsg));
				} catch (ISOException e) {
					e.printStackTrace();
					err = e.getMessage();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					err = e.getMessage();
				}
				if (err.equals("")) {
					reply = WSUtil.findResponse(isoMsg);
					try {
						reply = AdditionalDataPayment.unpacksixtyone(reply);
					} catch (Exception e) {
						err = e.getMessage();
					}
				} else {
					if (err.toLowerCase().contains("mapping not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_002));
					} else if (err.toLowerCase().contains("mapping variable")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_003));
					} else if (err.toLowerCase().contains("variable a")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_004));
					} else if (err.toLowerCase().contains("feature not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_006));
					} else if (err.toLowerCase().contains("mapping data")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_005));
					} else if (err.toLowerCase().contains("not found")) {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_008));
					} else {
						reply = new ISOMsg();
						reply.set(39, ForSystemParameter.getSysValue(Constants.RC_SAM_009));
					}
				}
			} else {
				reply = new ISOMsg();
				reply.setMTI("0210");
				reply.set(39, ForAggregators.getErrorCode(validity.b));
				reply.set(61, isoMsg.getString(61));
			}

			if (reply != null) {
//				emailSupport.sendEmail(reply.getString(32), "ISO");
				samLogExecutor.log(reply, isoMsg, "ISO",Constants.LABEL_PAYMENT, 2);
				samLogExecutor.log(reply, isoMsg, "ISO");
				samLogExecutor.log(reply, "ISO", Constants.LABEL_PAYMENT, 1);
			}
			// send response to iso client
			isoSrc.send(reply);
		}

	}

	private void acceptNetworkMsg(ISOSource isoSrc, ISOMsg isoMsg) throws ISOException, IOException {
		if (isoMsg.getMTI().equalsIgnoreCase("0800")) {
			log.info("Accepting Network Management Request");
			ISOMsg reply = (ISOMsg) isoMsg.clone();
			reply.setMTI("0810");
			reply.set(39, "00");

			isoSrc.send(reply);
		}

	}

	// public void dumpSignOn(ISOMUX mux) {
	// while (mux.isConnected()) {
	// // send iso message to simulated iso server
	// ISOMsg isoMsg = new ISOMsg();
	// try {
	// isoMsg = createDummySignOnNetworkRequets(isoMsg);
	// } catch (ISOException e) {
	// e.printStackTrace();
	// }
	// ISORequest req = new ISORequest(isoMsg);
	// mux.queue(req);
	//
	// ISOMsg reply =
	// req.getResponse(BaseConfig.getBaseConfig().getTimeoutISORequest());
	// if (reply == null) {
	// break;
	// }
	// }
	// }

}
