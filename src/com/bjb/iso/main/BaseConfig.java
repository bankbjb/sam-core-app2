package com.bjb.iso.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bjb.cache.ForSystemParameter;
import com.bjb.constants.Constants;

/**
 * Class to contain several properties for the system.
 * 
 * @author arifino
 *
 */
@Component
public class BaseConfig {

	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(BaseConfig.class);

	static BaseConfig baseConfig = null;
	// IP server is destination address to connect with
	// this case is Switching BJB
	private String IPServer = "10.6.226.160";
	private int portServer = 10511;

	// IP listener is our address to open connection with client
	private String IPListener = "localhost";
	private int portListener = 12345;

	// setting timeout for ISO messaging
	private int timeoutISORequest = 10; // 10s
	private int echoInterval = 60; // 30s
	private int connectInterval = 3; // 3s
	
	//Setting Procode
	private String procodeInquiryPayment = "341018||300020||321066";
	private String procodePayment = "541018||500020||521066";

	public BaseConfig() {
		if (ForSystemParameter.isEmpty()) {
			ForSystemParameter.init();
		}
		if (ForSystemParameter.getSysValue(Constants.IF_400_IP) != null) {

			setIPServer(ForSystemParameter.getSysValue(Constants.IF_400_IP));
		} else {
			setIPServer(IPServer);
		}
		if (ForSystemParameter.getSysValue(Constants.IF_400_PORT) != null) {
			setPortServer(Integer.valueOf(ForSystemParameter.getSysValue(Constants.IF_400_PORT)));
		} else {
			setPortServer(portServer);
		}
		if (ForSystemParameter.getSysValue(Constants.WEB_SERVICE_IP) != null) {
			setIPListener(ForSystemParameter.getSysValue(Constants.WEB_SERVICE_IP));
		} else {
			setIPListener(IPListener);
		}
		if (ForSystemParameter.getSysValue(Constants.CORE_SAM_PORT) != null) {
			setPortListener(Integer.valueOf(ForSystemParameter.getSysValue(Constants.CORE_SAM_PORT)));
		} else {
			setPortListener(portListener);
		}
		if (ForSystemParameter.getSysValue(Constants.IF_400_TIMEOUT) != null) {
			setTimeoutISORequest(Integer.valueOf(ForSystemParameter.getSysValue(Constants.IF_400_TIMEOUT)));
		} else {
			setTimeoutISORequest(timeoutISORequest);
		}
		if (ForSystemParameter.getSysValue(Constants.IF_400_ECHO) != null) {
			setEchoInterval(Integer.valueOf(ForSystemParameter.getSysValue(Constants.IF_400_ECHO)));
		} else {
			setEchoInterval(echoInterval);
		}
		if (ForSystemParameter.getSysValue(Constants.IF_400_CONNECT) != null) {
			setConnectInterval(Integer.valueOf(ForSystemParameter.getSysValue(Constants.IF_400_CONNECT)));
		} else {
			setConnectInterval(connectInterval);
		}
		if (ForSystemParameter.getSysValue(Constants.PRO_CODE_INQ_PAYMENTS) != null) {
			setProcodeInquiryPayment(ForSystemParameter.getSysValue(Constants.PRO_CODE_INQ_PAYMENTS));
			System.out.println("==========="+ForSystemParameter.getSysValue(Constants.PRO_CODE_INQ_PAYMENTS));
		} else {
			setProcodeInquiryPayment(procodeInquiryPayment);
			System.out.println("==========="+procodeInquiryPayment);
		}
		if (ForSystemParameter.getSysValue(Constants.PRO_CODE_PAYMENTS) != null) {
			setProcodePayment(ForSystemParameter.getSysValue(Constants.PRO_CODE_PAYMENTS));
			System.out.println("==========="+ForSystemParameter.getSysValue(Constants.PRO_CODE_PAYMENTS));
		} else {
			setProcodePayment(procodePayment);
			System.out.println("==========="+procodePayment);
		}

		// setIPServer("10.6.226.160");
		// setPortServer(10511);
		// setIPListener("localhost");
		// setPortListener(12345);
		// setTimeoutISORequest(10*1000);
		// setEchoInterval(60*1000);
		// setConnectInterval(3*1000);
	}

	public String getProcodePayment() {
		return procodePayment;
	}

	public void setProcodePayment(String procodePayment) {
		this.procodePayment = procodePayment;
	}

	public static BaseConfig getBaseConfig() {
		if (baseConfig == null) {
			baseConfig = new BaseConfig();
		}
		return baseConfig;
	}

	public String getIPServer() {
		return IPServer;
	}

	public void setIPServer(String iPServer) {
		IPServer = iPServer;
	}

	public int getPortServer() {
		return portServer;
	}

	public void setPortServer(int portServer) {
		this.portServer = portServer;
	}

	public String getIPListener() {
		return IPListener;
	}

	public void setIPListener(String iPListener) {
		IPListener = iPListener;
	}

	public int getPortListener() {
		return portListener;
	}

	public void setPortListener(int portListener) {
		this.portListener = portListener;
	}

	public int getTimeoutISORequest() {
		return timeoutISORequest;
	}

	public void setTimeoutISORequest(int timeoutISORequest) {
		this.timeoutISORequest = timeoutISORequest;
	}

	public int getEchoInterval() {
		return echoInterval;
	}

	public void setEchoInterval(int echoInterval) {
		this.echoInterval = echoInterval;
	}

	public int getConnectInterval() {
		return connectInterval;
	}

	public void setConnectInterval(int connectInterval) {
		this.connectInterval = connectInterval;
	}

	public String getProcodeInquiryPayment() {
		return procodeInquiryPayment;
	}

	public void setProcodeInquiryPayment(String procodeInquiryPayment) {
		this.procodeInquiryPayment = procodeInquiryPayment;
	}

}
