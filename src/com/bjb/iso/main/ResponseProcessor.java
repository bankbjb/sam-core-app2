package com.bjb.iso.main;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.cache.MapResponses;
import com.bjb.constants.Constants;
import com.bjb.iso.util.ISOHelper;
import com.bjb.iso.util.SamPackager;

@Component
public class ResponseProcessor implements Runnable {

	private static Queue<byte[]> responseQueue = new LinkedList<>();
	InputStream in = OpenSocketConnector.in;
	Object mutex = new Object();
	private static Logger log = LoggerFactory.getLogger(ResponseProcessor.class);
	
	SamPackager samPackager = new SamPackager();
	@Override
	public void run() {
		// initiate
		log.info(Thread.currentThread().getName() + " running...");
		ISOHelper helper = new ISOHelper();
		while (true) {
			synchronized (mutex) {
				if (getResponseQueue() != null && getResponseQueue().size() > 0) {
					// processing data byte received from stream into string
					if (getResponseQueue().peek() != null) {
						String isoResponse = helper.byteToString(getResponseQueue().poll());
						log.info(isoResponse);
						// [PRE PROCESS] checking iso response contain multiple
						// response
						// parsing batch response into list single response
						List<String> listResponse = new ArrayList<String>();
						listResponse.add(isoResponse);
						// listResponse = parseBatchResponse(isoResponse);

						// [PRE PROCESS] listResponse (in string) into
						// listResponseISOMsg (in ISOMsg) into dump response
						List<ISOMsg> listResponseISOMsg = new ArrayList<>();
						listResponseISOMsg = preProcessBatchResponse(listResponse);

						// [MAIN] processing all response we got
						processAllResponse(listResponseISOMsg);
					}
				}
			}
		}
	}

	/**
	 * List of singleResponse we got before, will be changed into ISOMsg. With
	 * that, we can dump to see the bit of response. For each element of list we
	 * will change it into ISOMsg and return back into list of
	 * singleResponseISOMsg.
	 * 
	 * @param listResponse
	 *            - list of singleReponse in string
	 * @return listResponseISOMsg - list of singleResponse in ISOMsg
	 */
	private List<ISOMsg> preProcessBatchResponse(List<String> listResponse) {
		List<ISOMsg> listResponseISOMsg = new ArrayList<>();

		for (String s : listResponse) {
			// String subResponse = s.substring(2);
			String subResponse = s;
			ISOMsg response = new ISOMsg();
			response.setPackager(samPackager);
			try {
				response.unpack(subResponse.getBytes());
				response.dump(System.out, "[RESPONSE] ");
				if (!listResponseISOMsg.contains(response)) {
					listResponseISOMsg.add(response);
				} else {
					log.warn("[DUPLICATE] response contain in listResponseISOMsg...");
				}
			} catch (ISOException e) {
				e.printStackTrace();
			}
		}

		return listResponseISOMsg;
	}

	/**
	 * List of singleResponseISOMsg we got, we will process it. The process are
	 * : matching with the request sent from PostingScheduler, when match
	 * insert/update into table cme_bulk_ft_resp, update span_penihilan_log,
	 * update response_log
	 * 
	 * @param listResponseISOMsg
	 *            - list of singleResponseISOMsg
	 */
	private void processAllResponse(List<ISOMsg> listResponseISOMsg) {
		for (ISOMsg msg : listResponseISOMsg) {
			// check response with sentMessage
			log.info(msg.getString(11));
//			msg.dump(System.out, "ALL");
			try {
				log.info(new String(msg.pack(),Constants.DEFAULT_CHARSET));
				if (!msg.getMTI().equals("0810")) {
					MapResponses.res.put(msg.getString(11), msg);
					log.info("Response : " + msg.getString(11));
					log.info("MAP SIZE : " + MapResponses.res.size());
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}


	public static Queue<byte[]> getResponseQueue() {
		return responseQueue;
	}

	public static void setResponseQueue(Queue<byte[]> responseQueue) {
		ResponseProcessor.responseQueue = responseQueue;
	}

}
