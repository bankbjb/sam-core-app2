package com.bjb.iso.main;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.cache.MapResponses;
import com.bjb.constants.Constants;
import com.bjb.iso.util.SamPackager;
import com.bjb.util.PubUtil;

/**
 * This class handles how responses from socket to be parsed and saved to
 * MapResponses.class
 * 
 * @author arifino TODO to MESSAGES
 */
@Component
public class ResponseHandler implements Runnable {

	InputStream in = OpenSocketConnector.in;
	Object mutex = new Object();

	private static Logger log = LoggerFactory.getLogger(ResponseHandler.class);
	
	@Autowired
	SamPackager samPackager;

	@Override
	public void run() {
		log.info(Thread.currentThread().getName() + " running...");
		// ISOHelper helper = new ISOHelper();
		int count = 0;
		// byte[] data = new byte[1024];
		byte[] data = new byte[321000];
		try {
			while ((count = in.read(data)) >= 0) {
				synchronized (mutex) {
					// System.out.println("length data read from socket: " +
					// count);
					// System.out.println(new
					// String(data,Constants.DEFAULT_CHARSET));
					// int length = PubUtil.getIntegerFrom2ByteArray(data);
					// System.out.println("BYTE LENGTH "+length);
					// String isoResponse = new String(data,
					// StandardCharsets.UTF_8);
					String isoResponse = new String(data, Constants.DEFAULT_CHARSET);
					// System.out.println("ISO ASCII "+isoResponse);
					// System.out.println(PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes(StandardCharsets.ISO_8859_1)));
					// System.out.println("LENGTH ISORESPONSE :
					// "+isoResponse.length());
					// System.out.println("RAW RESPONSE : "+isoResponse);
					// System.out.println("LENGTH :
					// "+PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes(StandardCharsets.ISO_8859_1)));
					isoResponse.substring(0, count);
					// System.out.println("CUT RESPONSE : "+isoResponse);
					// System.out.println("CR LENGTH :
					// "+PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes(StandardCharsets.ISO_8859_1)));
					// String subIsoResponse = isoResponse.substring(2);
					// System.out.println("[ISO RESPONSE] : "+subIsoResponse);
					//
					// if (subIsoResponse.startsWith("0810")
					// || subIsoResponse.startsWith("0800")) {
					// System.out
					// .println("receive network management response ...");
					// } else {
					// System.out.println("transaction response ...");
					//
					// // checking iso response contain multiple response or not
					// | if yes then parse it
					// List<String> listResponse = new ArrayList<String>();
					// int msgLength =
					// com.bjb.util.PubUtil.getIntegerFrom2ByteArray(data);
					// System.out.println(msgLength);
					// while(msgLength > 0){
					// if (isoResponse.length() > msgLength){
					// String singleResponse = isoResponse.substring(0,
					// msgLength+2);
					// if (!listResponse.contains(singleResponse)){
					// System.out.println(singleResponse);
					// listResponse.add(singleResponse);
					// } else {
					// System.out.println("[DUPLICATE] response contain in
					// listResponse...");
					// }
					//
					// isoResponse = isoResponse.substring(msgLength+2);
					// msgLength =
					// PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes());
					// }
					// else {
					// System.out.println("isoResponse length less than
					// msgLength ...");
					// break;
					// }
					// }
					// System.out.println("end while");
					// // unpacking response
					// //List<ISOMsg> listResponseISOMsg = new ArrayList<>();
					// for (String s : listResponse) {
					// String subResponse = s.substring(2);
					// ISOMsg response = new ISOMsg();
					// response.setPackager(new SAMPackager());
					// try {
					// response.unpack(subResponse.getBytes());
					// response.dump(System.out, "RESPONSE : ");
					// String key = response.getString(11);
					// // if (!ISOResList.containsValue(response)){
					// // ISOResList.add(response);
					// // System.out.println("ADD RESPONSE : "+new
					// String(ISOResList.get(key).pack()));
					// // }
					// if(!MapResponses.res.containsKey(response.getString(11))){
					// MapResponses.res.put(response.getString(11), response);
					// System.out.println("add response : "+new
					// String(response.pack()));
					// }
					// else {
					// System.out.println("[DUPLICATE] response contain in
					// listResponseISOMsg...");
					// }
					//
					// } catch (ISOException e) {
					// e.printStackTrace();
					// }
					// }
					// }

					// [PRE PROCESS] checking iso response contain multiple
					// response
					// parsing batch response into list single response
					List<String> listResponse = new ArrayList<String>();
					listResponse = parseBatchResponse(isoResponse);

					// [PRE PROCESS] listResponse (in string) into
					// listResponseISOMsg (in ISOMsg) into dump response
					List<ISOMsg> listResponseISOMsg = new ArrayList<>();
					listResponseISOMsg = preProcessBatchResponse(listResponse);

					// [MAIN] processing all response we got
					processAllResponse(listResponseISOMsg);

				}
				data = new byte[321000];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.info(Thread.currentThread().getName() + " end...");

	}

	private List<String> parseBatchResponse(String isoResponse) {
		List<String> listResponse = new ArrayList<>();
		int firstMsgLength = PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes(StandardCharsets.ISO_8859_1));

		while (firstMsgLength > 0) {
			// System.out.println("[INFO] Message length read from socket : " +
			// firstMsgLength);
			if (isoResponse.length() >= firstMsgLength) {
				// System.out.println("ISORESPONSE : "+isoResponse);
				String singleResponse = isoResponse.substring(0, firstMsgLength + 2);
				// System.out.println("singleResponse : "+singleResponse);
				String singleResponseTemp = singleResponse.substring(2);
				// System.out.println("tempresponse : "+singleResponseTemp);
				// echo forget
				if (singleResponseTemp.startsWith("0800") || singleResponseTemp.startsWith("0810")) {
					ISOMsg m = new ISOMsg();
					m.setPackager(samPackager);
					try {
						m.unpack(singleResponseTemp.getBytes(Constants.DEFAULT_CHARSET));
						m.dump(System.out, "");
						// System.out.println("Echo Response received.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					if (!listResponse.contains(singleResponse)) {
						log.info("[SINGLE RESPONSE]" + singleResponse);
						listResponse.add(singleResponse);
					} else {
						log.warn("[DUPLICATE] response contain in listResponse...");
					}
				}
				isoResponse = isoResponse.substring(firstMsgLength + 2);
				if (isoResponse.length() == 0)
					firstMsgLength = 0;
				else
					firstMsgLength = PubUtil
							.getIntegerFrom2ByteArray(isoResponse.getBytes(StandardCharsets.ISO_8859_1));
				log.info("[INFO] Message length after processing singleresponse : " + firstMsgLength);
			} else {
				log.warn("[WARNING] IsoResponse length less than msgLength ...");
				break;
			}
		}
		log.info("End while - end parse batch response");

		return listResponse;
	}

	// private List<String> parseBatchResponse(byte[] isoResponse) {
	// List<String> listResponse = new ArrayList<>();
	// int firstMsgLength = PubUtil.getIntegerFrom2ByteArray(isoResponse);
	//
	// while(firstMsgLength > 0){
	// System.out.println("[INFO] Message length read from socket : " +
	// firstMsgLength);
	// byte[] singleData = Arrays.copyOfRange(isoResponse, 0, firstMsgLength);
	// if (isoResponse. >= firstMsgLength){
	// System.out.println("ISORESPONSE : "+isoResponse);
	// String singleResponse = isoResponse.substring(0, firstMsgLength+2);
	// System.out.println("singleResponse : "+singleResponse);
	// String singleResponseTemp = singleResponse.substring(2);
	// System.out.println("tempresponse : "+singleResponseTemp);
	// // echo forget
	// if (singleResponseTemp.startsWith("0800") ||
	// singleResponseTemp.startsWith("0810")){
	// ISOMsg m = new ISOMsg();
	// try {
	//// m.unpack(singleResponseTemp.getBytes());
	//// m.dump(System.out, "");
	// System.out.println("echo response get.");
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// else {
	// if (!listResponse.contains(singleResponse)){
	// System.out.println("[SINGLE RESPONSE]" + singleResponse);
	// listResponse.add(singleResponse);
	// } else {
	// System.out.println("[DUPLICATE] response contain in listResponse...");
	// }
	// }
	// isoResponse = isoResponse.substring(firstMsgLength+2);
	// if (isoResponse.length() == 0)
	// firstMsgLength = 0;
	// else
	// firstMsgLength =
	// PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes());
	// System.out.println("[INFO] Message length after processing singleresponse
	// : " + firstMsgLength);
	// }
	// else {
	// System.out.println("[WARNING] IsoResponse length less than msgLength
	// ...");
	// break;
	// }
	// }
	// System.out.println("[INFO] End while - end parse batch response");
	//
	// return listResponse;
	// }

	/**
	 * List of singleResponse we got before, will be changed into ISOMsg. With
	 * that, we can dump to see the bit of response. For each element of list we
	 * will change it into ISOMsg and return back into list of
	 * singleResponseISOMsg.
	 * 
	 * @param listResponse
	 *            - list of singleReponse in string
	 * @return listResponseISOMsg - list of singleResponse in ISOMsg
	 */
	private List<ISOMsg> preProcessBatchResponse(List<String> listResponse) {
		List<ISOMsg> listResponseISOMsg = new ArrayList<>();

		for (String s : listResponse) {
			// System.out.println("RESPONSE "+s);
			String subResponse = s.substring(2);
			// System.out.println("SUBS RSPONSE "+subResponse);
			ISOMsg response = new ISOMsg();
			response.setPackager(samPackager);
			try {
				response.unpack(subResponse.getBytes(Constants.DEFAULT_CHARSET));
				response.dump(System.out, "[RESPONSE] ");
				if (!listResponseISOMsg.contains(response)) {
					log.info("ADD " + new String(response.pack()));
					listResponseISOMsg.add(response);
				} else {
					log.warn("[DUPLICATE] response contain in listResponseISOMsg...");
				}
			} catch (ISOException e) {
				e.printStackTrace();
			}
		}

		return listResponseISOMsg;
	}

	/**
	 * List of singleResponseISOMsg we got, we will process it. The process are
	 * : putting message to mapresponses.class
	 * 
	 * @param listResponseISOMsg
	 *            - list of singleResponseISOMsg
	 */
	private void processAllResponse(List<ISOMsg> listResponseISOMsg) {
		for (ISOMsg msg : listResponseISOMsg) {
			// check response with sentMessage
			log.info("Response with stan : "+msg.getString(11));
			msg.dump(System.out, "ADDED");
			MapResponses.res.put(msg.getString(11), msg);
		}
	}
}
