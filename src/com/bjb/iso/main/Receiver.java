package com.bjb.iso.main;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.util.PubUtil;

public class Receiver implements Runnable {

	InputStream in = OpenSocketConnector.in;
	
	Object mutex = new Object();
	Logger log = LoggerFactory.getLogger(Receiver.class);
	
	@Override
	public void run() {
		// initiate
		log.info(Thread.currentThread().getName() + " running...");
		int count = 0;
		byte[] data = new byte[1024];
		int len = 2;
		int isoDataLength = 0;
		// main processing
		try {
			while ((count = in.read(data, 0, len)) >= 0) {
				log.info("length data read from socket: " + count);
				isoDataLength = PubUtil.getIntegerFrom2ByteArray(data);
				log.info("iso data length : " + isoDataLength);
				data = new byte[isoDataLength];
				in.read(data, 0, isoDataLength);
//				logger.debug("response : " + new String(data));
				ResponseProcessor.getResponseQueue().add(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.info(Thread.currentThread().getName() + " end...");
	}

}
