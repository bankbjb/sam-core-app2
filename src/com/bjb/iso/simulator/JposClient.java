package com.bjb.iso.simulator;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISORequest;
import org.jpos.iso.channel.ASCIIChannel;
import org.springframework.stereotype.Component;

import com.bjb.iso.util.SamPackager;
import com.bjb.resource.additional.AmountInformation;
import com.bjb.util.CommonUtils;

@Component
public class JposClient {

	public static void main(String... args) throws ISOException, IOException {

		// ** config client **//
		String hostname = "10.6.226.20";// BaseConfig.getBaseConfig().getIPListener();
		int portNumber = 12345;// BaseConfig.getBaseConfig().getPortListener();

		System.out.println("Connecting to " + hostname + " : " + portNumber);
		ISOPackager samPackager = new SamPackager();

		ASCIIChannel channel = new ASCIIChannel(hostname, portNumber, samPackager);
		try {
			channel.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ISOMUX isoMux = new ISOMUX(channel) {
			@Override
			protected String getKey(ISOMsg m) throws ISOException {
				return super.getKey(m);
			}
		};

		new Thread(isoMux).start();
		// ** end config client **//

		// BaseConnector connector = new BaseConnector("localhost", 12346);

		//** create network request **//
		ISOMsg networkReq = new ISOMsg();
		networkReq = createDummyNetworkRequets(networkReq);

		//** send network request **//
		ISORequest req = new ISORequest(networkReq);
		isoMux.queue(req);

		//** get network response **//
		// System.out.println("timeout : " +
		// BaseConfig.getBaseConfig().getTimeoutISORequest() + " "
		// + BaseConfig.getBaseConfig().getTimeoutISORequest() * 1000);
		ISOMsg isoResponse = req.getResponse(10 * 1000);

		if (isoResponse != null) {
			dumpIso(networkReq, isoResponse);

			// ** if success then send inquiry saldo request **//
			ISOMsg msg = new ISOMsg();
//			msg = createDummyInquirySaldo(msg);
//			msg.dump(System.out, "");
//			ISORequest reqs = new ISORequest(msg);
//			isoMux.queue(reqs);

			//ISOMsg reply = reqs.getResponse(30 * 1000);
//			System.out.println("REPLY");
//			reply.dump(System.out, "");
//			System.out.println("[TRACE] [main] [inquiry saldo]");
			//dumpIso(msg, reply);

			/*==========Purchase==========*/
			// ISOMsg msgPurchase = new ISOMsg();
			// msgPurchase = createDummyPurchase(msgPurchase);
			// ISORequest reqPurchase = new ISORequest(msgPurchase);
			// isoMux.queue(reqPurchase);
			//
			// ISOMsg replyPurchase =
			// reqPurchase.getResponse(30000);
			// System.out.println("[TRACE] [main] [purchase]");
			// dumpIso(msgPurchase, replyPurchase);
//			
			/*==========Inquiry Payment==========*/
			 ISOMsg msgInqPayment = new ISOMsg();
			 msgInqPayment = createDummyInquiryPayment(msgInqPayment);
			 ISORequest reqInqPayment = new ISORequest(msgInqPayment);
			 isoMux.queue(reqInqPayment);
			
			 ISOMsg replyInqPayment =
			 reqInqPayment.getResponse(30000);
			 System.out.println("[TRACE] [main] [inquiry payment]");
			 dumpIso(msgInqPayment, replyInqPayment);

//			 /*==========Payment==========*/
//			 ISOMsg msgPayment = new ISOMsg();
//			 msgPayment = createDummyPayment(msgPayment);
//			 //msgPayment.set(32, "0102");
//			 //msgPayment.set(60, "106");
//			 ISORequest reqPayment = new ISORequest(msgPayment);
//			 isoMux.queue(reqPayment);
//			
//			 ISOMsg replyPayment =
//			 reqPayment.getResponse(30000);
//			 reqPayment.dump(System.out, "RES");
//			 System.out.println("[TRACE] [main] [payment]");
//			 dumpIso(msgPayment, replyPayment);
//			 
		} else {
			dumpIso(networkReq, isoResponse);
			System.err.println("null response");
		}

	}

	public static ISOMsg createDummyNetworkRequets(ISOMsg msg) throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, "9000");
//		msg.set(15, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(70, "301");

		return msg;
	}

	public static ISOMsg createDummyInquirySaldo(ISOMsg msg) throws ISOException {
		msg.setMTI("0200");
		msg.set(2, "6220118888888888");
		msg.set(3, "301000");
		msg.set(4, "1234990");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, "9001");
		msg.set(32, "0001");
		msg.set(35, "6220118888888888=9912?");
		msg.set(41, "323");
		msg.set(42, "Tem");
		msg.set(43, "nae");
		msg.set(49, "360");
		msg.set(59, "PAY");
		// msg.set(63, "01");
		msg.set(102, "0023269392100");

		return msg;
	}

	public static ISOMsg createDummyPurchase(ISOMsg msg) throws ISOException {
		msg.setMTI("0200");
		msg.set(2, "6220118888888888");
		msg.set(3, "501089");
		msg.set(4, "1000990");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, "9002");
		msg.set(32, "55555");
		msg.set(35, "6220118888888888=9912?");
		msg.set(41, "asdf1234");
		msg.set(42, "Term Name");
		msg.set(43, "Card Acceptor Name");
		msg.set(49, "360");
		msg.set(59, "PAY");
		msg.set(63, "01");
		msg.set(102, "1234567890123");

		return msg;
	}

	public static ISOMsg createDummyInquiryPayment(ISOMsg msg) throws ISOException {
//		/*======= PBB 341018 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "341018");
//		msg.set(4, "");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "490213");
//		msg.set(12, "103535");
//		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
//		msg.set(15, "0316");
//		msg.set(18, "6015");
//		msg.set(22, "010");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37, "000000490213");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
////		msg.set(57, new AmountInformation("9000", "200", "75").pack());
//		msg.set(57, "000000000000000000000000000000000000000000000000");
//		msg.set(59, "PAY122");
//		msg.set(60, "122");
//		String b1 = "367503000700702130";
//		String b2 = "2015";
//		b1 = CommonUtils.padright(b1, 18, ' ');
//		b2 = CommonUtils.padleft(b2, 4, ' ');
//		msg.set(61, b1);
//		msg.set(102, "0080806231001");
//		msg.set(103, "0023269392101");
		
//		/*======= SIPANDU 300020 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "300020");
//		msg.set(4, "");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "490035");
//		msg.set(12, "183535");
//		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
//		msg.set(15, "0316");
//		msg.set(18, "6025");
//		msg.set(22, "110");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37,"000000490035");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
//		msg.set(57, "000000000000000000000000000000000000000000000000");
//		msg.set(59, "PAY");
//		msg.set(60, "841");// Provinsi */ 
////		msg.set(60, "830"); // Kabupaten
//		msg.set(61,"20180841        ");
//		msg.set(102,"0080806231001");
//		msg.set(103,"0012199936360");	
		
		/*======= PDAM 321066 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "321066");
//		msg.set(4, "");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "490042");
//		msg.set(12, "143029");
//		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
//		msg.set(15, "0319");
//		msg.set(18, "6015");
//		msg.set(22, "021");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37, "000000490042");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
//		msg.set(57, "000000000000000000000000000000000000000000000000");
//		msg.set(59, "PAY");
//		msg.set(60, "466");
//		msg.set(61, "33000100          ");
//		msg.set(102,"0080806231001");
//		msg.set(103,"0023269392101");
//		return msg;
//	}
		
		
		/*======= ESAMSAT 301099 =======*/
		msg.setMTI("0200");
		msg.set(2, "6220118888888888888");
		msg.set(3, "301099");
		msg.set(4, "000000000000");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, "531294");
		msg.set(12, "090210");
		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(15, "0426");
		msg.set(18, "6015");
		msg.set(22, "110");
		msg.set(32, "102");
		msg.set(35, "622011888888888888=9912?");
		msg.set(37, "000000531294");
		msg.set(41, "01234567");
		msg.set(42, "012345678901234");
		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
		msg.set(48, "0137");
		msg.set(49, "360");
		msg.set(57, "000000000000000000000000000000000000000000000000");
		msg.set(59, "PAY017");
		msg.set(60, "017");
		msg.set(61, "3221801904180005");
		msg.set(102,"0004444213001");
		msg.set(103,"0012199936360");
		return msg;
	}

	public static ISOMsg createDummyPayment(ISOMsg msg) throws ISOException {
//		/*======= PBB 541018 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "541018");
//		msg.set(4, "1045144");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "489701");
//		msg.set(12, "103535");
//		msg.set(13, "0316");
//		msg.set(15, "0316");
//		msg.set(18, "6015");
//		msg.set(22, "010");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37, "000000489699");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
//		msg.set(57,"000001043444000000000000000000001700000001045144");
//		msg.set(59, "PAY122");
//		msg.set(60,"122");
//		msg.set(61,"3675030007007021202017WAJIB PAJAK AGGREGATOR  9          BL. SANGIANG                       KELURAN 7                        KECAMATAN 30                       BANTEN                             000000164100000000000000201409300000007050300000338414000001043444000000000000");
//		msg.set(102, "0023269392100");
//		msg.set(103, "1029131221321");

//		/*======= SIPANDU 500020 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "500020");
//		msg.set(4, "19000000");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "489702");
//		msg.set(12, "103535");
//		msg.set(13, "0316");
//		msg.set(15, "0316");
//		msg.set(18, "6025");
//		msg.set(22, "010");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37, "000000489702");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
//		msg.set(57, "000019000000000000000000000000000000000019000000");
//		msg.set(59, "PAY");
//		msg.set(60, "841");
//		msg.set(61,"20180302        ");
//		msg.set(102, "0023269392100");
//		msg.set(103, "0023269392100");
		
//		/*======= PDAM 521066 =======*/
//		msg.setMTI("0200");
//		msg.set(2, "622011888888888888");
//		msg.set(3, "521066");
//		msg.set(4, "54600");
//		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
//		msg.set(11, "489703");
//		msg.set(12, "143029");
//		msg.set(13, "0320");
//		msg.set(15, "0320");
//		msg.set(18, "6015");
//		msg.set(22, "010");
//		msg.set(32, "102");
//		msg.set(35, "622011888888888888=9912?");
//		msg.set(37, "000000489703");
//		msg.set(41, "01234567");
//		msg.set(42, "012345678901234");
//		msg.set(43, "TESTSAMS0123456789012345TESTSAMS01234567");
//		msg.set(49, "360");
//		msg.set(57, "000000054600000000002500000000000000000000057100");
//		msg.set(59, "PAY");
//		msg.set(60, "466");
//		msg.set(61,"33000100          DMMY'SMLTOR.NAME-33000100DMMY'SMLTOR.ADDR-330001003A0                   000000005260000000000000000000000000000000000000000000000000000000000000002000                                                                                                                                ----03");
//		msg.set(102, "0080806231001");
//		msg.set(103, "0023269392101 0012199936360 ");
		return msg;
	}

	public static void dumpIso(ISOMsg request, ISOMsg response) throws ISOException {
		request.dump(System.out, "");
		System.out.println("Request : [" + new String(request.pack()) + "]");
		System.out.println("Response : [" + new String(response.pack()) + "]");
	}
}
