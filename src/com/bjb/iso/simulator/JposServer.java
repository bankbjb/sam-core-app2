package com.bjb.iso.simulator;

import java.io.IOException;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.PostChannel;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bjb.constants.Constants;
import com.bjb.iso.util.SamPackager;
import com.bjb.resource.additional.AmountInformation;
import com.bjb.resource.additional.Bills;
import com.bjb.util.CommonUtils;

public class JposServer implements ISORequestListener {

	private static Logger log = LoggerFactory.getLogger(JposServer.class);

	public static void main(String[] args) throws ISOException {
		String hostname = "localhost";
		int portNumber = 10511;

		ISOPackager samPackager = new GenericPackager("D:/Kerja/SAM-BJB/resources/iso8583.xml");//SAMPackager();

		ServerChannel sahChannel = new PostChannel(hostname, portNumber, samPackager);

		ISOServer sahServer = new ISOServer(portNumber, sahChannel, null);
		sahServer.addISORequestListener(new JposServer());

		new Thread(sahServer).start();

		log.info("Server ready receive connection on port [" + portNumber + "]");
	}

	@Override
	public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
		try {
			log.info("Server receives connection from ["
					+ ((BaseChannel) isoSrc).getSocket().getInetAddress().getHostAddress() + "]");

			if (isoMsg.getMTI().equalsIgnoreCase("0800")) {
				acceptNetworkMsg(isoSrc, isoMsg);
			} else if (isoMsg.getMTI().equalsIgnoreCase("0200")) {
				processRequest(isoSrc, isoMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void acceptNetworkMsg(ISOSource isoSrc, ISOMsg isoMsg) throws ISOException, IOException {
		if (isoMsg.getMTI().equalsIgnoreCase("0800")) {
			log.info("Accepting Network Management Request");
			ISOMsg reply = (ISOMsg) isoMsg.clone();
			reply.setMTI("0810");
			reply.set(39, "00");

			isoSrc.send(reply);
		}
	}

	private void processRequest(ISOSource isoSrc, ISOMsg isoMsg) throws ISOException, IOException {
		System.out.println("RECEIVING ISO MESSAGE : "+new String(isoMsg.pack(),Constants.DEFAULT_CHARSET));
		if (isoMsg.getMTI().equalsIgnoreCase("0200")) {
			// inquiry saldo
			if (isoMsg.getString(3).equalsIgnoreCase("301000")) {
				log.info("[Inquiry Saldo] Accepting Finance Transaction Request");
				ISOMsg reply = (ISOMsg) isoMsg.clone();
				reply.setMTI("0210");
				reply.set(39, "00");
				reply.set(54, "0199000");
				isoSrc.send(reply);
			} else if (isoMsg.getString(3).equalsIgnoreCase("501089")) {
				log.info("[Purchase] Accepting Finance Transaction Request");
				ISOMsg reply = (ISOMsg) isoMsg.clone();
				reply.setMTI("0210");
				reply.set(39, "00");
				String res = CommonUtils.padright("purchase", 20, ' ') + CommonUtils.padright((CommonUtils.unPadRight(isoMsg.getString(61).substring(0,20),' ')+isoMsg.getString(60)),20,' ') + CommonUtils.padright((CommonUtils.unPadRight(isoMsg.getString(61).substring(20,40),' ')), 20, ' ');
				// reply.set(57, StringUtils.leftPad("123", 48, '0'));
				reply.set(61, res);
				isoSrc.send(reply);
			} else if (isoMsg.getString(3).equalsIgnoreCase("341018")) {
				log.info("[Inquiry Payment] Accepting Finance Transaction Request");
				ISOMsg reply = (ISOMsg) isoMsg.clone();
				reply.setMTI("0210");
				String billamount = "161000";
				billamount = CommonUtils.padleft(billamount, 12, '0');
				reply.set(4, billamount);
				billamount = CommonUtils.padright(billamount, 48, '0');
				reply.set(57, billamount);
				reply.set(39, "00");
				String b1 = isoMsg.getString(61).substring(0,20);
				//b1 = CommonUtils.padright(b1, 20, ' ');
				String b2 = "081121";
				//b2 = CommonUtils.padleft(b2, 20, '0');
				String b3 = "1";
				//b3 = CommonUtils.padleft(b3, 20, '0');
				String b4 = "19847265789";
				String b5 = "185000";
				String b6 = "Andi Suhanjatprana";
				b1 = CommonUtils.padleft(b1, 20, '0');
				b2 = CommonUtils.padright(b2, 20, ' ');
				b3 = CommonUtils.padright(b3, 20, ' ');
				b4 = CommonUtils.padleft(b4, 20, ' ');
				b5 = CommonUtils.padleft(b5, 20, '0');
				b6 = CommonUtils.padright(b6, 20, ' ');
				reply.set(60, isoMsg.getString(60));
				String b61 = b1+b2+b3+b4+b5+b6;
				//String res = CommonUtils.padright("inqpayment", 20, ' ') + CommonUtils.padright((CommonUtils.unPadRight(isoMsg.getString(61).substring(0,20),' ')+isoMsg.getString(60)),20,' ') + CommonUtils.padright(isoMsg.getString(32), 20, ' ');
				reply.set(61, b61);
				log.info("BIT 4 : " + reply.getString(4));
				reply.dump(System.out, "REPLY");
				// reply.set(57, StringUtils.leftPad("123", 48, '0'));
				// reply.set(61, StringUtils.rightPad("BIT61", 999, ' '));
				isoSrc.send(reply);
			} else if (isoMsg.getString(3).equalsIgnoreCase("541018")) {
				log.info("[Payment] Accepting Finance Transaction Request");
				isoMsg.dump(System.out, "REQUEST");
				ISOMsg reply = (ISOMsg) isoMsg.clone();
				reply.setMTI("0210");
				reply.set(39, "00");
				log.info("BIT 4 : " + reply.getString(4));
				String res = CommonUtils.padright("payment", 20, ' ') + CommonUtils.padright((CommonUtils.unPadRight(isoMsg.getString(61).substring(0,20),' ')+isoMsg.getString(60)),20,' ') + CommonUtils.padright((CommonUtils.unPadRight(isoMsg.getString(61).substring(20,40),' ')), 20, ' ');
				// reply.set(57, StringUtils.leftPad("123", 48, '0'));
				//reply.set(61, res);
				reply.dump(System.out, "REPLY");
				// reply.set(57, StringUtils.leftPad("123", 48, '0'));
				// reply.set(61, StringUtils.rightPad("BIT61", 999, ' '));
				isoSrc.send(reply);
			}
		} else {
			log.warn("invalid request MTI");
		}

	}

}
