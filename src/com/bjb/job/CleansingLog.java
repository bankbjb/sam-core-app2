package com.bjb.job;

import java.sql.Time;
import java.util.Calendar;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.model.SystemParameter;
import com.bjb.repository.StoreProcRepository;
import com.bjb.repository.SystemParameterRepository;

/**
 * This class is quartzJob to call storedprocedure for cleansing log (archiving
 * transaction & message log into history table)
 * 
 * @author arifino
 *
 */
@Component
public class CleansingLog extends QuartzJobBean {

	private Logger log = LoggerFactory.getLogger(CleansingLog.class);

	static SystemParameterRepository systemParameterRepository;

	static StoreProcRepository storeProcRepository;

	@SuppressWarnings({ "deprecation" })
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info(Messages.CLEAN_LOG_START);
		SystemParameter timeLog = systemParameterRepository.findByName(Constants.CLEANSING_LOG_TIME);
		log.info("Time to log " + timeLog);
		if (timeLog != null) {
			if (timeLog.getStatus().equals("1")) {
				String[] times = timeLog.getValue().split(":");
				Time time = new Time(Integer.valueOf(times[0]), Integer.valueOf(times[1]), Integer.valueOf(times[2]));
				Long ttlog = time.getTime();
				System.out.println("TIME TO LOG : " + time +" : "+ttlog+" ms");
				Calendar cal = Calendar.getInstance();
				Time time2 = new Time(cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
				Long ttnow = time2.getTime();
				System.out.println("TIME NOW : " + time2+" : "+ttnow+" ms");
				Long interval = ttnow < ttlog ? ttlog - ttnow : (ttlog - ttnow) + (24 * 60 * 60 * 1000);
				try {
					log.info("INTERVAL : " + interval / 1000 + " second / " + interval / 60000 + " minute / "
							+ interval / 3600000 + " hour / " + interval / (24 * 3600000) + " day");
					log.info("Cleansing Will Start after " + interval + " ms");
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				String response = storeProcRepository.CleansingLog();
				log.info(Messages.CLEAN_LOG_END + " [Code : " + response + " ]");
			} else {
				log.info(Messages.CLEAN_LOG_END + " [Code : DISABLED ]");
			}
		}
	}

	public SystemParameterRepository getSystemParameterRepository() {
		return systemParameterRepository;
	}

	@Autowired
	public void setSystemParameterRepository(SystemParameterRepository systemParamterRepository) {
		systemParameterRepository = systemParamterRepository;
	}

	public StoreProcRepository getStoreProcRepository() {
		return storeProcRepository;
	}

	@Autowired
	public void setStoreProcRepository(StoreProcRepository storeProcRepositor) {
		storeProcRepository = storeProcRepositor;
	}

}
