package com.bjb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.MessageFields;

public interface MessageFieldsRepository extends JpaRepository<MessageFields, Integer> {
	public MessageFields findById(Integer id);
	public List<MessageFields> findByMessageTypeIgnoreCase(String type);
}
