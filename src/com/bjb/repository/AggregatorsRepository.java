package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.Aggregators;

public interface AggregatorsRepository extends JpaRepository<Aggregators,Integer> {
	public Aggregators findById(Integer id);
	public Aggregators findByCode(String code);
}
