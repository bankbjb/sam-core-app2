package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.CollectingAgent;

public interface CollectingAgentRepository extends JpaRepository<CollectingAgent, Integer> {

	public CollectingAgent findById(Integer id);
}
