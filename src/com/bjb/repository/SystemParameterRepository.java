package com.bjb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.SystemParameter;

public interface SystemParameterRepository extends JpaRepository<SystemParameter, String> {
	public SystemParameter findByName(String name);
	public List<SystemParameter> findByNameContaining(String name);
}
