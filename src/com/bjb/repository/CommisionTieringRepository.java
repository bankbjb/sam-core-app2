package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.CommissionTiering;

public interface CommisionTieringRepository extends JpaRepository<CommissionTiering, Integer> {

}
