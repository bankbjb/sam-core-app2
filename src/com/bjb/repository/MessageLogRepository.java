package com.bjb.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.model.MessageLog;

public interface MessageLogRepository extends JpaRepository<MessageLog, BigDecimal> {
	public List<MessageLog> findByCreateDateBetween(@Temporal(TemporalType.DATE) Date date1,@Temporal(TemporalType.DATE) Date date2);
	
	@Transactional
	public void deleteByCreateDateBetween(@Temporal(TemporalType.DATE) Date date1,@Temporal(TemporalType.DATE) Date date2);
	
}
