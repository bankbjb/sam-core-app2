package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.FeatureMapping;

public interface FeatureMappingRepository extends JpaRepository<FeatureMapping, Integer> {

	public FeatureMapping findById(Integer id);
	
	public FeatureMapping findByFeatureIdAndAggregatorId(Integer fid,Integer aggid);
}
