package com.bjb.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.model.EmailLogNotification;

public interface EmailLogNotificationRepository extends JpaRepository<EmailLogNotification, Integer> {
	public EmailLogNotification findByAggregatorIdAndDate(Integer id, @Temporal(TemporalType.DATE) Date date);
	
	public List<EmailLogNotification> findByDateBetween(@Temporal(TemporalType.DATE) Date date1,@Temporal(TemporalType.DATE) Date date2);

	@Transactional
	public void deleteByDateBetween(@Temporal(TemporalType.DATE) Date date1,@Temporal(TemporalType.DATE) Date date2);

}
