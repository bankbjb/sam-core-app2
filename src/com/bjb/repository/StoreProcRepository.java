package com.bjb.repository;

import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bjb.model.StoreProc;

public interface StoreProcRepository extends CrudRepository<StoreProc, Integer> {
	@Procedure("stan_generator")
	public String StanGenerator();
	
	@Procedure("next_stan")
	public String NextStan(@Param("inParam") Integer inParam);
	
	@Procedure("cleansing_log")
	public String CleansingLog();
}
