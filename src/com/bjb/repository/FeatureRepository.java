package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.Feature;

public interface FeatureRepository extends JpaRepository<Feature, Integer> {

	public Feature findById(Integer id);
}
