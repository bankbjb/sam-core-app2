package com.bjb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bjb.model.MessageMapping;

public interface MessageMappingRepository extends JpaRepository<MessageMapping,Integer>{
	public MessageMapping findById(Integer id);
}
