package com.bjb.cache;

import java.util.HashMap;
import java.util.Map;

import org.jpos.iso.ISOMsg;

/**
 * This class contains map to hold responses from IF400.
 * @author arifino
 *
 */

public class MapResponses {
	/**
	 * Map for caching response from IF400.
	 * Key 		: [String]stan
	 * Value 	: [object:isoMsg]response
	 */
	public static Map<String, ISOMsg> res = new HashMap<>();
}
