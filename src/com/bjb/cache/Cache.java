package com.bjb.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bjb.constants.Messages;
/**
 * Class for initiating cache build.
 */
@Component
public class Cache {
	
	private static Logger log = LoggerFactory.getLogger(Cache.class);

	/**
	 * This method init all of the cache used in the system.
	 * SystemParameter,Aggregators,Stan,Bit57(FeatureMapping),Bit61(MessageMapping),MessageFields
	 * @author arifino
	 */
	public static void init() {
		try {
			log.info(Messages.CACHE_INIT);
			ForSystemParameter.init();
			ForAggregators.init();
			ForStan.init();
			ForBitFiftySevenMapping.init();
			ForBitSixtyOneMapping.init();
			ForBitSixtyOneMessageField.init();
			/*Ayuda add init forCaCode - 26-03-2018*/
			ForCaCode.init();
			log.info(Messages.CACHE_INITIATED);
		} catch (Exception e) {
			log.warn(e.getMessage());
		}
	}
}
