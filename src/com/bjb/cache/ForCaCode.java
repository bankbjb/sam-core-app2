package com.bjb.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Messages;
import com.bjb.dto.Pair;
import com.bjb.model.CollectingAgent;
import com.bjb.repository.CollectingAgentRepository;
import com.bjb.util.CommonUtils;
/*Ayuda add class for ca code - 26/03/2018*/
@Component
public class ForCaCode {

	private static Map<Integer, CollectingAgent> caMap = new HashMap<>();
	private static Logger log = LoggerFactory.getLogger(ForCaCode.class);
	private static CollectingAgentRepository collectingAgentRepository;
	
	@Autowired
	public void setCollectingAgentRepository(CollectingAgentRepository repo) {
		collectingAgentRepository = repo;
	}
	
	/**
	 * Init collectingAgent cache from db
	 */
	@SuppressWarnings("rawtypes")
	public static void init() {
		List list = collectingAgentRepository.findAll();
		if (list.size() > 0) {
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				CollectingAgent ca = (CollectingAgent) iterator.next();
				caMap.put(ca.getId(), ca);
			}
		}
		System.out.println(" Init Collecting Agent ");
	}

	
	public static Pair<Boolean, String> getValidCaCode(String caCode, String msg) {
		CollectingAgent ca = getCaCode(caCode);
		System.out.println("~1~ caCode : "+caCode);
		System.out.println("~2~ ca : "+ca);
		if (ca != null) {
			System.out.println("~3~ CA Found");
			return new Pair<>(true, ">> Collecting Agent Code Found <<");
		} else
			System.out.println("~3~ CA Not Found");
			return new Pair<>(false,">> Collecting Agent Code Not Found <<");// return false;
	}
	
	public static CollectingAgent getCaCode(String caCode) {
		// TODO bit use number?
		log.info(">> RETRIEVING Collecting Agent DATA <<");
		CollectingAgent ca = (CollectingAgent) caMap.get(Integer.valueOf(CommonUtils.removeUntilNumber(caCode)));
		if (ca != null) {
			log.info(">> Collecting Agent Code Found <<");
			return ca;
		} else
			log.info(">> Collecting Agent Code Not Found <<");
			return null;
	}
	/**
	 * Update cache if there is change in db. Tied to DBListener class.
	 * 
	 * @param caId
	 */
	public static void updateCache(String caId) {
		CollectingAgent ca = collectingAgentRepository.findById(Integer.valueOf(caId));
		if (ca != null) {
			caMap.put(ca.getId(), ca);
			log.info(Messages.CACHE_AGG_UPDATED);
		} else {
			log.info(Messages.CACHE_AGG_UPDATE_ERROR);
		}
	}

	/**
	 * Delete cache if there is change in db. Tied to DBListener class.
	 * 
	 * @param aggregatorId
	 */
	public static void deleteCache(String caId) {
		CollectingAgent ca = collectingAgentRepository.findById(Integer.valueOf(caId));
		if (ca != null) {
			caMap.remove(ca.getId());
			// System.out.println(agg);
			log.info(Messages.CACHE_AGG_UPDATED);
		} else {
			log.info(Messages.CACHE_AGG_UPDATE_ERROR);
		}
	}
}
