package com.bjb.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Messages;
import com.bjb.model.CommissionTiering;
import com.bjb.model.FeatureMapping;
import com.bjb.repository.FeatureMappingRepository;

/**
 * This class is cache for bit 57 building, include : featureMapping and commisionTiering.
 * 
 * @author arifino
 *
 */
@Component
public class ForBitFiftySevenMapping {
	/**
	 * Map for caching featureMapping.
	 * Key		: [string]aggregatorId+collectingAgentId+featureId (ex:100170108)
	 * Value	: [object:FeatureMapping]featureMapping
	 */
	private static Map<String, FeatureMapping> mapFiftySeven = new HashMap<>();

	private static FeatureMappingRepository featureMappingRepository;

	@Autowired
	public void setFeatureMappingRepository(FeatureMappingRepository repo) {
		featureMappingRepository = repo;
	}

	private static Logger log = LoggerFactory.getLogger(ForBitFiftySevenMapping.class);

	/**
	 * Init featureMapping cache from db.
	 */
	@SuppressWarnings("rawtypes")
	public static void init() {
		List list = featureMappingRepository.findAll();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			FeatureMapping mfm = (FeatureMapping) iterator.next();
			String key = "" + mfm.getAggregatorId().intValue() + mfm.getCollectingAgentId().intValue()
					+ mfm.getFeatureId().intValue();
			mapFiftySeven.put(key.toLowerCase(), mfm);
//			System.out.println("== Key : "+key.toLowerCase()+"value : "+mfm+" ==");
		}
	}

	/**
	 * Return featureMapping from cache.
	 * 
	 * @param aggCode
	 * @param caCode
	 * @param featureCode
	 * @return FeatureMapping
	 */
	public static FeatureMapping getFeatureMapping(Integer aggCode, Integer caCode, Integer featureCode) {
		String key = "" + aggCode.intValue() + caCode.intValue() + featureCode.intValue();
		return mapFiftySeven.get(key.toLowerCase());
	}

	public static boolean getValidFeatureMapping(Integer aggCode, Integer caCode, Integer featureCode) {
		FeatureMapping fm = getFeatureMapping(aggCode, caCode, featureCode);
		if (fm != null) {
			if (fm.getStatus().equals(Integer.valueOf(1))) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * Returns commision tiering from cache.
	 * 
	 * @param aggCode
	 * @param caCode
	 * @param featureCode
	 * @param billAmount
	 * @return CommisionTiering
	 */
	@SuppressWarnings("rawtypes")
	public static CommissionTiering getCommissionTiering(Integer aggCode, Integer caCode, Integer featureCode,
			Long billAmount) {
		log.info("Looking For Commision Tiering for : "+aggCode+", "+caCode+", "+featureCode);
		CommissionTiering c = null;
		FeatureMapping m = getFeatureMapping(aggCode, caCode, featureCode);
		if (m != null) {
			if (m.getStatus().equals(Integer.valueOf(1))) {
				Iterator iterator = m.getCommisionTieringSet().iterator();
				CommissionTiering cMax = null;
				while (iterator.hasNext()) {
					c = (CommissionTiering) iterator.next();
					if (c.getUpperLimit().longValue() == -1) {
						cMax = c;
					}
					if ((c.getLowerLimit().longValue() <= billAmount) && (billAmount < c.getUpperLimit().longValue())) {
						if (c.getStatus().equals(Integer.valueOf(1))) {
							//System.out.println("return c " + c);
							return c;
						} else
							return null;
					}
				}
				if (cMax != null) {
					if (cMax.getLowerLimit().longValue() <= billAmount) {
						if (cMax.getStatus().equals(Integer.valueOf(1))) {
							return cMax;
						} else
							return null;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Update cache if there is change in db. Tied to DBListener.class
	 * 
	 * @param key
	 */
	public static void updateCache(String key) {
		FeatureMapping fm = mapFiftySeven.get(key);
//		System.out.println("=== Key : "+ key);
		if (fm != null) {
			FeatureMapping mfm = featureMappingRepository.findById(fm.getId());
			if (mfm != null) {
				String keys = "" + mfm.getAggregatorId().intValue() + mfm.getCollectingAgentId().intValue()
						+ mfm.getFeatureId().intValue();
				mapFiftySeven.put(keys, mfm);
				log.info(Messages.CACHE_57_UPDATED);
			} else {
//				 System.out.println("====a1====");
				log.warn(Messages.CACHE_57_UPDATE_ERROR);
			}
		} else {
//			 System.out.println("====a2====");
			FeatureMapping mfm = featureMappingRepository.findById(Integer.valueOf(key));
//			System.out.println("=== mfm: "+ mfm);
			if (mfm != null) {
				String keys = "" + mfm.getAggregatorId().intValue() + mfm.getCollectingAgentId().intValue()
						+ mfm.getFeatureId().intValue();
				mapFiftySeven.put(keys, mfm);
				log.info(Messages.CACHE_57_UPDATED);
			} else {
//				 System.out.println("====a3====");
				log.warn(Messages.CACHE_57_UPDATE_ERROR);
			}
		}
	}

	/**
	 * Delete cache if there is change in db. Tied to DBListener.class
	 * @param key
	 */
	public static void deleteCache(String key) {
		FeatureMapping fm = mapFiftySeven.get(key);
		if (fm != null) {
			FeatureMapping mfm = featureMappingRepository.findById(fm.getId());
			if (mfm != null) {
				String keys = "" + mfm.getAggregatorId().intValue() + mfm.getCollectingAgentId().intValue()
						+ mfm.getFeatureId().intValue();
				mapFiftySeven.remove(keys);
				log.info(Messages.CACHE_57_UPDATED);
			} else {
				log.warn(Messages.CACHE_57_UPDATE_ERROR);
			}
		} else {
			// System.out.println("a2");
			FeatureMapping mfm = featureMappingRepository.findById(Integer.valueOf(key));
			if (mfm != null) {
				String keys = "" + mfm.getAggregatorId().intValue() + mfm.getCollectingAgentId().intValue()
						+ mfm.getFeatureId().intValue();
				mapFiftySeven.remove(keys);
				log.info(Messages.CACHE_57_UPDATED);
			} else {
				log.warn(Messages.CACHE_57_UPDATE_ERROR);
			}
		}

	}

}
