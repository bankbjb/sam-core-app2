package com.bjb.cache;

import org.jpos.iso.IFA_AMOUNT;
import org.jpos.iso.IFA_BINARY;
import org.jpos.iso.IFA_BITMAP;
import org.jpos.iso.IFA_LLCHAR;
import org.jpos.iso.IFA_LLLCHAR;
import org.jpos.iso.IFA_LLNUM;
import org.jpos.iso.IFA_NUMERIC;
import org.jpos.iso.IFB_BINARY;
import org.jpos.iso.IF_CHAR;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOFieldPackager;
import org.springframework.stereotype.Component;

/**
 * Returns length of field for bit 61 generation
 * @author arifino
 *
 */
@Component
public class SAMPackagerCache extends ISOBasePackager{
	protected static ISOFieldPackager fld[] = {
			/* 000 */new IFA_NUMERIC(4, "MessageTypeIndicator"),
			/* 001 */new IFA_BITMAP(16, "PRIMARYBITMAP"),
			/* 002 */new IFA_LLNUM(16, "PrimaryAccountNumber"),//in
			/* 003 */new IFA_NUMERIC(6, "ProcessingCode"),//in
			/* 004 */new IFA_NUMERIC(12, "AmountTransaction"),//in
			/* 005 */new IFA_NUMERIC(12, "AMOUNTSETTLEMENT"),
			/* 006 */new IFA_NUMERIC(12, "AMOUNTCARDHOLDER BILLING"),
			/* 007 */new IFA_NUMERIC(10, "TransmissionDateTime"),//in
			/* 008 */new IFA_NUMERIC(8, "AMOUNTCARDHOLDERBILLINGFEE"),
			/* 009 */new IFA_NUMERIC(8, "CONVERSIONRATESETTLEMENT"),
			/* 010 */new IFA_NUMERIC(8, "CONVERSIONRATECARDHOLDER BILLING"),
			/* 011 */new IFA_NUMERIC(6, "SystemTraceAuditNumber"),//in
			/* 012 */new IFA_NUMERIC(6, "TimeLocalTransaction"),//in
			/* 013 */new IFA_NUMERIC(4, "LocalTransactionDate"),//in
			/* 014 */new IFA_NUMERIC(4, "DATEEXPIRATION"),
			/* 015 */new IFA_NUMERIC(4, "SettlementDate"),//in
			/* 016 */new IFA_NUMERIC(4, "DATECONVERSION"),
			/* 017 */new IFA_NUMERIC(4, "DATECAPTURE"),
			/* 018 */new IFA_NUMERIC(4, "MerchantType"),//in
			/* 019 */new IFA_NUMERIC(3, "ACQUIRINGINSTITUTIONCOUNTRYCODE"),
			/* 020 */new IFA_NUMERIC(3, "PANEXTENDEDCOUNTRYCODE"),
			/* 021 */new IFA_NUMERIC(3, "FORWARDINGINSTITUTIONCOUNTRYCODE"),
			/* 022 */new IFA_NUMERIC(3, "POSEntry ModeCode"),//in
			/* 023 */new IFA_NUMERIC(3, "CARDSEQUENCENUMBER"),
			/* 024 */new IFA_NUMERIC(3, "NETWORKINTERNATIONALIDENTIFIEER"),
			/* 025 */new IFA_NUMERIC(2, "POINTOFSERVICECONDITIONCODE"),
			/* 026 */new IFA_NUMERIC(2, "POINTOFSERVICEPINCAPTURECODE"),
			/* 027 */new IFA_NUMERIC(1, "AUTHORIZATIONIDENTIFICATIONRESPLEN"),
			/* 028 */new IFA_AMOUNT(9, "AMOUNTTRANSACTIONFEE"),
			/* 029 */new IFA_AMOUNT(9, "AMOUNTSETTLEMENTFEE"),
			/* 030 */new IFA_AMOUNT(9, "AMOUNTTRANSACTIONPROCESSINGFEE"),
			/* 031 */new IFA_AMOUNT(9, "AMOUNTSETTLEMENTPROCESSINGFEE"),
			/* 032 */new IFA_LLNUM(5, "AggregatorCode"),//in
			/* 033 */new IFA_LLNUM(11, "FORWARDINGINSTITUTIONIDENTCODE"),
			/* 034 */new IFA_LLCHAR(28, "PANEXTENDED"),
			/* 035 */new IFA_LLCHAR(23, "Track2Data"),//in
			/* 036 */new IFA_LLLCHAR(104, "TRACK3DATA"),
			/* 037 */new IF_CHAR(12, "SequenceNumber"),//in
			/* 038 */new IF_CHAR(6, "AUTHORIZATIONIDENTIFICATIONRESPONSE"),
			/* 039 */new IFA_NUMERIC(2, "ResponseCode"),//iin
			/* 040 */new IF_CHAR(3, "SERVICERESTRICTIONCODE"),
			/* 041 */new IF_CHAR(8, "TerminalIdentificationNumber"),//in
			/* 042 */new IFA_NUMERIC(15, "TerminalName"),//in
			/* 043 */new IF_CHAR(40, "CardAcceptorName"),//in
			/* 044 */new IFA_LLCHAR(25, "ADITIONALRESPONSEDATA"),
			/* 045 */new IFA_LLCHAR(76, "TRACK1DATA"),
			/* 046 */new IFA_LLLCHAR(999, "ADITIONALDATAISO"),
			/* 047 */new IFA_LLLCHAR(999, "ADITIONALDATANATIONAL"),
			/* 048 */new IFA_LLLCHAR(999, "ADITIONALDATAPRIVATE"),	
			/* 049 */new IFA_NUMERIC(3, "CurrencyCode"),//in
			/* 050 */new IF_CHAR(3, "CURRENCY CODESETTLEMENT"),
			/* 051 */new IF_CHAR(3, "CURRENCY CODECARDHOLDERBILLING"),
			/* 052 */new IFB_BINARY(8, "PINDATA"),
			/* 053 */new IFA_NUMERIC(16, "SECURITYRELATEDCONTROLINFORMATION"),
			/* 054 */new IFA_LLCHAR(17, "InquirySaldo"),//in
			/* 055 */new IFA_LLLCHAR(999, "RESERVEDISO"),
			/* 056 */new IFA_LLLCHAR(999, "RESERVEDISO"),
			/* 057 */new IFA_LLCHAR(48, "AdditionalData"),//in
			/* 058 */new IFA_LLLCHAR(999, "RESERVEDNATIONAL"),
			/* 059 */new IFA_LLLCHAR(999, "AdditionalData"),//pay
			/* 060 */new IFA_LLCHAR(6, "featureCode"),//in
			/* 061 */new IFA_LLLCHAR(999, "AdditionalData"),//in
			/* 062 */new IFA_LLLCHAR(999, "RESERVEDPRIVATE"),
			/* 063 */new IFA_LLLCHAR(999, "AdditionalData"),//in
			/* 064 */new IFA_BINARY(8, "MESSAGEAUTHENTICATIONCODEFIELD"),
			/* 065 */new IFA_BINARY(8, "BITMAPEXTENDED"),
			/* 066 */new IFA_NUMERIC(1, "SETTLEMENTCODE"),
			/* 067 */new IFA_NUMERIC(2, "EXTENDEDPAYMENTCODE"),
			/* 068 */new IFA_NUMERIC(3, "RECEIVINGINSTITUTIONCOUNTRYCODE"),
			/* 069 */new IFA_NUMERIC(3, "SETTLEMENTINSTITUTIONCOUNTRYCODE"),
			/* 070 */new IFA_NUMERIC(3, "NETWORKMANAGEMENTINFORMATIONCODE"),
			/* 071 */new IFA_NUMERIC(4, "MESSAGENUMBER"),
			/* 072 */new IFA_NUMERIC(4, "MESSAGENUMBERLAST"),
			/* 073 */new IFA_NUMERIC(6, "DATEACTION"),
			/* 074 */new IFA_NUMERIC(10, "CREDITSNUMBER"),
			/* 075 */new IFA_NUMERIC(10, "CREDITSREVERSALNUMBER"),
			/* 076 */new IFA_NUMERIC(10, "DEBITSNUMBER"),
			/* 077 */new IFA_NUMERIC(10, "DEBITSREVERSALNUMBER"),
			/* 078 */new IFA_NUMERIC(10, "TRANSFERNUMBER"),
			/* 079 */new IFA_NUMERIC(10, "TRANSFERREVERSALNUMBER"),
			/* 080 */new IFA_NUMERIC(10, "INQUIRIESNUMBER"),
			/* 081 */new IFA_NUMERIC(10, "AUTHORIZATIONNUMBER"),
			/* 082 */new IFA_NUMERIC(12, "CREDITSPROCESSINGFEEAMOUNT"),
			/* 083 */new IFA_NUMERIC(12, "CREDITSTRANSACTIONFEEAMOUNT"),
			/* 084 */new IFA_NUMERIC(12, "DEBITSPROCESSINGFEEAMOUNT"),
			/* 085 */new IFA_NUMERIC(12, "DEBITSTRANSACTIONFEEAMOUNT"),
			/* 086 */new IFA_NUMERIC(16, "CREDITSAMOUNT"),
			/* 087 */new IFA_NUMERIC(16, "CREDITSREVERSALAMOUNT"),
			/* 088 */new IFA_NUMERIC(16, "DEBITSAMOUNT"),
			/* 089 */new IFA_NUMERIC(16, "DEBITSREVERSALAMOUNT"),
			/* 090 */new IFA_NUMERIC(42, "ORIGINALDATAELEMENTS"),
			/* 091 */new IF_CHAR(1, "FILEUPDATECODE"),
			/* 092 */new IF_CHAR(2, "FILESECURITYCODE"),
			/* 093 */new IF_CHAR(5, "RESPONSEINDICATOR"),
			/* 094 */new IF_CHAR(7, "SERVICEINDICATOR"),
			/* 095 */new IF_CHAR(42, "REPLACEMENTAMOUNTS"),
			/* 096 */new IFA_BINARY(8, "MESSAGESECURITYCODE"),
			/* 097 */new IFA_AMOUNT(17, "AMOUNTNETSETTLEMENT"),
			/* 098 */new IF_CHAR(25, "PAYEE"),
			/* 099 */new IFA_LLNUM(11, "SETTLEMENTINSTITUTIONIDENTCODE"),
			/* 100 */new IFA_LLNUM(11, "RECEIVINGINSTITUTIONIDENTCODE"),
			/* 101 */new IFA_LLCHAR(17, "FILENAME"),
			/* 102 */new IFA_LLNUM(13, "SourceAccountNumber"),//in
			/* 103 */new IFA_LLNUM(13, "DestinationAccountNumber"),//in
			/* 104 */new IFA_LLLCHAR(100, "TRANSACTIONDESCRIPTION"),
			/* 105 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 106 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 107 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 108 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 109 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 110 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 111 */new IFA_LLLCHAR(999, "RESERVEDISOUSE"),
			/* 112 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 113 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 114 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 115 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 116 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 117 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 118 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 119 */new IFA_LLLCHAR(999, "RESERVEDNATIONALUSE"),
			/* 120 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 121 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 122 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 123 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 124 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 125 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 126 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 127 */new IFA_LLLCHAR(999, "RESERVEDPRIVATEUSE"),
			/* 128 */new IFA_LLLCHAR(999, "MAC2")

	};
	/**
	 * Return length of field 
	 * @param field
	 * @return
	 */
	public static int getBit(String field){
		int i = 0;
		while(i<fld.length){
			if(fld[i].getDescription().equalsIgnoreCase(field)){
				return i;
			}
			i++;
		}
		return -1;
	}
	
}
