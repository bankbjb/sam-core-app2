package com.bjb.cache;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.dto.Pair;
import com.bjb.model.Aggregators;
import com.bjb.repository.AggregatorsRepository;
import com.bjb.util.CommonUtils;

/**
 * This class is cache for Aggregators.
 * 
 * @author arifino
 *
 */
@Component
public class ForAggregators {
	/**
	 * Map for caching Aggregators. Key : [string]aggregatorId Value :
	 * [object:Aggregators] aggregators
	 */
	private static Map<Integer, Aggregators> aggregatorsMap = new HashMap<>();

	private static AggregatorsRepository aggregatorsRepository;

	private static Logger log = LoggerFactory.getLogger(ForAggregators.class);

	@Autowired
	public void setAggregatorsRepository(AggregatorsRepository repo) {
		aggregatorsRepository = repo;
	}

	/**
	 * Init aggregators cache from db
	 */
	@SuppressWarnings("rawtypes")
	public static void init() {
		List list = aggregatorsRepository.findAll();
		if (list.size() > 0) {
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Aggregators aggregator = (Aggregators) iterator.next();
				aggregatorsMap.put(aggregator.getId(), aggregator);
			}
		}
	}

	/**
	 * Return aggregators from map based on code(id)
	 * 
	 * @param aggCode
	 * @return Aggregators
	 */
	public static Aggregators getAggregators(String aggCode) {
		// TODO bit use number?
		log.info(Messages.AGG_RETRIEVING);
		Aggregators agg = (Aggregators) aggregatorsMap.get(Integer.valueOf(CommonUtils.removeUntilNumber(aggCode)));
		if (agg != null) {
			log.info(Messages.AGG_FOUND);
			return agg;
		} else
			return null;
	}

	/**
	 * For testing purpose, never used.
	 * 
	 * @param aggCode
	 * @param type
	 * @return boolean
	 */
	public static boolean getValidMessageType(String aggCode, String type) {
		// TODO bit use number?
		Aggregators agg = (Aggregators) aggregatorsMap.get(Integer.valueOf(CommonUtils.removeUntilNumber(aggCode)));
		// Aggregators agg = (Aggregators)
		// aggregatorsMap.get(Integer.valueOf(aggCode.substring(1)));
		if (agg != null) {
			if (agg.getMessageType().equalsIgnoreCase(type)) {
				return true;
			} else
				return false;
		} else
			return false;
	}

	/**
	 * Return account number of aggregators.
	 * 
	 * @param aggCode
	 * @return String
	 */
	public static String getAggregatorAccount(String aggCode) {
		Aggregators agg = getAggregators(aggCode);
		if (agg != null) {
			return agg.getAccountNo();
		} else
			return null;
	}

	public static String getAggregatorGlAccount(String aggCode) {
		Aggregators agg = getAggregators(aggCode);
		if (agg != null) {
			return agg.getAccountGlNo();
		} else
			return null;
	}

	/**
	 * Check validity of aggregators based on status,message type, agg ca
	 * status, pks date.
	 * 
	 * @param aggCode
	 * @param msg
	 * @return boolean
	 */
	public static boolean getValid(String aggCode, String msg) {
		Aggregators agg = getAggregators(aggCode);
		if (agg != null) {
			if (agg.getStatus().equals(Integer.valueOf(1))) {
				if (agg.getMessageType().equalsIgnoreCase(msg)) {
					// if(agg.getStatusCa().equals(Integer.valueOf(1))){
					Timestamp now = new Timestamp(new Date().getTime());
					Timestamp exp = agg.getPksExpiredDate();
					if (now.before(exp)) {
						return true;
					} else {
						log.info(Messages.AGG_PKS_DATE_EXPIRED);
						return false;
					}
					// }else {
					// log.info(Messages.AGG_CA_DISABLED);
					// return false;
					// }
				} else {
					log.info(Messages.AGG_MESSAGE_TYPE_MISMATCH + " : " + agg.getMessageType() + " >< " + msg);
					return false;
				}
			} else {
				log.info(Messages.AGG_DISABLED);
				return false;
			}
		} else
			return false;
	}

	/**
	 * Check validity of aggregators based on status,message type, agg ca
	 * status, pks date.
	 * 
	 * @param aggCode
	 * @param msg
	 * @return boolean
	 */
	public static Pair<Boolean, String> getValidWithReason(String aggCode, String msg) {
		Aggregators agg = getAggregators(aggCode);
		if (agg != null) {
			if (agg.getStatus().equals(Integer.valueOf(1))) {
				if (agg.getMessageType().equalsIgnoreCase(msg)) {
					// if(agg.getStatusCa().equals(Integer.valueOf(1))){
					Timestamp now = new Timestamp(new Date().getTime());
					Timestamp exp = agg.getPksExpiredDate();
					if (now.before(exp)) {
						//log.info(Messages.AGG_VALID);
						return new Pair<>(true, Messages.AGG_FOUND);
						// return true;
					} else {
						//log.info(Messages.AGG_PKS_DATE_EXPIRED);
						return new Pair<>(false, Messages.AGG_PKS_DATE_EXPIRED);
						// return false;
					}
					// }else {
					// log.info(Messages.AGG_CA_DISABLED);
					// return new Pair<>(false, Messages.AGG_CA_DISABLED);
					// //return false;
					// }
				} else {
					//log.info(Messages.AGG_MESSAGE_TYPE_MISMATCH + " : " + agg.getMessageType() + " >< " + msg);
					return new Pair<>(false, Messages.AGG_MESSAGE_TYPE_MISMATCH);
					// return false;
				}
			} else {
				//log.info(Messages.AGG_DISABLED);
				// return false;
				return new Pair<>(false, Messages.AGG_DISABLED);
			}
		} else
			return new Pair<>(false, Messages.AGG_NOT_FOUND);// return false;
	}

	public static String getErrorCode(String message) {
		if (message.equalsIgnoreCase(Messages.AGG_CA_DISABLED)) {
			return ForSystemParameter.getSysValue(Constants.RC_SAM_010);
		} else if (message.equalsIgnoreCase(Messages.AGG_DISABLED)) {
			return ForSystemParameter.getSysValue(Constants.RC_SAM_011);
		} else if (message.equalsIgnoreCase(Messages.AGG_MESSAGE_TYPE_MISMATCH)) {
			return ForSystemParameter.getSysValue(Constants.RC_SAM_012);
		} else if (message.equalsIgnoreCase(Messages.AGG_PKS_DATE_EXPIRED)) {
			return ForSystemParameter.getSysValue(Constants.RC_SAM_013);
		} else {
			return ForSystemParameter.getSysValue(Constants.RC_SAM_001);
		}
	}

	/**
	 * Update cache if there is change in db. Tied to DBListener class.
	 * 
	 * @param aggregatorId
	 */
	public static void updateCache(String aggregatorId) {
		Aggregators agg = aggregatorsRepository.findById(Integer.valueOf(aggregatorId));
		if (agg != null) {
			aggregatorsMap.put(agg.getId(), agg);
			// System.out.println(agg);
			log.info(Messages.CACHE_AGG_UPDATED);
		} else {
			log.info(Messages.CACHE_AGG_UPDATE_ERROR);
		}
	}

	/**
	 * Delete cache if there is change in db. Tied to DBListener class.
	 * 
	 * @param aggregatorId
	 */
	public static void deleteCache(String aggregatorId) {
		Aggregators agg = aggregatorsRepository.findById(Integer.valueOf(aggregatorId));
		if (agg != null) {
			aggregatorsMap.remove(agg.getId());
			// System.out.println(agg);
			log.info(Messages.CACHE_AGG_UPDATED);
		} else {
			log.info(Messages.CACHE_AGG_UPDATE_ERROR);
		}
	}

}
