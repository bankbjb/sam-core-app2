package com.bjb.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.model.MessageFields;
import com.bjb.repository.MessageFieldsRepository;

/**
 * Testing only.
 * 
 * @author arifino
 *
 */
@Component
public class ForBitSixtyOneMessageField {
	@SuppressWarnings("rawtypes")
	private static List bitXml = new ArrayList<>();
	@SuppressWarnings("rawtypes")
	private static List bitJson = new ArrayList<>();
	
	private static Map<Integer,MessageFields> bit61Variable = new HashMap<>();
//	private static Map<String,Integer> bit61VarLength = new HashMap<>();
//	private static Map<String,String> bit61VarName = new HashMap<>();

	private static MessageFieldsRepository messageFieldsRepository;

	@Autowired
	public void setMessageFieldsRepository(MessageFieldsRepository repo) {
		messageFieldsRepository = repo;
	}

	public static void init() {
		//bitXml = messageFieldsRepository.findByMessageTypeIgnoreCase("XML");
		//bitJson = messageFieldsRepository.findByMessageTypeIgnoreCase("JSON");
		List<MessageFields> list = messageFieldsRepository.findByMessageTypeIgnoreCase("61");
		for(MessageFields f : list){
			bit61Variable.put(f.getId(), f);
//			bit61VarLength.put(f.getName(), Integer.valueOf(f.getDescription()));
//			bit61VarName.put(f.getName(),String.valueOf(f.getId()));
		}
	}
	
//	public static Map<String,Integer> getMapVariableIdLength(){
//		return bit61VarLength;
//	}
//	
//	public static Map<String,String> getMapVariableNameId(){
//		return bit61VarName;
//	}

	@SuppressWarnings("rawtypes")
	public static List getFields(String type) {
		if (type.equalsIgnoreCase("XML")) {
			return bitXml;
		} else if (type.equalsIgnoreCase("JSON")) {
			return bitJson;
		} else
			return null;
	}
	
	public static MessageFields getFieldById(String type,Integer id){
		if(type.equalsIgnoreCase("61")){
			return bit61Variable.get(id);
		}else return null;
	}
	
//	public static Integer getVariableLenth(String var){
//		return bit61VarLength.get(var);
//	}

	@SuppressWarnings({ "rawtypes" })
	public static String getFieldsById(String type, Integer id) {
		if (type.equalsIgnoreCase("XML")) {
			Iterator iter = bitXml.iterator();
			while (iter.hasNext()) {
				MessageFields m = (MessageFields) iter.next();
				if (id.equals(m.getId())) {
					return m.getName();
				}
			}
			return null;
		} else if (type.equalsIgnoreCase("JSON")) {
			Iterator iter = bitJson.iterator();
			while (iter.hasNext()) {
				MessageFields m = (MessageFields) iter.next();
				if (id == m.getId()) {
					return m.getName();
				}
			}
			return null;
		} 
		return null;
	}

}
