package com.bjb.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Messages;
import com.bjb.model.MessageMapping;
import com.bjb.repository.MessageMappingRepository;

/**
 * This class is cache for bit 61 building. Include : messageMapping, fieldMapping.
 * 
 * @author arifino
 *
 */
@Component
public class ForBitSixtyOneMapping {
	/**
	 * Map for caching messageMapping.
	 * Key 		: [string]featureId+messageType (ex:108XML)
	 * Value 	: [object:MessageMapping]messageMapping
	 */
	private static Map<String, MessageMapping> mapSixtyOne = new HashMap<>();

	private static MessageMappingRepository messageMappingRepository;

	@Autowired
	public void setMessageMappingRepository(MessageMappingRepository repo) {
		messageMappingRepository = repo;
	}

	private static Logger log = LoggerFactory.getLogger(ForBitSixtyOneMapping.class);
	@SuppressWarnings("rawtypes")
	public static void init() {
		List list = messageMappingRepository.findAll();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MessageMapping mm = (MessageMapping) iterator.next();
			String key = mm.getFeatureId().intValue() + mm.getMessageType();
			// System.out.println(key);
			mapSixtyOne.put(key.toLowerCase(), mm);

		}
	}

	/**
	 * Returns message mapping from cache
	 * 
	 * @param featureCode
	 * @return MessageMapping
	 */
	public static MessageMapping getMessageMapping(String featureCode) {
		MessageMapping m = null;
		m = mapSixtyOne.get(featureCode);
		if (m != null) {
			if (m.getStatus().equals(Integer.valueOf(1))) {
				return m;
			} else
				return null;
		} else
			return null;
	}

	/**
	 * Returns message mapping based on feature code and message type
	 * 
	 * @param featureCode
	 * @param messageType
	 * @return MessageMapping
	 */
	public static MessageMapping getMessageMapping(Integer featureCode, String messageType) {
		MessageMapping m = null;
		String key = Integer.valueOf(featureCode) + messageType;
		m = mapSixtyOne.get(key.toLowerCase());
		if (m != null) {
			if (m.getStatus().equals(Integer.valueOf(1))) {
				return m;
			} else
				return null;
		} else
			return null;
	}
	/**
	 * Delete cache if there is change in db. Tied to DBListener.class
	 * 
	 * @param key
	 */
	public static void deleteCache(String key){
		MessageMapping m = null;
		m = mapSixtyOne.get(key.toLowerCase());
		if (m != null) {
			MessageMapping mm = messageMappingRepository.findById(m.getId());
			if (mm != null) {
				String keys = mm.getFeatureId().intValue() + mm.getMessageType();
				mapSixtyOne.remove(keys.toLowerCase());
				log.info(Messages.CACHE_61_UPDATE);
			} else {
				log.warn(Messages.CACHE_61_UPDATE_ERROR);
			}
		} else {
			MessageMapping mm = messageMappingRepository.findById(Integer.valueOf(key));
			if (mm != null) {
				String keys = mm.getFeatureId().intValue() + mm.getMessageType();
				mapSixtyOne.remove(keys.toLowerCase());
				log.info(Messages.CACHE_61_UPDATE);
			} else {
				log.warn(Messages.CACHE_61_UPDATE_ERROR);
			}
		}
	}

	/**
	 * Update cache if there is change in db. Tied to DBListener.class
	 * 
	 * @param key
	 */
	public static void updateCache(String key) {
		MessageMapping m = null;
		m = mapSixtyOne.get(key.toLowerCase());
		if (m != null) {
			MessageMapping mm = messageMappingRepository.findById(m.getId());
			if (mm != null) {
				String keys = mm.getFeatureId().intValue() + mm.getMessageType();
				mapSixtyOne.put(keys.toLowerCase(), mm);
				log.info(Messages.CACHE_61_UPDATE);
			} else {
				log.warn(Messages.CACHE_61_UPDATE_ERROR);
			}
		} else {
			MessageMapping mm = messageMappingRepository.findById(Integer.valueOf(key));
			if (mm != null) {
				String keys = mm.getFeatureId().intValue() + mm.getMessageType();
				mapSixtyOne.put(keys.toLowerCase(), mm);
				log.info(Messages.CACHE_61_UPDATE);
			} else {
				log.warn(Messages.CACHE_61_UPDATE_ERROR);
			}
		}
	}

}
