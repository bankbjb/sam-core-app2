package com.bjb.cache;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.postgresql.PGConnection;
import org.postgresql.PGNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.model.SystemParameter;
import com.bjb.util.CommonUtils;

/**
 * Listener for PostgreDB using notify,listen function This class is tied to DB
 * triggers function.
 * 
 * @author arifino
 *
 */
@Component
public class DBListener implements Runnable {

	private static Logger log = LoggerFactory.getLogger(DBListener.class);

	@Autowired
	private DataSource datasource;

	@Override
	public void run() {
		try {
			log.info(Messages.DB_LISTENER_START);
			Class.forName(Constants.DB_DRIVER_NAME);

			String dbHost = "localhost";
			String dbPort = "5432";
			String dbName = "bjb_sam_db";
			String user = "pgsql";
			String pass = "pgsql";
			Properties props = new Properties();
			InputStream fis = null;
			String url = "";
			Connection lConn;
			try {
				lConn = datasource.getConnection();
				log.info("DB Listener Using Datasource");
			} catch (Exception exa) {
				try {
					ClassPathResource res = new ClassPathResource("datasource.properties");
					fis = new FileInputStream(res.getFile());
					props.load(fis);
					url = props.getProperty("db.url");
					user = props.getProperty("db.username");
					pass = props.getProperty("db.password");
					if (CommonUtils.isBlank(url) || CommonUtils.isBlank(user) || CommonUtils.isBlank(pass)) {
						throw new Exception("Not Found Enough Information");
					}
					log.info("DB Listener Using Properties");
				} catch (Exception e) {
					e.printStackTrace();
					if (ForSystemParameter.getSysValue(Constants.DATABASE_IP) != null) {
						dbHost = ForSystemParameter.getSysValue(Constants.DATABASE_IP);
					}
					if (ForSystemParameter.getSysValue(Constants.DATABASE_PORT) != null) {
						dbPort = ForSystemParameter.getSysValue(Constants.DATABASE_PORT);
					}
					if (ForSystemParameter.getSysValue(Constants.DATABASE_NAME) != null) {
						dbName = ForSystemParameter.getSysValue(Constants.DATABASE_NAME);
					}
					if (ForSystemParameter.getSysValue(Constants.DATABASE_USERNAME) != null) {
						user = ForSystemParameter.getSysValue(Constants.DATABASE_USERNAME);
					}
					if (ForSystemParameter.getSysValue(Constants.DATABASE_PASSWORD) != null) {
						pass = ForSystemParameter.getSysValue(Constants.DATABASE_PASSWORD);
					}
					url = Constants.DB_URL + "//" + dbHost + ":" + dbPort + "/" + dbName;
				}
			lConn = DriverManager.getConnection(url, user, pass);
			}
			Listener listener = new Listener(lConn);
			listener.start();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

/**
 * Inner class for listening to db
 * 
 * @author arifino
 *
 */
class Listener extends Thread {

	private Logger log = LoggerFactory.getLogger(Listener.class);

	private Connection conn;
	private PGConnection pgconn;

	// TODO add listener for other delete triggers
	Listener(Connection conn) throws SQLException {
		this.conn = conn;
		this.pgconn = (PGConnection) conn;
		Statement stmt = conn.createStatement();
		stmt.execute("LISTEN " + Constants.NOTIFICATION_AGGREGATOR);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_FIFTYSEVEN);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_SIXTYONE);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_PARAMETERS);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_SIXTYONE_DEL);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_AGGREGATOR_DEL);
		stmt.execute("LISTEN " + Constants.NOTIFICATION_FIFTYSEVEN_DEL);
		stmt.close();
	}

	public void run() {
		while (true) {
			try {
				// issue a dummy query to contact the backend
				// and receive any pending notifications.
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT 1");
				rs.close();
				stmt.close();

				PGNotification notifications[] = pgconn.getNotifications();
				if (notifications != null) {
					for (int i = 0; i < notifications.length; i++) {
						log.info(Messages.DB_LISTENER_LISTEN + notifications[i].getName() + " : "
								+ notifications[i].getParameter());
						// notifications[i].get
						if (notifications[i].getName().equals(Constants.NOTIFICATION_AGGREGATOR)) {
							ForAggregators.updateCache(notifications[i].getParameter());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_AGGREGATOR)) {
							ForAggregators.deleteCache(notifications[i].getParameter());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_FIFTYSEVEN)) {
							ForBitFiftySevenMapping.updateCache(notifications[i].getParameter().toLowerCase());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_FIFTYSEVEN_DEL)) {
							ForBitFiftySevenMapping.deleteCache(notifications[i].getParameter().toLowerCase());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_SIXTYONE)) {
							ForBitSixtyOneMapping.updateCache(notifications[i].getParameter().toLowerCase());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_SIXTYONE_DEL)) {
							ForBitSixtyOneMapping.updateCache(notifications[i].getParameter().toLowerCase());
						} else if (notifications[i].getName().equals(Constants.NOTIFICATION_PARAMETERS)) {
							ForSystemParameter.updateCache(notifications[i].getParameter());
							SystemParameter prop = ForSystemParameter.getSystemParameter(Constants.CORE_STAN_CACHE);
							if (prop != null) {
								ForStan.setStanCache(Integer.valueOf(prop.getValue()));
							}
						} else {
							log.warn(Messages.UNRECOGNIZED_NOTIFICATION);
							log.warn(notifications[0].getName() + " " + notifications[0].getName().length());
							log.warn(Constants.NOTIFICATION_FIFTYSEVEN + " "
									+ Constants.NOTIFICATION_FIFTYSEVEN.length());
						}
					}
				}

				// wait a while before checking again for new
				// notifications
				Thread.sleep(500);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}

/**
 * Notifier class for testing purpose
 * 
 * @author arifino
 *
 */
class Notifier extends Thread {

	private Connection conn;

	public Notifier(Connection conn) {
		this.conn = conn;
	}

	public void run() {
		while (true) {
			try {
				Statement stmt = conn.createStatement();
				stmt.execute("NOTIFY sam_db");
				stmt.close();
				Thread.sleep(2000);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}
