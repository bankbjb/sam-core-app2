package com.bjb.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Messages;
import com.bjb.dto.Pair;
import com.bjb.model.FeatureMapping;
import com.bjb.repository.CollectingAgentRepository;
import com.bjb.repository.FeatureMappingRepository;
import com.bjb.repository.FeatureRepository;

@Component
public class ForAdditionalInfo {

	private static CollectingAgentRepository collectingAgentRepository;

	private static FeatureMappingRepository featureMappingRepository;

	private static FeatureRepository featureRepository;

	@Autowired
	public void setFeatureRepository(FeatureRepository sitory) {
		featureRepository = sitory;
	}

	@Autowired
	public void setCollectingAgentRepository(CollectingAgentRepository ository) {
		collectingAgentRepository = ository;
	}

	@Autowired
	public void setFeatureMappingRepository(FeatureMappingRepository ory) {
		featureMappingRepository = ory;
	}

	private static Integer getCaId(Integer featureId, Integer aggId) {
		FeatureMapping fm = featureMappingRepository.findByFeatureIdAndAggregatorId(featureId, aggId);
		if (fm != null) {
			return fm.getCollectingAgentId();
		} else {
			return 0;
		}
		// return
		// featureMappingRepository.findByFeatureIdAndAggregatorId(featureId,
		// aggId).getCollectingAgentId();
	}

	public static Pair<Boolean, String> getValidCA(Integer featureId, Integer aggId) {
		Integer caId = getCaId(featureId, aggId);
		if (caId > 0) {
			if (collectingAgentRepository.findById(caId).getStatus().equals(Integer.valueOf(1))) {
				return new Pair<>(true, Messages.AGG_FOUND);
			} else {
				return new Pair<>(false, Messages.AGG_CA_DISABLED);
			}
		} else {
			return new Pair<>(false, Messages.AGG_CA_FEATURE_NOT_FOUND);
		}
	}

	public static String getFeatureCommisionAccountNo(Integer featureid) {
		return featureRepository.findById(featureid).getCommisionAccountNo();
	}

}
