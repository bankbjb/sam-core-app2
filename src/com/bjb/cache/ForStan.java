package com.bjb.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Constants;
import com.bjb.model.SystemParameter;
import com.bjb.repository.StoreProcRepository;
import com.bjb.util.CommonUtils;

/**
 * Caching for stan generation.
 * 
 * @author arifino
 *
 */
@Component
public class ForStan {

	private static StoreProcRepository storeProcRepository;

	@Autowired
	public void setStoreProcRepository(StoreProcRepository repo) {
		storeProcRepository = repo;
	}

	private static Logger log = LoggerFactory.getLogger(ForStan.class);

	/**
	 * Number of cache stan number each cycle.
	 */
	private static int stanCache = 100;
	/**
	 * Number of remaining stan number in this cycle.
	 */
	private static int remStan;
	/**
	 * Current stan
	 */
	private static String stanNow;

	public static int getStanCache() {
		return stanCache;
	}

	public static void setStanCache(int stanCach) {
		stanCache = stanCach;
	}

	public static void init() {
		SystemParameter prop = ForSystemParameter.getSystemParameter(Constants.CORE_STAN_CACHE);
		if (prop != null) {
			stanCache = Integer.valueOf(prop.getValue());// prop.getStanCache();
		}
		remStan = Integer.valueOf(stanCache);
		stanNow = storeProcRepository.NextStan(Integer.valueOf(stanCache));
		log.info("<<< STAN IN DB : " + stanNow + " >>>");
		stanNow = CommonUtils.padleft((Integer.valueOf(stanNow) - stanCache) + "", 6, '0');

	}

	/**
	 * Returns current number of stan.
	 * 
	 * @return String
	 */
	public static String getStanNow() {
		String retuStan = stanNow;

		int stanNw = Integer.parseInt(stanNow);
		stanNw++;
		remStan--;
		if (remStan == 0) {
			stanNow = storeProcRepository.NextStan(Integer.valueOf(stanCache));
			stanNow = CommonUtils.padleft((Integer.valueOf(stanNow) - stanCache) + "", 6, '0');
			remStan = stanCache;
		} else {
			stanNow = CommonUtils.padleft(stanNw + "", 6, '0');
		}
		return retuStan;
	}
}
