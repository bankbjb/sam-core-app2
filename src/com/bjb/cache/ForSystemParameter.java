package com.bjb.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjb.constants.Constants;
import com.bjb.constants.Messages;
import com.bjb.model.SystemParameter;
import com.bjb.repository.SystemParameterRepository;

/**
 * Caching for SystemParameter.
 * 
 * @author arifino
 *
 */
@Component
public class ForSystemParameter {
	/**
	 * Map for caching systemParameter.
	 * Key		: [string]parameter_name
	 * Value	: [object:SystemParameter]systemParameter
	 */
	private static Map<String, Object> systemParameterMap = new HashMap<>();

	private static SystemParameterRepository systemParameterRepository;

	@Autowired
	public void setSystemParameterRepository(SystemParameterRepository repo) {
		systemParameterRepository = repo;
	}

	private static Logger log = LoggerFactory.getLogger(ForSystemParameter.class);

	@SuppressWarnings("rawtypes")
	public static void init() {
		List list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_CORE);
		//System.out.println(list.size());
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_DATABASE);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			log.info(sp.toString());
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_IF_400);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_WEB_SERVICE);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_MAIL);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_RETENTION);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_RC);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
		list = systemParameterRepository.findByNameContaining(Constants.GROUP_LABEL_BALANCE);
		for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
			SystemParameter sp = (SystemParameter) iterator.next();
			systemParameterMap.put(sp.getName().toLowerCase(), sp);
		}
	}

	/**
	 * Returns systemParameter
	 * 
	 * @param name
	 * @return
	 */
	public static SystemParameter getSystemParameter(String name) {
		SystemParameter sp = (SystemParameter) systemParameterMap.get(name.toLowerCase());
		if (sp != null) {
			return sp;
		} else {
			return null;
		}
	}

	/**
	 * Return value of SystemParameter
	 * 
	 * @param name
	 * @return
	 */
	public static String getSysValue(String name) {
		SystemParameter sp = (SystemParameter) getSystemParameter(name);
		if (sp != null) {
			return sp.getValue();
		} else
			return null;
	}

	/**
	 * Update cache if there is change in db. Tied to DBListener.class
	 * 
	 * @param truth
	 */
	public static void updateCache(String truth) {
		if (truth.equals("true")) {
			init();
			log.info(Messages.CACHE_SYSPARAM_UPDATED);
		}
	}

	public static Map<String, String> getEmailProviderSettings() {
		Map<String, String> emailSetting = new HashMap<>();
		String host;
		String auth;
		String port;
		String mailer;
		String password;
		String subject;
		String text;
		if (ForSystemParameter.getSysValue(Constants.MAIL_SMTP_HOST) != null) {
			host = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_HOST);
			emailSetting.put(Constants.MAIL_SMTP_HOST, host);
		}
		if (ForSystemParameter.getSysValue(Constants.MAIL_SMTP_AUTH) != null) {
			auth = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_AUTH);
			emailSetting.put(Constants.MAIL_SMTP_AUTH, auth);
		}
		if (ForSystemParameter.getSysValue(Constants.MAIL_SMTP_PORT) != null) {
			port = ForSystemParameter.getSysValue(Constants.MAIL_SMTP_PORT);
			emailSetting.put(Constants.MAIL_SMTP_PORT, port);
		}
		if (ForSystemParameter.getSysValue(Constants.MAIL_MAILER_ACCOUNT) != null) {
			mailer = ForSystemParameter.getSysValue(Constants.MAIL_MAILER_ACCOUNT);
			emailSetting.put(Constants.MAIL_MAILER_ACCOUNT, mailer);
		}
		if (ForSystemParameter.getSysValue(Constants.MAIL_MAILER_PASSWORD) != null) {
			password = ForSystemParameter.getSysValue(Constants.MAIL_MAILER_PASSWORD);
			emailSetting.put(Constants.MAIL_MAILER_PASSWORD, password);
		}
		if (ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_SUBJECT) != null) {
			subject = ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_SUBJECT);
			emailSetting.put(Constants.BALANCE_NOTIF_SUBJECT, subject);
		}
		if (ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_TEXT) != null) {
			text = ForSystemParameter.getSysValue(Constants.BALANCE_NOTIF_TEXT);
			emailSetting.put(Constants.BALANCE_NOTIF_TEXT, text);
		}
		return emailSetting;
	}
	
	public static boolean isEmpty(){
		return systemParameterMap.isEmpty();
	}

}
